"""
@author: Российский государственный университет нефти и газа имени И. М. Губкина
         Кафедра РиЭНМ
         
Обозначения:
    Структуры потока (fpat - [flow pattern]):
        dbub - [dispersed bubble] - рассеяный пузырьковый поток
        bub - [bubble] - пузырьковый (аэрированный) поток
        slug - пробковый режим потока
        single - однофазный поток
        annular - кольцевой режим потока
    lyambdal - объемное содержание жидкости без проскальзывания    
    Составляющие градиента давления:
        pge - [elevation pressure gradient] - гравитационная 
        pgf - [friction pressure gradient] - трения
        pga - [acceleration pressure gradient] - ускорения
        pgt - [total pressure gradient] - общая
        pgc - газовое ядро - общая
help
'sympy'
    diff (y, x) - операция дифференцирования
    integrate (y, x) - операция интегрирования
    Symbol('x') - символьная переменная

scipy.optimize - import решение нелинейных уравнений, например newton   
    Для применения различных методов нужно заменить команду в коде на одну из ниже приведенных:
    newton(func, x0, fprime=None, args=(), tol=1.48e-8, maxiter=50, fprime2=None)
    bisect(f, a, b, args=(), xtol=_xtol, rtol=_rtol, maxiter=_iter, full_output=False, disp=True)
    ridder(f, a, b, args=(), xtol=_xtol, rtol=_rtol, maxiter=_iter, full_output=False, disp=True)
    brentq(f, a, b, args=(), xtol=_xtol, rtol=_rtol, maxiter=_iter, full_output=False, disp=True)   
    brenth(f, a, b, args=(), xtol=_xtol, rtol=_rtol, maxiter=_iter, full_output=False, disp=True)

Доработать в следующих версиях:
1) Запрогать поверхностное натяжение по Ляпкову.
"""
from math import*
from scipy.optimize import*
from decimal import*
g = 9.81         # постоянная ускорения свободного падения
rho_air = 1.2217 # плотность воздуха при стандартных условиях
gamma_w = 1      # для корреляции плотности воды от солёности

class Qvogel():
    def Qvogel_wcF(self, P_test, Pr, Pb, pi, wc):
        qb = pi * (Pr - Pb)
        '''Q_test    - м3/сут'
         ' Pr        - атм'
         ' Pb        - атм'
         ' PI - м3/сут/атм'
         ' wc        - %'''        
        if wc > 100:
             wc = 100
        if wc < 0:
            wc = 0
        if (wc == 100) or (P_test > Pb):
            Qvogel_wc = pi * (Pr - P_test)
        else:
            fw = wc / 100
            fo = 1 - fw
            qo_max = qb + (pi * Pb) / 1.8
            Pwfg = fw * (Pr - qo_max / pi)
            if P_test > Pwfg:
                a = 1 + (P_test - (fw * Pr)) / (0.125 * fo * Pb)
                b = fw / (0.125 * fo * Pb * pi)
                c = (2 * a * b) + 80 / (qo_max - qb)
                d = (a ** 2) - (80 * qb / (qo_max - qb)) - 81
                if b == 0:
                    Qvogel_wc = abs(d / c)
                else:
                    Qvogel_wc = (-c + ((c * c - 4 * b * b * d) ** 0.5)) / (2 * b ** 2)
            else:
                CG = 0.001 * qo_max
                CD = fw * (CG / pi) + fo * 0.125 * Pb * (-1 + (1 + 80 * ((0.001 * qo_max) / (qo_max - qb))) ** 0.5)
                Qvogel_wc = (Pwfg - P_test) / (CD / CG) + qo_max
        self.Qvogel_wc = Qvogel_wc
class pressuregradient:
    def Pipe_pressure_drop (self, d, q_osc, teta, ksi, length, p_sn, t_sn, t_en, WCT, gamma_o, gamma_g, R_p, n_n, Salinity, Calc_p_out, PVT_correlations, r_sb, P_rb, b_ro, MaxSegmLen, grad_Calc, Payne_et_all_holdup, Payne_et_all_friction, slip):
        '''
        Расчёт давления в начале или конце трубы [а также, по трубе] через расчёт градиентов давления
        Параметры:
        d - диаметр трубы [м]
        L - длина трубы [м]
        teta - угол наклона трубы [положительное значение - поток восходящий, иначе нисходящий]
        ksi - шероховатость стенки трубы [м]
        p_sn - Давление в начале трубы [атм]
        T_sn - Температура в начале трубы [*C]
        q_oSC - Дебит нефти в стандартных условиях [м3/сут]
        WCT - Обводненность в стандартных условиях [доли ед.]
        gamma_o - Удельная плотность нефти по воде [доли ед.]
        gamma_g - Удельная плотность газа по воздуху [доли ед.]
        r_p - Отношение добываемого газа к жидкости [м3/м3]
        Salinity - Солёность воды
        PVT_correlations - Индикатор выбора корреляций
        
        PVT - корреляции: 
        Вязкость
          Dead oil - Beggs & Robinson
          Saturated - Beggs & Robinson
          Undersaturated - Vazquez & Beggs
          Gas - Lee
          Water - McCain 1990
        Газонасыщенность
          Standing
        Объемный коэффициент нефти
          Standing
        Сжимаемость нефти
          Vazquez & Beggs
        Коэффициент сверхсжимаемости газа
          Dranchuk & Abu-Kassem (Computer version of Standing & Katz )
        
        r_sb - газонасыщенность при давлении насыщения [м3/м3]
        p_rb - Калибровочное давление насыщения [атм]
        b_ro - Калибровочный объемный коэффициент нефти при давлении насыщения
        n_n  - число расчётных узлов
        '''
        n_nod = n_n
        if (MaxSegmLen > 0 and length > MaxSegmLen): 
            n_nod = (length / MaxSegmLen) + 1
        if n_nod < 0: 
            n_nodes = 10
        else:
            n_nodes = n_nod
        MaxIter = 5
        
        'Коэффициент поверхностного натяжения вода - газ [Н/м]'        
        sigma_w = 0.01
        
        'Коэффициент поверхностного натяжения нефть - газ [Н/м]'
        sigma_o = 0.00841
        
        'Значение плотности для расчёта плотности нефти из входного параметра удельная плотность нефти [усл. плот. воды при стандартных условиях]'
        rho_ref = 1000
        
        'При отсутствии лабораторных показателей газонасыщенности нефти при давлении насыщения, принимаем производственный газовый фактор'
        if (r_sb > 0):
            r_sb = r_sb
        else: 
            r_sb = R_p
        
        'Если не задана солёность воды, то принимаем опциональное значение'
        if (Salinity < 0):
            Salinity = 50000
        
        'Предохранитель, если дебит нефти 0, то принимаем что этот показатель стремится к нулю'
        if (q_osc < 0.000001): 
            q_osc = 0.000001
        
        'Дебит газа в стандартных условиях'
        q_gsc = R_p * q_osc  
        
        'Дебит воды в стандартных условиях'
        q_wsc = WCT * q_osc / (1 - WCT)
        
        'Плотность нефти в стандартных условиях'
        rho_osc = gamma_o * rho_ref
        
        'Плотность газа в стандартных условиях'
        rho_gsc = gamma_g * rho_air
        
        'Плотность воды в стандартных условиях'
        rho_w_sc = WaterDensitySC(Salinity, gamma_w) * rho_ref

        
        p_pvt = p_sn
        P = p_pvt 
        T = t_sn
        t_pvt = T
        L = 0
        delta_l = length / (n_nodes - 1)
        delta_t = (t_en - t_sn) / (n_nodes - 1)
        
        'Создаем массив для сбора выходных параметров'
        md_n=[] # глубина измеренная
        vd_n=[] # глубина по вертикали
        p_n=[]  # массив давлений
        t_n=[]  # массив температур
        gradP = [] # массив градиента давления
        fp_m = [] # массив режимов течения        
        p_b_m = [] # массив давлений насыщения        
        p_b_m.append(None)
        mu_o_m = [] # массив вязкостей нефти        
        mu_o_m.append(None) 
        mu_g_m = [] # массив вязкостей газа        
        mu_g_m.append(None)
        mu_w_m = [] # массив вязкостей воды        
        mu_w_m.append(None)         
        b_o_m = [] # массив объемного коэффициента нефти        
        b_o_m.append(None)
        b_g_m = [] # массив объемного коэффициента газа       
        b_g_m.append(None) 
        b_w_m = [] # массив объемного коэффициента воды       
        b_w_m.append(None)        
        z_m = [] # массив Z - факторов        
        z_m.append(None)        
        r_s_m = [] # массив газосодержания        
        r_s_m.append(None)        
        md_n.append(0) # глубина измеренная
        vd_n.append(0) # глубина по вертикали
        gradP.append(None)        
        fp_m.append(None)
        p_n.append(p_pvt)
        t_n.append(T)
        index = 0
        
        while L<length - 0.001: # условие P > 1 - для расчёта (ГЛ расстановка, для нахождения кривой 2)
            if (Calc_p_out == 0):
                sign = 1
            else:
                sign = -1
            delta_p = 0
            counter = 0
            i = 0
            
            'Итеративный расчёт PVT на центре сегмента'
            while abs(p_pvt - (P + 0.5 * delta_p)) > 0.5 and counter < MaxIter or i < 1: 
                p_pvt = P + 0.5 * delta_p
                t_pvt = T + 0.5 * delta_t
                i = i + 1
                if p_pvt<=1:
                    P = 1
                    p_pvt = 1
                """ Вызываем блок PVT данных"""
                PVT = Calc_PVT(gamma_o, gamma_g, PVT_correlations, r_sb, P_rb, b_ro, Salinity, gamma_w) 
                PVT.CalcPVT(p_pvt, t_pvt, gamma_o, gamma_g, PVT_correlations, r_sb, P_rb, b_ro, Salinity, gamma_w) 
                """Выходными параметрами являются: PVT.p_b [МПа]; PVT.r_s [м3/м3]; PVT.b_o [д. ед.]; PVT.mu_o [сПз]; PVT.z; PVT.b_g; PVT.mu_g; PVT.b_w [д. ед.]; PVT.mu_w [сПз]"""                

                a_p = pi * d ** 2 / 4
                
                'Расчёт дебитов при опорном давлении'
               
                q_o = q_osc * PVT.b_o
                q_w = q_wsc * PVT.b_w
                q_l = q_o + q_w
                q_g = PVT.b_g * (q_gsc - PVT.r_s * q_osc)
                
                'Предохранитель, если дебит газа отрицательный, то обнуляем параметр'
                if q_g <= 0:
                    q_g = 0.00000001
                
                'Объемная доля воды'
                f_w = q_w / q_l
                
                'Объемное содержание жидкости в предположении отсутствия проскальзывания'
                lambda_l = q_l / (q_l + q_g)
                
                'Плотности'
                rho_o = (rho_osc + PVT.r_s * rho_gsc) / PVT.b_o
                rho_w = rho_w_sc / PVT.b_w
                rho_l = rho_o * (1 - f_w) + rho_w * f_w
                rho_g = rho_gsc / PVT.b_g
                
                'Плотность смеси при отсутствии проскальзывания'
                rho_n = rho_l * lambda_l + rho_g * (1 - lambda_l)
                
                'Поверхностное натяжение в системе жидкость-газ'
                sigma_l = sigma_o * (1 - f_w) + sigma_w * f_w
                
                'Вязкость жидкости'
                mu_l = PVT.mu_o * (1 - f_w) + PVT.mu_w * f_w
                
                'Вязкость жидкости при отсутствии проскальзывания'
                mu_n = mu_l * lambda_l + PVT.mu_g * (1 - lambda_l)
                
                'Приведенные скорости'
                v_sl = 0.000011574 * q_l / a_p
                v_sg = 0.000011574 * q_g / a_p
                v_m = v_sl + v_sg                
                
                if grad_Calc == 1:
                    """Блок расчёта градиента давления по методике Ansari """
                    A = ansari()
                    A.Flow(v_sl, v_sg, rho_l, rho_g, mu_l * 10**(-3), PVT.mu_g * 10**(-3), teta, d, sigma_l, ksi)
                    if A.fpat == "slug":
                        A.slug(v_sl, v_sg, rho_l, rho_g, A.fpat, mu_l * 10**(-3), PVT.mu_g * 10**(-3), teta, d, sigma_l, ksi)
                        fpat_massiv = 4
                    elif A.fpat == "dbub" or A.fpat == "bub":
                        A.bubble(v_sl, v_sg, rho_l, rho_g, A.fpat, mu_l * 10**(-3), PVT.mu_g * 10**(-3), teta, d, sigma_l, ksi)
                    elif A.fpat == "annular":
                        fpat_massiv = 5
                    elif A.fpat == "dbub":
                        fpat_massiv = 2
                    elif A.fpat == "bub":
                        fpat_massiv = 3
                    elif A.fpat == "singleG":
                        A.single(teta * pi / 180, d, ksi / d, v_sg, rho_g, PVT.mu_g, p_pvt)
                        fpat_massiv = 1
                    elif A.fpat == "singleL":
                        A.single(teta * pi / 180, d, ksi / d, v_sl, rho_l, mu_l, p_pvt)
                        fpat_massiv = 1
                    'elif A.fpat == "annular":' 
                    '    A.annular(v_sl, v_sg, rho_l, deng, A.fpat, mu_l, PVT.mu_g, teta, d, sigma_l, ksi)' 
                    ' Расчёт A.pgt - уже произведен в Flow, вызывать дополнительно не требуется' 
                    """ Выходом будет являться A.pgt - Общий градиент давления"""
                    delta_p = sign * A.pgt * 10 ** (-5) * delta_l

                if grad_Calc == 2:
                    """Блок расчёта градиента давления по методике Beggs Brill """
                    B = BeggsBrillGradient()
                    B.CalcBeggsBrillGradient(v_sl, v_sg, rho_l, rho_g, mu_l, PVT.mu_g, teta, d, sigma_l, ksi, v_m, rho_n, mu_n, Payne_et_all_holdup, Payne_et_all_friction, slip, lambda_l)
                    fpat_massiv = B.fpat + 1 
                    'flow_pattern - flow pattern (0 -Расслоенный, 1 - Перемежающийся, 2 - Распределенный, 3- Переходный) [+ 1 для графика]'                    
                    delta_p = sign * B.pgt * delta_l
                
                
                if grad_Calc == 3:
                    """Блок расчёта градиента давления по методике Beggs Brill """
                    M = Saharov_Mokhov_Gradient()
                    M.Calc_Saharov_Mokhov_Gradient(v_sl, v_sg, rho_l, rho_o, rho_g,  teta, d, sigma_o, ksi, v_m, rho_n, mu_n, lambda_l)
                    fpat_massiv = None 
                    'flow_pattern - flow pattern (None)'                    
                    delta_p = sign * M.pgt * delta_l
                
                if grad_Calc == 4:
                    """Блок расчёта градиента давления по методике Gray """
                    Gr = GrayGradient()
                    Gr.CalcGray(v_sl, v_sg, rho_l, rho_g, teta, d, sigma_l, ksi, v_m, rho_n, mu_n)
                    fpat_massiv = None 
                    'flow_pattern - flow pattern (None)'                    
                    delta_p = sign * Gr.pgt * delta_l                
                
                if grad_Calc == 5:
                    """Блок расчёта градиента давления по методике HagedornBrown"""
                    HB = HagedornBrownGradient()
                    HB.CalcHagedornBrownGradient(v_sl, v_sg, rho_l, rho_g, mu_l, PVT.mu_g, teta, d, sigma_l, ksi, v_m, rho_n, lambda_l, P)
                    fpat_massiv = None 
                    'flow_pattern - flow pattern (None)'                    
                    delta_p = sign * HB.pgt * delta_l                
                
                if grad_Calc == 6:
                    """Блок расчёта градиента давления по методике Gron"""
                    Gn = GronGradient()
                    Gn.CalcGronGradient(v_sl, v_sg, rho_l, rho_o, rho_g, mu_l, PVT.mu_g, teta, d, ksi, v_m, rho_n, mu_n, lambda_l)
                    fpat_massiv = Gn.fpat 
                    'flow_pattern - flow pattern (1 - пробковая; 2 - кольцевая)'                    
                    delta_p = sign * Gn.pgt * delta_l                
                counter = counter + 1
                if P<=1:
                    delta_p = 0
                    P = 1
                    p_pvt = 1
            index = index + 1
            P = P + delta_p
            T = T + delta_t
            L = L + delta_l
            if grad_Calc ==1:
                gradP.append(A.pgt*10**(-5))            
            if grad_Calc ==2:
                gradP.append(B.pgt)
            if grad_Calc ==3:
                gradP.append(M.pgt)
            if grad_Calc ==4:
                gradP.append(Gr.pgt)
            if grad_Calc ==5:
                gradP.append(HB.pgt)
            if grad_Calc ==6:
                gradP.append(Gn.pgt)                
            md_n.append(L)
            p_n.append(P)
            t_n.append(T)
            fp_m.append(fpat_massiv)
            
            '''Выводим PVT свойства'''            
            p_b_m.append(PVT.p_b * 10) # [атм]
            r_s_m.append(PVT.r_s) # [м3/м3]
            b_o_m.append(PVT.b_o) # [м3/м3]
            mu_o_m.append(PVT.mu_o) # [сПз]
            z_m.append(PVT.z) # [д. ед.]
            b_g_m.append(PVT.b_g) # [м3/м3]
            mu_g_m.append(PVT.mu_g) # [сПз]
            b_w_m.append(PVT.b_w) # [м3/м3]
            mu_w_m.append(PVT.mu_w) # [сПз]
        self.md_n = md_n
        self.p_n = p_n
        self.gradP = gradP
        self.t_n = t_n
        self.fp_m = fp_m
        self.p_b_m = p_b_m
        self.r_s_m = r_s_m
        self.b_o_m = b_o_m
        self.mu_o_m = mu_o_m
        self.z_m = z_m
        self.b_g_m = b_g_m
        self.mu_g_m = mu_g_m
        self.b_w_m = b_w_m
        self.mu_w_m = mu_w_m
class ansari:
    def Flow(self, vsl, vsg, denl, deng, visl, visg, teta, d, sigmal, ksi): 
        lyambdal = vsl / (vsl + vsg)
        pgc = 0 # для выхода, если fpat не annular        
        if lyambdal == 0:
            fpat = "singleG"    
        else:    
            if lyambdal >= 0.9999:
                fpat = "singleL"    
            else:
                dentp = denl * lyambdal + deng * (1 - lyambdal)
                vistp = visl * lyambdal + visg * (1 - lyambdal)
                vtp = vsl + vsg
                NRetp = dentp * vtp * d / vistp
                if NRetp < 2000: 
                    ftp = 64 / NRetp
                else:
                    myfunc = lambda ftp: 1.74-2*log(2*ksi/d+18.7/NRetp/sqrt(ftp),10)-1/sqrt(ftp)     
#                    dermyfunc = lambda ftp: 1/(2*ftp**(3/2)) + 18.7/(NRetp*ftp**(3/2)*(2*ksi/d + 18.7/(NRetp*sqrt(ftp)))*log(10))
#                    ftp = newton(myfunc, x0 = 0.02, fprime = dermyfunc, tol=1e-12, maxiter=50)
                    ftp = newton(myfunc, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
                
                'Переход "Рассеяный пузырьковый режим" - "Пузырьковый режим", кривая А карты режимов потока'
                if 2*(0.4*sigmal/(denl-deng)/g)**0.5*(denl/sigmal)**(3/5)*(ftp/2/d)**(2/5)*(vsl+vsg)**1.2==0.725+4.15*(vsg/(vsg+vsl))**0.5:
                    fpat = "dbub"
                else:
                    'проверяем условие перехода к кольцевому режиму'
                    vsga = 3.1*(g*sigmal*(denl-deng)/deng**2)**(1/4)
                    vcrit = 10000 * vsg * visg / sigmal * (deng / denl) ** (1 / 2)    
                    Fe = 1 - exp( - 0.125 * (vcrit - 1.5))
                    if Fe <0:
                        Fe = 0  # Предохранитель, Fe - часть общего объема жидкости, захваченная потоком газа (не может быть меньше 0)
                    lyambdalc = Fe * vsl / (Fe * vsl + vsg)
                    denc = denl * lyambdalc + deng * (1 - lyambdalc)
                    vissc = visl * lyambdalc + visg * (1 - lyambdalc)
                    vsc = Fe * vsl + vsg
                    NResc = denc * vsc * d / vissc
                    if NResc < 2000: 
                        '''поток ламинарный'''
                        fsc = 64 / NResc
                    else:
                        myfunc2 = lambda fsc: 1.74-2*log(2*ksi/d+18.7/NResc/sqrt(fsc),10)-1/sqrt(fsc)     
#                        dermyfunc2 = lambda fsc: 1/(2*fsc**(3/2)) + 18.7/(NResc*fsc**(3/2)*(2*ksi/d + 18.7/(NResc*sqrt(fsc)))*log(10))
#                        fsc = newton(myfunc2, x0 = 0.02, fprime = dermyfunc2, tol=1e-12, maxiter=50)
                        fsc = newton(myfunc2, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
                    
                    NResl = denl * vsl * d / visl
                    if NResl < 2000: 
                        '''поток ламинарный'''
                        fsl = 64 / NResl
                    else:
                        myfunc = lambda fsl: 1.74-2*log(2*ksi/d+18.7/NResl/sqrt(fsl),10)-1/sqrt(fsl)     
#                        dermyfunc = lambda fsl: 1/(2*fsl**(3/2)) + 18.7/(NResl*fsl**(3/2)*(2*ksi/d + 18.7/(NResl*sqrt(fsl)))*log(10))
#                        fsl = newton(myfunc, x0 = 0.02, fprime = dermyfunc, tol=1e-12, maxiter=50)
                        fsl = newton(myfunc, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
                    pgsc = fsc * denc * (vsc) ** 2 / (2 * d)
                    pgsl = fsl * denl * vsl ** 2 / (2 * d)
                    B = (1 - Fe) ** 2 * 1 # где 1 - это (ff/fsl) = 1 по примеру в Мукерджи Брилл, а как иначе найти ff?
                    xm = sqrt(B * pgsl / pgsc)
                    ym = g * sin(teta * pi / 180) * (denl - denc) / pgsc
                    if Fe > 0.9:
                        myfunc3 = lambda dm: ym-(1+300*dm)/(4*dm*(1-dm)*(1-4*dm*(1-dm))**2.5)+xm**2/(4*dm*(1-dm))**3 
#                        dermyfunc3 = lambda dm: -(-20.0*dm + 10.0)*(300*dm + 1)*(-4*dm*(-dm + 1) + 1)**(-3.5)/(4*dm*(-dm + 1)) - 75*(-4*dm*(-dm + 1) + 1)**(-2.5)/(dm*(-dm + 1)) - (300*dm + 1)*(-4*dm*(-dm + 1) + 1)**(-2.5)/(4*dm*(-dm + 1)**2) + (300*dm + 1)*(-4*dm*(-dm + 1) + 1)**(-2.5)/(4*dm**2*(-dm + 1)) + 3*xm**2/(64*dm**3*(-dm + 1)**4) - 3*xm**2/(64*dm**4*(-dm + 1)**3)
        #                dermyfunc3 = lambda dm: float(-Decimal(-20.0*dm + 10.0)*Decimal(300*dm + 1)*Decimal(-4*dm*(-dm + 1) + 1)**Decimal(-3.5)/Decimal(4*dm*(-dm + 1)) - 75*Decimal(-4*dm*(-dm + 1) + 1)**Decimal(-2.5)/Decimal(dm*(-dm + 1)) - Decimal(300*dm + 1)*Decimal(-4*dm*(-dm + 1) + 1)**Decimal(-2.5)/Decimal(4*dm*(-dm + 1)**2) + Decimal(300*dm + 1)*Decimal(-4*dm*(-dm + 1) + 1)**Decimal(-2.5)/Decimal(4*dm**2*(-dm + 1)) + Decimal(3*xm)**Decimal(2)/Decimal(64*dm**3*(-dm + 1)**4) - Decimal(3*xm)**Decimal(2)/Decimal(64*dm**4*(-dm + 1)**3))
#                        dm = newton(myfunc3, x0 = 0.02, fprime = dermyfunc3, tol=1e-12, maxiter=50)
                        dm = newton(myfunc3, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
                        Z = 1 + 300 * dm
                    elif Fe < 0.9:
                        myfunc3 = lambda dm: ym-(1 + 24 * (denl / deng) ** (1 / 3) * dm)/(4*dm*(1-dm)*(1-4*dm*(1-dm))**2.5)+xm**2/(4*dm*(1-dm))**3 
        #                dermyfunc3 = lambda dm: -6*(denl/deng)**0.333333333333333*(-4*dm*(-dm + 1) + 1)**(-2.5)/(dm*(-dm + 1)) - (-20.0*dm + 10.0)*(24*dm*(denl/deng)**0.333333333333333 + 1)*(-4*dm*(-dm + 1) + 1)**(-3.5)/(4*dm*(-dm + 1)) - (24*dm*(denl/deng)**0.333333333333333 + 1)*(-4*dm*(-dm + 1) + 1)**(-2.5)/(4*dm*(-dm + 1)**2) + (24*dm*(denl/deng)**0.333333333333333 + 1)*(-4*dm*(-dm + 1) + 1)**(-2.5)/(4*dm**2*(-dm + 1)) + 3*xm**2/(64*dm**3*(-dm + 1)**4) - 3*xm**2/(64*dm**4*(-dm + 1)**3)
                        dm = newton(myfunc3, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
                        Z = 1 + 24 * (denl / deng) ** (1 / 3) * dm
                    hlf=4*dm*(1-dm) 
                    pgc = Z / (1 - 2 * dm) ** 5 * pgsc + denc * g * sin (teta * pi / 180) 
                
                    if vsg<=vsga and (hlf+lyambdalc*(d-2*dm)**2/d**2)>0.12:
                        'по обоим критериям не кольцевой режим, проверим 2 критерия: может ли быть пузырьковый, и выполняется ли переход из пузырькового в пробковый'                
                        dmin=19.01*((denl-deng)*sigmal/denl**2/g)**(1/2)
                        vs1=1.53 * (g * sigmal * (denl - deng) / denl**2)**0.25
                        vsl1=3*(vsg-0.25*vs1*sin(teta * pi / 180))
                        
                        if dmin<d and vsl1<vsl:     
                            fpat="bub"
                        else:
                            fpat="slug"
                    elif vsg>vsga and (hlf+lyambdalc*(d-2*dm)**2/d**2)>0.12:   
                        'по первому критерию кольцевой, но по дополнительному - пленка не стабильна, пока предполагаем не кольцевой'
                        dmin=19.01*((denl-deng)*sigmal/denl**2/g)**(1/2)
                        vs1=1.53 * (g * sigmal * (denl - deng) / denl**2)**0.25
                        vsl1=3*(vsg-0.25*vs1*sin(teta * pi / 180))
                        
                        if dmin<d and vsl1<vsl:     
                            fpat="bub"
                        else:
                            fpat="slug"
                    else:    
                         fpat = "annular"
                         '''Градиент давления в плёнке и в газовом ядре одинаковые'''
        self.pgt = pgc        
        self.fpat = fpat
    
    def single(self, angr, di, ed, v, den, vis, P):
        pge = den * sin(angr) * g
        RE = di * den * v / vis
        ff = 64 / RE
        if (RE > 2000):
            ff = 0.25 / (Log10(2 * ed / 3.7 - 5.02 / RE * Log10(2 * ed / 3.7 + 13 / RE))) ** 2
        pgf = 0.5 * den * ff * v * v / di
        ekk = den * v * v / P
        icrit = 0
        if (ekk > 0.95): 
            icrit = 1
        if (icrit == 1): 
            ekk = 0.95
        pgt = (pge + pgf) / (1 - ekk)
        pga = pgt * ekk
        pgt = (pge + pgf)    
        self.pge = pge; self.pgf = pgf; self.pga = pga; self.pgt = pgt        
        
    def bubble(self, vsl, vsg, denl, deng, fpat, visl, visg, teta, d, sigmal, ksi): 
        lyambdal = vsl / (vsl + vsg)            
        vm = vsl + vsg    #Rkh    
        if fpat == "dbub": 
            dentp = denl * lyambdal + deng * (1 - lyambdal)
            vistp = visl * lyambdal + visg * (1 - lyambdal)
            vtp = vsl + vsg # = vm
        '''Для учёта эффекта проскальзывания в аэрированном потоке, скорость
           подъема пузырьков газа связывают со скоростью смеси.
           Допуская существование турбулентного течения, можно предполагать,
           что концентрация поднимающихся пузырьков будет больше в центре трубы'''    
        if fpat == "bub":
            '''Неявно вычислим фактическое объемное содержание жидкости в аэрированном потоке'''              
            myfunc = lambda hl: 1.53 * (g * sigmal * (denl - deng) / denl**2)**0.25*hl**0.5-vsg/(1-hl)+1.2*vm     
#            dermyfunc = lambda hl: 0.765*hl**(-0.5)*(g*sigmal*(-deng + denl)/denl**2)**0.25 - vsg/(-hl + 1)**2 - vsl/hl**2
#            hl = newton(myfunc, x0 = 0.5, fprime = dermyfunc, tol=1e-12, maxiter=50)    
            hl = newton(myfunc, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
            dentp = denl * hl + deng * (1 - hl)
            vistp = visl * hl + visg * (1 - hl)
            vtp = vsl + vsg # = vm # Rkh
        pge = dentp * g * sin(teta * pi / 180)
        NRetp = dentp * vtp * d / vistp
        if NRetp < 2000: 
            '''поток ламинарный'''
            ftp = 64 / NRetp
        else:
            myfunc2 = lambda ftp: 1.74-2*log(2*ksi/d+18.7/NRetp/sqrt(ftp),10)-1/sqrt(ftp)     
#            dermyfunc2 = lambda ftp: 1/(2*ftp**(3/2)) + 18.7/(NRetp*ftp**(3/2)*(2*ksi/d + 18.7/(NRetp*sqrt(ftp)))*log(10))
#            ftp = newton(myfunc2, x0 = 0.02, fprime = dermyfunc2, tol=1e-12, maxiter=50)
            ftp = newton(myfunc2, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
        pgf = ftp * dentp * (vtp) ** 2 / (2 * d)
        '''Поскольку в пузырьковом режиме доминирует жидкая фаза, которая
           является относительно несжимаемой, значительного изменения плотности
           флюидов не происходит. Скорость флюида остается постоянной, 
           поэтому падения давления вследствии ускорения не наблюдается.
           Составляющей по ускорению можно пренебречь.'''
        pga = 0
        pgt = pge + pgf + pga
        self.pge = pge; self.pgf = pgf; self.pga = pga; self.pgt = pgt
                    
        
         
    def slug(self, vsl, vsg, denl, deng, fpat, visl, visg, teta, d, sigmal, ksi): 
        vm = vsl + vsg # Rkh        
        vtb = 1.2 * vm + 0.35 * (g * d * (denl - deng) / denl) ** 0.5
        hgls = vsg / (0.425 + 2.65 * vm)
        hlls = 1 - hgls        
        vgls = 1.2 * vm + 1.53 * (g * sigmal * (denl - deng) / (denl) ** 2) ** (1 / 4) * (hlls) ** 0.5
        A = hgls*(vtb-vgls)+vm
        myfunc = lambda hltb: (9.916*sqrt(g*d))*(1-sqrt(1-hltb))**0.5*hltb-vtb*(1-hltb)+A     
#        dermyfunc = lambda hltb: 2.479*hltb*sqrt(d*g)*(-sqrt(-hltb + 1) + 1)**(-0.5)/sqrt(-hltb + 1) + vtb + 9.916*sqrt(d*g)*(-sqrt(-hltb + 1) + 1)**0.5
#        hltb = newton(myfunc, x0 = 0.5, fprime = dermyfunc, tol=1e-12, maxiter=50)    
        hltb = newton(myfunc, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
        
        #hgtb = 1 - hltb
        #vltb = 9.916 * (g * d * (1 - sqrt(hgtb))) ** (1 / 2)
        #vlls = (vtb * hlls - (vtb + vltb) * hltb) / hlls
        vgtb = (vtb * (1 - hltb) - (vtb - vgls) * (1 - hlls)) / (1 - hltb)
        beta = (vsg - vgls * (1 - hlls)) / (vgtb - vgtb * hltb - vgls * (1 - hlls))
        ''' ? принимается Lls = 30d ? - длина пробки жидкости'''
        #lls = 30 * d
        #lsu = lls / (1 - beta)
        #ltb = lsu - lls
        denls = denl * hlls + deng * (1 - hlls)
        pge = ((1 - beta) * denls + beta * deng) * g * sin(teta * pi / 180)
        visls = visl * hlls + visg * (1 - hlls)
        NRels = denls * vm * d / visls
        if NRels < 2000: 
            '''поток ламинарный'''
            fls = 64 / NRels
        else:
            myfunc2 = lambda fls: 1.74-2*log(2*ksi/d+18.7/NRels/sqrt(fls),10)-1/sqrt(fls)     
#            dermyfunc2 = lambda fls: 1/(2*fls**(3/2)) + 18.7/(NRels*fls**(3/2)*(2*ksi/d + 18.7/(NRels*sqrt(fls)))*log(10))
#            fls = newton(myfunc2, x0 = 0.02, fprime = dermyfunc2, tol=1e-12, maxiter=50)
            fls = newton(myfunc2, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
        pgf = fls * denls * (vm) ** (2) / (2 * d) * (1 - beta)
        '''составляющей градиента давления по ускорению пренебрегаем'''
        pga = 0
        pgt = pge + pgf + pga
        self.pge = pge; self.pgf = pgf; self.pga = pga; self.pgt = pgt
             
                
                
    def annular(self, vsl, vsg, denl, deng, fpat, visl, visg, teta, d, sigmal, ksi):
        vcrit = 10000 * vsg * visg / sigmal * (deng / denl) ** (1 / 2)
        Fe = 1 - exp( - 0.125 * (vcrit - 1.5))
        lyambdalc = Fe * vsl / (Fe * vsl + vsg)
        denc = denl * lyambdalc + deng * (1 - lyambdalc)
        NResl = denl * vsl * d / visl
        if NResl < 2000: 
            '''поток ламинарный'''
            fsl = 64 / NResl
        else:
            myfunc = lambda fsl: 1.74-2*log(2*ksi/d+18.7/NResl/sqrt(fsl),10)-1/sqrt(fsl)     
#            dermyfunc = lambda fsl: 1/(2*fsl**(3/2)) + 18.7/(NResl*fsl**(3/2)*(2*ksi/d + 18.7/(NResl*sqrt(fsl)))*log(10))
#            fsl = newton(myfunc, x0 = 0.02, fprime = dermyfunc, tol=1e-12, maxiter=50)
            fsl = newton(myfunc, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
        pgsl = fsl * denl * vsl ** 2 / (2 * d)
        vsc = Fe * vsl + vsg
        vissc = visl * lyambdalc + visg * (1 - lyambdalc)
        NResc = denc * vsc * d / vissc
        if NResc < 2000: 
            '''поток ламинарный'''
            fsc = 64 / NResc
        else:
            myfunc2 = lambda fsc: 1.74-2*log(2*ksi/d+18.7/NResc/sqrt(fsc),10)-1/sqrt(fsc)     
#            dermyfunc2 = lambda fsc: 1/(2*fsc**(3/2)) + 18.7/(NResc*fsc**(3/2)*(2*ksi/d + 18.7/(NResc*sqrt(fsc)))*log(10))
#            fsc = newton(myfunc2, x0 = 0.02, fprime = dermyfunc2, tol=1e-12, maxiter=50)
            fsc = newton(myfunc2, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
        pgsc = fsc * denc * (vsc) ** 2 / (2 * d)
        B = (1 - Fe) ** 2 * 1 # где 1 - это (ff/fsl) = 1 по примеру в Мукерджи Брилл, а как иначе найти ff?
        xm = sqrt(B * pgsl / pgsc)
        ym = g * sin(teta * pi / 180) * (denl - denc) / pgsc
        if Fe > 0.9:
            myfunc3 = lambda dm: ym-(1+300*dm)/(4*dm*(1-dm)*(1-4*dm*(1-dm))**2.5)+xm**2/(4*dm*(1-dm))**3 
#            dermyfunc3 = lambda dm: -(-20.0*dm + 10.0)*(300*dm + 1)*(-4*dm*(-dm + 1) + 1)**(-3.5)/(4*dm*(-dm + 1)) - 75*(-4*dm*(-dm + 1) + 1)**(-2.5)/(dm*(-dm + 1)) - (300*dm + 1)*(-4*dm*(-dm + 1) + 1)**(-2.5)/(4*dm*(-dm + 1)**2) + (300*dm + 1)*(-4*dm*(-dm + 1) + 1)**(-2.5)/(4*dm**2*(-dm + 1)) + 3*xm**2/(64*dm**3*(-dm + 1)**4) - 3*xm**2/(64*dm**4*(-dm + 1)**3)
#            dm = newton(myfunc3, x0 = 0.02, fprime = dermyfunc3, tol=1e-12, maxiter=50)
            dm = newton(myfunc3, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
            Z = 1 + 300 * dm
        elif Fe < 0.9:
            myfunc3 = lambda dm: ym-(1 + 24 * (denl / deng) ** (1 / 3) * dm)/(4*dm*(1-dm)*(1-4*dm*(1-dm))**2.5)+xm**2/(4*dm*(1-dm))**3 
#            dermyfunc3 = lambda dm: -6*(denl/deng)**0.333333333333333*(-4*dm*(-dm + 1) + 1)**(-2.5)/(dm*(-dm + 1)) - (-20.0*dm + 10.0)*(24*dm*(denl/deng)**0.333333333333333 + 1)*(-4*dm*(-dm + 1) + 1)**(-3.5)/(4*dm*(-dm + 1)) - (24*dm*(denl/deng)**0.333333333333333 + 1)*(-4*dm*(-dm + 1) + 1)**(-2.5)/(4*dm*(-dm + 1)**2) + (24*dm*(denl/deng)**0.333333333333333 + 1)*(-4*dm*(-dm + 1) + 1)**(-2.5)/(4*dm**2*(-dm + 1)) + 3*xm**2/(64*dm**3*(-dm + 1)**4) - 3*xm**2/(64*dm**4*(-dm + 1)**3)
            dm = newton(myfunc3, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
            Z = 1 + 24 * (denl / deng) ** (1 / 3) * dm
        pgc = Z / (1 - 2 * dm) ** 5 * pgsc + denc * g * sin (teta * pi / 180) 
        '''Градиент давления в плёнке и в газовом ядре одинаковые'''
        self.pgt = pgc


class BeggsBrillGradient:
    def CalcBeggsBrillGradient(self, V_sl, V_sg, rho_l, rho_g, mu_l, mu_g, theta, d, sigma_l, eps, v_m, rho_n, mu_n, Payne_et_all_holdup, Payne_et_all_friction, slip, lambda_l):
        '''
        pgt [атм/м] - общий градиент давления
        d - внутренний диаметр трубы [м]
        theta - угол наклона трубы [град]
        eps - шероховатость стенки трубы [м]
        p - опорное давление [атм]
        mu_o [сПз]
        mu_w [сПз]
        mu_g [сПз]
        '''
        'Переводной коэффициент из Па -> атм'
        c_p = 0.000009871668
        'Число Рейнольдса'
        n_re = 1000 * rho_n * v_m * d / mu_n
        'Число Фруда'
        n_fr = v_m ** 2 / (g * d)
        'Число скорости жидкости'
        n_lv = V_sl * (rho_l / (g* sigma_l)) ** 0.25
        'Относительная шероховатость трубы'
        e = eps / d
        'Определяем режим течения'
        if (n_fr >= 316 * lambda_l ** 0.302 or n_fr >= 0.5 * lambda_l ** -6.738):
          flow_pattern = 2
        else:
          if (n_fr <= 0.000925 * lambda_l ** -2.468):
            flow_pattern = 0
          else:
            if (n_fr <= 0.1 * lambda_l ** -1.452):
              flow_pattern = 3
            else:
              flow_pattern = 1
        if (flow_pattern == 0 or flow_pattern == 1 or flow_pattern == 2):
            h_l = h_l_thetaF(flow_pattern, lambda_l, n_fr, n_lv, theta, Payne_et_all_holdup) 
        else:
            l_2 = 0.000925 * lambda_l ** -2.468
            l_3 = 0.1 * lambda_l ** -1.452
            aa = (l_3 - n_fr) / (l_3 - l_2)
            h_l = aa * h_l_thetaF(0, lambda_l, n_fr, n_lv, theta, Payne_et_all_holdup) + (1 - aa) * h_l_thetaF(1, lambda_l, n_fr, n_lv, theta, Payne_et_all_holdup)
        'Нормализованный коэффициент трения'
        f_n = calc_friction_factor(n_re, e, Payne_et_all_friction) 
        y = max(lambda_l / h_l ** 2, 0.001)
        if (y > 1 and y < 1.2):
            s = log(2.2 * y - 1.2)
        else:
            s = log(y) / (-0.0523 + 3.182 * log(y) - 0.8725 * (log(y)) ** 2 + 0.01853 * (log(y)) ** 4)
        'Коэффициент трения'
        f = f_n * exp(s)
        'Плотность смеси'
        rho_s = rho_l * h_l + rho_g * (1 - h_l)
        'Градиент давления за счет гравитации'
        dpdl_g = c_p * rho_s * g * sin(pi / 180 * theta)
        'Градиент давления за счет трения'
        dpdl_f = c_p * f * rho_n * v_m ** 2 / (2 * d)
        'Градиент давления за счет гравитации, без проскальзывания газа'
        dpdl_g1 = c_p * rho_n * g * sin(pi / 180 * theta)
        'Градиент давления за счет трения, без проскальзывания газа'
        dpdl_f1 = c_p * f * rho_n * v_m ** 2 / (2 * d)
        if slip == 1:
            BegsBrillGradient = dpdl_g + dpdl_f
        if slip == 0:
            BegsBrillGradient = dpdl_g1 + dpdl_f1
        self.pgt = BegsBrillGradient
        self.fpat = flow_pattern


def h_l_thetaF(flow_pattern, lambda_l, n_fr, n_lv, theta, Payne_et_all):
    'Объемное содержание жидкости'
    'flow_pattern - flow pattern (0 -Расслоенный, 1 - Перемежающийся, 2 - Распределенный, 3- Переходный)'
    a = []
    a= (0.98, 0.845, 1.065)
    b = []
    b = (0.4846, 0.5351, 0.5824)
    c = []
    c = (0.0868, 0.0173, 0.0609)
    e = []
    e = (0.011, 2.96, 1)
    f = []
    f = (-3.768, 0.305, 0)
    j = []
    j = (3.539, -0.4473, 0)
    h = []
    h = (-1.614, 0.0978, 0)
    
    'Объемное содержание жидкости для горизонтального потока'
    h_l_0 = a[flow_pattern] * lambda_l ** b[flow_pattern] / n_fr ** c[flow_pattern]
    
    'Коррекция на угол наклона'
    cc = max(0, (1 - lambda_l) * log(e[flow_pattern] * lambda_l ** f[flow_pattern] * n_lv ** j[flow_pattern] * n_fr ** h[flow_pattern]))
    
    'Конвертация град в рад'
    theta_d = pi / 180 * theta
    
    psi = 1 + cc * (sin(1.8 * theta_d) + 0.333 * (sin(1.8 * theta_d)) ** 3)
    
    'Объемное содержание жидкости с модификацией payne et al. correction factor'
    if Payne_et_all > 0:
        if theta > 0: 
            'восходящее течение'
            h_l_theta = Max(Min(1, 0.924 * h_l_0 * psi), lambda_l)
        else:  
            'нисходящее течение'
            h_l_theta = max(min(1, 0.685 * h_l_0 * psi), lambda_l)
    else:
        h_l_theta = max(min(1, h_l_0 * psi), lambda_l)
    return h_l_theta

def calc_friction_factor(n_re, e, Rough_pipe):
    if n_re > 2000: 
        'турбулентное течение'
        if Rough_pipe > 0: 
            'Коэффициент трения для шероховатых труб по методу Moody - Payne et all для корреляции Beggs Brill'
            f_n = (2 * log10(2 / 3.7 * e - 5.02 / n_re * log10(2 / 3.7 * e + 13 / n_re))) ** -2
            i = 0
            f_n_new = f_n
            f_int = -1
            while abs(f_n_new - f_int) > 0.001:
                f_n_new = (1.74 - 2 * log10(2 * e + 18.7 / (n_re * f_n ** 0.5))) ** -2
                i = i + 1
                f_int = f_n
                f_n = f_n_new
        else: 
            'Коэффициент трения для гладких труб используя корреляцию Drew - оригинального Begs&Brill без модификаций'
            f_n = 0.0056 + 0.5 * n_re ** -0.32
    else: 
        'ламинарный поток'
        f_n = 64 / n_re
    calc_friction_factor = f_n
    return calc_friction_factor

class Saharov_Mokhov_Gradient:
    def Calc_Saharov_Mokhov_Gradient(self, V_sl, V_sg, rho_l, rho_o, rho_g,  theta, d, sigma_o, eps, v_m, rho_n, mu_n, lambda_l):
        '''
        pgt [атм/м] - общий градиент давления
        d - внутренний диаметр трубы [м]
        theta - угол наклона трубы [град]
        eps - шероховатость стенки трубы [м]
        p - опорное давление [атм]
        mu_o [сПз]
        mu_w [сПз]
        mu_g [сПз]
        '''
        'Переводной коэффициент из Па -> атм'
        c_p = 0.000009871668
        'Число Рейнольдса'
        n_re = 1000 * rho_n * v_m * d / mu_n
        'Число Фруда'
        n_fr = v_m ** 2 / (g * d)
        
        'Относительная шероховатость трубы'
        e = eps / d
        delta_rho = rho_o - rho_g
        We = sigma_o / (delta_rho * d ** 2 * g)
        Ku = ((rho_l ** 2) / (delta_rho ** 2) * (n_fr ** 2) / We) ** (1 / 4)
        f_mohov = (0.13 * Ku + 1) / (1.13 * Ku + 1) * delta_rho / rho_l * 2 * (1 - lambda_l) / n_fr * sin(pi / 180 * theta) + 0.11 * (68 / n_re + e / d) ** 0.25
               
        'Градиент давления за счет гравитации'
        dpdl_g = c_p * rho_n * g * sin(pi / 180 * theta)
        'Градиент давления за счет трения'
        dpdl_f = c_p * f_mohov * rho_n * v_m ** 2 / (2 * d)
        self.pgt = dpdl_g + dpdl_f
        self.fpat = None

class GrayGradient:
    def CalcGray(self, V_sl, V_sg, rho_l, rho_g, theta, d, sigma_l, eps, v_m, rho_n, mu_n):
        '''
        pgt [атм/м] - общий градиент давления
        d - внутренний диаметр трубы [м]
        theta - угол наклона трубы [град]
        eps - шероховатость стенки трубы [м]
        p - опорное давление [атм]
        mu_n [сПз]
        '''
        'Переводной коэффициент из Па -> атм'
        c_p = 0.000009871668
        'Число Рейнольдса'
        n_re = 1000 * rho_n * v_m * d / mu_n
        'Относительная шероховатость трубы'
        e = eps / d
        R = V_sl / V_sg
        e1 = 28.5 * sigma_l / (rho_n * v_m ** 2)
        if R >= 0.007:
            e = e1
        if R < 0.007: 
            e = e + R * (e1 - e) / 0.0007
        Nv = rho_n ** 2 * v_m ** 4 / (g * sigma_l * (rho_l - rho_g))
        Nd = g * (rho_l - rho_g) * d ** 2 / sigma_l
        b = 0.0814 * (1 - 0.0554 * log(1 + 730 * R / (R + 1)))
        h_l = (R + exp(-2.314 * (Nv * (1 + 205 / Nd)) ** b)) / (R + 1)        
        f_n = calc_friction_factor(n_re, e, 1)
        'Плотность смеси'
        rho_s = rho_l * h_l + rho_g * (1 - h_l)
        'Градиент давления за счет гравитации'
        dpdl_g = c_p * rho_s * g * sin(pi / 180 * theta)
        'Градиент давления за счет трения'
        dpdl_f = c_p * f_n * rho_n * v_m ** 2 / (2 * d)
        self.pgt = dpdl_g + dpdl_f
        self.fpat = None

class HagedornBrownGradient:
    def CalcHagedornBrownGradient(self, V_sl, V_sg, rho_l, rho_g, mu_l, mu_g, theta, d, sigma_l, eps, v_m, rho_n, lambda_l, P):
        '''
        pgt [атм/м] - общий градиент давления
        d - внутренний диаметр трубы [м]
        theta - угол наклона трубы [град]
        eps - шероховатость стенки трубы [м]
        p - опорное давление [атм]
        mu_o [сПз]
        mu_w [сПз]
        mu_g [сПз]
        '''
        'Переводной коэффициент из Па -> атм'
        c_p = 0.000009871668
        a0 = -2.69851
        a1 = 0.1584095
        a2 = -0.5509976
        a3 = 0.5478492
        a4 = -0.1219458
        B0 = -0.1030658
        b1 = 0.617774
        B2 = -0.632946
        B3 = 0.29598
        B4 = -0.0401
        c0 = 0.9116257
        c1 = -4.821756
        c2 = 1232.25
        c3 = -22253.58
        c4 = 116174.3
        'Безразмерные параметры Данса и Роса'
        'Безразмерная скорость жидкости'
        n_lv = 1.938 * V_sl / 0.3048 * (rho_l * 0.06243 / (sigma_l * 1000)) ** 0.25
        'Безразмерная скорость газа'
        n_gv = 1.938 * V_sg / 0.3048 * (rho_l * 0.06243 / (sigma_l * 1000)) ** 0.25
        'Безразмерный диаметр'
        n_d = 120.872 * d / 0.3048 * (rho_l * 0.06243 / (sigma_l * 1000)) ** 0.5
        'Безразмерная вязкость жидкости'
        n_l = 0.15726 * mu_l * (1 / (rho_l * 0.06243 * (sigma_l * 1000) ** 3)) ** 0.25
        N_lc = 10 ** (a0 + a1 * (log10(n_l) + 3) + a2 * (log10(n_l) + 3) ** 2 + a3 * (log10(n_l) + 3) ** 3 + a4 * (log10(n_l) + 3) ** 4)
        a = (n_lv * N_lc) / ((n_gv ** 0.575) * n_d) * (P * 14.7 / (14.7)) ** 0.1
        aa = B0 + b1 * (log10(a) + 6) + B2 * (log10(a) + 6) ** 2 + B3 * (log10(a) + 6) ** 3 + B4 * (log10(a) + 6) ** 4
        b = (n_gv * n_l ** 0.38) / n_d ** 2.14
        psi = c0 + c1 * b + c2 * b ** 2 + c3 * b ** 3 + c4 * b ** 4
        h_l = aa * psi
        if h_l < lambda_l: 
            h_l = lambda_l
        mu_tp = mu_l ** h_l + mu_g ** (1 - h_l)
        n_re = 1000 * rho_n * v_m * d / mu_tp

        e = eps / d
        f_n = calc_friction_factor(n_re, e, 1)
        f = f_n
        'Плотность смеси'
        rho_s = rho_l * h_l + rho_g * (1 - h_l)
        'Градиент давления за счет гравитации'
        dpdl_g = c_p * rho_s * g * sin(pi / 180 * theta)
        'Градиент давления за счет трения'
        dpdl_f = c_p * f * rho_n * v_m ** 2 / (2 * d)
        self.pgt = dpdl_g + dpdl_f
        self.fpat = None

class GronGradient:
    def CalcGronGradient(self, V_sl, V_sg, rho_l, rho_o, rho_g, mu_l, mu_g, theta, d, eps, v_m, rho_n, mu_n, lambda_l):
        '''
        pgt [атм/м] - общий градиент давления
        d - внутренний диаметр трубы [м]
        theta - угол наклона трубы [град]
        eps - шероховатость стенки трубы [м]
        p - опорное давление [атм]
        mu_o [сПз]
        mu_w [сПз]
        mu_g [сПз]
        '''
        'Переводной коэффициент из Па -> атм'
        c_p = 0.000009871668
        'Число Рейнольдса'
        n_re = 1000 * rho_n * v_m * d / mu_n
        'Число Фруда'
        n_fr = v_m ** 2 / (g * d)
        'Относительная шероховатость трубы'
        e = eps / d
        if d >= 0.015 and d <= 0.0762:
            if (d > 0.06 and d < 0.067 and mu_l >= 1 and mu_l <= 450) or (d > 0.035 and d < 0.04 and mu_l >= 1 and mu_l <= 1500) or (d > 0.048 and d < 0.053 and mu_l >= 1 and mu_l <= 750) or (d > 0.0732 and d < 0.0792 and mu_l >= 1 and mu_l <= 300):
                n_re_g = 1000 * rho_g * v_m * d / mu_g
                W = (n_re_g * n_fr * rho_g / (rho_l - rho_g)) ** (1 / 3)
                W_boundary = (8.2 - 1.7 * 0.01 * (mu_g / mu_l) ** (-0.6)) * exp((8 + 62 * (mu_g / mu_l) * (1 - lambda_l)))
                if W <= W_boundary:
                    flow_pattern = 1 # Пробковая
                else: 
                    flow_pattern = 2 # Кольцевая
                C1 = (2.24 * exp(0.05 * mu_l) / (1 + 1.1 * 0.05 * mu_l) - 8 * 0.001 * (mu_l ** 0.6) * (d / 0.015 - 1))
                if mu_l <= 40:
                    C2 = (1 + 0.11 * exp(0.05 * mu_l) / (1 + 1.1 * 0.05 * mu_l) - (0.1 - 2.52 * 0.001 * (mu_l - 1)) * (d / 0.015 - 1))
                if mu_l > 40:
                    C2 = (1 + 0.11 * exp(0.05 * mu_l) / (1 + 1.1 * 0.05 * mu_l))
                f = 0.067 * (158 / n_re + 2 * e / d) ** 0.2
                Hl = lambda_l / (C1 + C2 * n_fr ** (-0.5))
                rho_s = (1 - Hl) * rho_l + Hl * rho_g        
                dpdl_g = c_p * rho_s * g * sin(pi / 180 * theta)
                dpdl_f = c_p * f * (rho_l * (V_sl ** 2) / (1 - Hl) + rho_g * (V_sg ** 2) / Hl) / (2 * d)
            else:
                dpdl_g = 0
                dpdl_f = 0
                flow_pattern = 0
        self.pgt = dpdl_g + dpdl_f
        self.fpat = flow_pattern

class Calc_PVT:
    def __init__(self, gamma_o, gamma_g, PVT_correlations, r_sb, P_rb, b_ro, s, gamma_w): 
        self.gamma_o = gamma_o; self.gamma_g = gamma_g; self.PVT_correlations = PVT_correlations; 
        self.r_sb = P_rb; self.P_rb = P_rb; self.b_ro = b_ro; self.s = s; self.gamma_w = gamma_w
    def CalcPVT(self, P, T, gamma_o, gamma_g, PVT_correlations, r_sb, P_rb, b_ro, s, gamma_w):
        'Расчёт PVT свойств нефти, газа и воды'
        'p - давление [атм]'
        't - температура [*C]'
        'gamma_o - Удельная плотность нефти по воде [доли ед.]'
        'gamma_g - Удельная плотность газа по воздуху [доли ед.]'
        'PVT_correlations - Индикатор выбора корреляций'
        
        'Переводной параметр [*C] -> [K]'
        t_offs = 273
        
        'Конвертируем температуру'
        T = T + t_offs
        
        'Переводной параметр [атм] -> [МПа]'
        c_p_mpa = 0.1013
        
        'Значение плотности для расчёта плотности нефти из входного параметра удельная плотность нефти [усл. плот. воды при стандартных условиях]'
        rho_ref = 1000
       
        'Псевдокритические давление и температура [газ]'
        
        if PVT_correlations > 0:
          P_pc = PseudoPressure(gamma_g)
          T_pc = PseudoTemperature(gamma_g)
        else:
          P_pc = PseudoPressureStanding(gamma_g)
          T_pc = PseudoTemperatureStanding(gamma_g)
        
        'Конвертируем давление'
        P_MPa = P * c_p_mpa
        
        'Конвертируем калибровочное давление насыщения'
        P_rb = P_rb * c_p_mpa
        
        'for saturated oil calibration is applied by application of factor p_fact to input pressure'
        'for undersaturated - by shifting according to p_offs'
            
        'Расчёт свойств воды к условиям опорных давления и температуры'
        b_wi = WaterFVF(P_MPa, T)
        
        mu_wi = WaterViscosity(P_MPa, T, s, gamma_w)
        
        'Если газонасыщенность при давлении насыщения отсутствует, тогда устанавливаем данный показатель аномально высоким'
        'Калибровочное давление насыщения  и калибровочный объемный коэффициент нефти при давлении насыщения рассчитываем по корреляциям'
        if (r_sb <= 0):
            r_sb = 100000
            P_rb = -1
            b_ro = -1
        
        if PVT_correlations == 0: 
            'Свойства газа'             
            p_pr = P_MPa / P_pc # приведенное давление
            T_pr = T / T_pc # приведенная температура
            Zi = ZFactorDranchuk(T_pr, p_pr) # Расчёт Z- фактора по методике Дранчука
            b_gi = GasFVF(T, P_MPa, Zi) # Объемный коэффициент газа 
            mu_gi = GVisc(T, P_MPa, Zi, gamma_g) # Вязкость газа
          
            'вязкость дегазированной нефти'
            mu_do = DeadOilViscosity_Beggs_Robinson(T, gamma_o) 
          
            'Вязкость насыщенной нефти'
            mu_o_sat = SaturatedOilViscosity_Beggs_Robinson(r_sb, mu_do) 
          
            'Давление насыщения'
            p_bi = Bubblepoint_Standing(r_sb, gamma_g, T, gamma_o) 
          
            'При заданном калибровочном давлении насыщения'
            if (P_rb > 0): 
                p_fact = p_bi / P_rb
                p_offs = p_bi - P_rb
            else: 
                p_fact = 1
                p_offs = 0
            
            'При заданном калибровочном объемном коэффициенте нефти при давлении насыщения'
            if (b_ro > 0): 
                b_o_sat = FVF_Saturated_Oil_Standing(r_sb, gamma_g, T, gamma_o)
                b_fact = (b_ro - 1) / (b_o_sat - 1)
            else: 
                b_fact = 1
          
            'Недонасыщенная нефть'
            if P_MPa > (p_bi / p_fact): 
                P_MPa = P_MPa + p_offs 
                r_si = r_sb
               
                'Корреляции Стендинга'
                b_o_sat = b_fact * (FVF_Saturated_Oil_Standing(r_si, gamma_g, T, gamma_o) - 1) + 1 #Предполагаем, что при P = 1 [атм] -> b_o = 1                 
                
                'Расчёт сжимаемости, при давлении насыщения'
                c_o = Compressibility_Oil_VB(r_sb, gamma_g, T, gamma_o, P_MPa)
                
                b_oi = b_o_sat * exp(c_o * (p_bi - P_MPa))
                
                'Vesquez&Beggs'
                mu_oi = OilViscosity_Vasquez_Beggs(mu_o_sat, P_MPa, p_bi)
          
            else: 
                'Насыщенная нефть'
                P_MPa = P_MPa * p_fact
                r_si = GOR_Standing(P_MPa, gamma_g, T, gamma_o)
                b_oi = b_fact * (FVF_Saturated_Oil_Standing(r_si, gamma_g, T, gamma_o) - 1) + 1 #Предполагаем, что при P = 1 [атм] -> b_o = 1
                mu_oi = SaturatedOilViscosity_Beggs_Robinson(r_si, mu_do)
        if PVT_correlations == 1: 
            'Свойства газа'
            p_pr = P_MPa / P_pc
            T_pr = T / T_pc
            Zi = ZFactor(T_pr, p_pr)
            b_gi = GasFVF(T, P_MPa, Zi)
            mu_gi = GVisc(T, P_MPa, Zi, gamma_g)
         
            'вязкость дегазированной нефти'
            mu_do = DeadOilViscosity_Standing(T, gamma_o)
            
            'Вязкость насыщенной нефти'
            mu_o_sat = SaturatedOilViscosity_Beggs_Robinson(r_sb, mu_do)
        
            'Давление насыщения'
            p_bi = Bubblepoint_Valko_McCainSI(r_sb, gamma_g, T, gamma_o)
            
            'При заданном калибровочном давлении насыщения'
            if (P_rb > 0):
                p_fact = p_bi / P_rb
                p_offs = p_bi - P_rb
            else:
                p_fact = 1
                p_offs = 0
          
            'Недонасыщенная нефть'
            if P_MPa > (p_bi / p_fact): 
                P_MPa = P_MPa + p_offs
            else:
                P_MPa = P_MPa * p_fact
            
            'Расчёт сжимаемости, при давлении насыщения'
            c_o = Compressibility_Oil_VB(r_sb, gamma_g, T, gamma_o, P_MPa)
                
            'При заданном калибровочном объемном коэффициенте нефти при давлении насыщения'
            if (b_ro > 0): 
                rho_o_sat = Density_McCainSI(p_bi, gamma_g, T, gamma_o, r_sb, p_bi, c_o)
                b_o_sat = FVF_McCainSI(r_sb, gamma_g, gamma_o * rho_ref, rho_o_sat)
                b_fact = (b_ro - 1) / (b_o_sat - 1)
            else:
                b_fact = 1
          
            'Газонасыщенность'
            r_si = GOR_VelardeSI(P_MPa, p_bi, gamma_g, T, gamma_o, r_sb)
            
            'Недонасыщенная нефть'
            if P_MPa > p_bi:
                rho_o_sat = Density_McCainSI(p_bi, gamma_g, T, gamma_o, r_sb, p_bi, c_o)
                b_o_sat = FVF_McCainSI(r_sb, gamma_g, gamma_o * rho_ref, rho_o_sat)
                b_o_sat = b_fact * (b_o_sat - 1) + 1 #Предполагаем, что при P = 1 [атм] -> b_o = 1
                b_oi = b_o_sat * exp(c_o * (p_bi - P_MPa))
            else:
                rho_o = Density_McCainSI(P_MPa, gamma_g, T, gamma_o, r_si, p_bi, c_o)
                b_oi = b_fact * (FVF_McCainSI(r_si, gamma_g, gamma_o * rho_ref, rho_o) - 1) + 1 #Предполагаем, что при P = 1 [атм] -> b_o = 1
        
            'Вязкость нефти'
            if (r_sb < 350): 
                mu_oi = Oil_Viscosity_Standing(r_si, mu_do, P_MPa, p_bi)
            else: 
                'Вязкость нефти - Begs&Robinson (насыщенная) and Vasquez&Begs (недонасыщенная)'
                if P_MPa > p_bi: 
                    mu_oi = OilViscosity_Vasquez_Beggs(mu_o_sat, P_MPa, p_bi)
                else:
                    mu_oi = SaturatedOilViscosity_Beggs_Robinson(r_si, mu_do)
        self.p_b = p_bi / p_fact; self.r_s = r_si; self.b_o = b_oi; 
        self.mu_o = mu_oi; self.z = Zi; self.b_g = b_gi; self.mu_g = mu_gi; self.b_w = b_wi;
        self.mu_w = mu_wi
       
       
       
def DeadOilViscosity_Standing(Temperature_K, gamma_oil):
    X = (0.32 + 1.8 * 10 ** 7 / (141.5 / gamma_oil - 131.5) ** 4.53) * (360 / (1.8 * Temperature_K - 260)) ** (10 ** (0.43 + 8.33 / (141.5 / gamma_oil - 131.5)))
    return X

def Compressibility_Oil_VB(Rs_m3m3, gamma_gas, Temperature_K, gamma_oil, Pressure_MPa):
    X = (28.1 * Rs_m3m3 + 30.6 * Temperature_K - 1180 * gamma_gas + 1784 / gamma_oil - 10910) / (100000 * Pressure_MPa)
    return X

def Bubblepoint_Standing(Rsb_m3m3, gamma_gas, Temperature_K, gamma_oil):
    Min_rsb = 1.8
    Rsb_old = Rsb_m3m3
    if Rsb_m3m3 < Min_rsb:
        Rsb_m3m3 = Min_rsb
    yg = 1.225 + 0.001648 * Temperature_K - 1.769 / gamma_oil
    X = 0.5197 * (Rsb_m3m3 / gamma_gas) ** 0.83 * 10 ** yg
    if Rsb_old < Min_rsb:
        X = (Bubblepoint_Standing - 0.1013) * Rsb_old / Min_rsb + 0.1013
    return X    
  
def FVF_Saturated_Oil_Standing(Rs_m3m3, gamma_gas, Temperature_K, gamma_oil):
    f = 5.615 * Rs_m3m3 * (gamma_gas / gamma_oil) ** 0.5 + 2.25 * Temperature_K - 575
    X = 0.972 + 0.000147 * f ** 1.175
    return X
    
def FVF_Above_Bubblepoint_Pressure_Standing(Pressure_MPa, Bubblepoint_Pressure_MPa, Oil_Compressibility, FVF_Saturated_Oil):
    if Pressure_MPa <= Bubblepoint_Pressure_MPa:
        X = FVF_Saturated_Oil
    else:
        X = FVF_Saturated_Oil * exp(Oil_Compressibility * (Bubblepoint_Pressure_MPa - Pressure_MPa))
    return X

def Oil_Viscosity_Standing(Rs_m3m3, Dead_oil_viscosity, Pressure_MPa, Bubblepoint_Pressure_MPa):
    a = 5.6148 * Rs_m3m3 * (0.1235 * 10 ** (-5) * Rs_m3m3 - 0.00074)
    B = 0.68 / 10 ** (0.000484 * Rs_m3m3) + 0.25 / 10 ** (0.006176 * Rs_m3m3) + 0.062 / 10 ** (0.021 * Rs_m3m3)
    X = 10 ** a * Dead_oil_viscosity ** B
    if Bubblepoint_Pressure_MPa < Pressure_MPa:
        X = X + 0.14504 * (Pressure_MPa - Bubblepoint_Pressure_MPa) * (0.024 * X ** 1.6 + 0.038 * X ** 0.56)
    return X   

def Density_Oil_Standing(Rs_m3m3, gamma_gas, gamma_oil, Pressure_MPa, FVF_m3m3, BP_Pressure_MPa, Compressibility_1MPa):
    X = (1000 * gamma_oil + 1.224 * gamma_gas * Rs_m3m3) / FVF_m3m3
    if Pressure_MPa > BP_Pressure_MPa:
        X = X * exp(Compressibility_1MPa * (Pressure_MPa - BP_Pressure_MPa))
    return X
    
def GOR_Standing(Pressure_MPa, gamma_gas, Temperature_K, gamma_oil):
    yg = 1.225 + 0.001648 * Temperature_K - 1.769 / gamma_oil
    X = gamma_gas * (1.92 * Pressure_MPa / 10 ** yg) ** 1.204
    return X

def Bubblepoint_Valko_McCainSI(Rsb_m3m3, gamma_gas, Temperature_K, gamma_oil):
    Min_rsb = 1.8
    Max_rsb = 800
    Rsb_old = Rsb_m3m3
    if Rsb_m3m3 < Min_rsb:
        Rsb_m3m3 = Min_rsb
    if Rsb_m3m3 > Max_rsb:
        Rsb_m3m3 = Max_rsb
    API = 141.5 / gamma_oil - 131.5
    z1 = -4.814074834 + 0.7480913 * log(Rsb_m3m3) + 0.1743556 * log(Rsb_m3m3) ** 2 - 0.0206 * log(Rsb_m3m3) ** 3
    z2 = 1.27 - 0.0449 * API + 4.36 * 10 ** (-4) * API ** 2 - 4.76 * 10 ** (-6) * API ** 3
    z3 = 4.51 - 10.84 * gamma_gas + 8.39 * gamma_gas ** 2 - 2.34 * gamma_gas ** 3
    z4 = -7.2254661 + 0.043155 * Temperature_K - 8.5548 * 10 ** (-5) * Temperature_K ** 2 + 6.00696 * 10 ** (-8) * Temperature_K ** 3
    z = z1 + z2 + z3 + z4
    lnpb = 2.498006 + 0.713 * z + 0.0075 * z ** 2
    X = 2.718282 ** lnpb
    if Rsb_old < Min_rsb:
        X = (X - 0.1013) * Rsb_old / Min_rsb + 0.1013
    if Rsb_old > Max_rsb:
        X = (X - 0.1013) * Rsb_old / Max_rsb + 0.1013
    return X    
    
def GOR_VelardeSI(Pressure_MPa, Bubblepoint_Pressure_MPa, gamma_gas, Temperature_K, gamma_oil, Rsb_m3_m3):
    API = 141.5 / gamma_oil - 131.5
    MaxRs = 800
    if Bubblepoint_Pressure_MPa > Bubblepoint_Valko_McCainSI(MaxRs, gamma_gas, Temperature_K, gamma_oil):
        if Pressure_MPa < Bubblepoint_Pressure_MPa:
            X = (Rsb_m3_m3) * (Pressure_MPa / Bubblepoint_Pressure_MPa)
        else:
            X = Rsb_m3_m3
    if Bubblepoint_Pressure_MPa > 0:
        Pr = (Pressure_MPa - 0.101) / (Bubblepoint_Pressure_MPa)
    else:
        Pr = 0
    if Pr <= 0:
        X = 0
    if Pr >= 1: 
        X = Rsb_m3_m3
    if Pr < 1:
        a_0 = 1.8653 * 10 ** (-4)
        a_1 = 1.672608
        a_2 = 0.92987
        a_3 = 0.247235
        a_4 = 1.056052
        a1 = a_0 * gamma_gas ** a_1 * API ** a_2 * (1.8 * Temperature_K - 460) ** a_3 * Bubblepoint_Pressure_MPa ** a_4
        b_0 = 0.1004
        b_1 = -1.00475
        b_2 = 0.337711
        b_3 = 0.132795
        b_4 = 0.302065
        a2 = b_0 * gamma_gas ** b_1 * API ** b_2 * (1.8 * Temperature_K - 460) ** b_3 * Bubblepoint_Pressure_MPa ** b_4
        c_0 = 0.9167
        c_1 = -1.48548
        c_2 = -0.164741
        c_3 = -0.09133
        c_4 = 0.047094
        A3 = c_0 * gamma_gas ** c_1 * API ** c_2 * (1.8 * Temperature_K - 460) ** c_3 * Bubblepoint_Pressure_MPa ** c_4
        Rsr = a1 * Pr ** a2 + (1 - a1) * Pr ** A3
        X = Rsr * Rsb_m3_m3
    return X

def FVF_McCainSI(Rs_m3m3, gamma_gas, STO_density_kg_m3, Reservoir_oil_density_kg_m3):
    X = (STO_density_kg_m3 + 1.22117 * Rs_m3m3 * gamma_gas) / Reservoir_oil_density_kg_m3
    return X  
    
def Density_McCainSI(Pressure_MPa, gamma_gas, Temperature_K , gamma_oil, Rs_m3_m3, BP_Pressure_MPa, Compressibility):
    #API = 141.5 / gamma_oil - 131.5
#limit input range to Rs = 800, Pb =1000
    if Rs_m3_m3 > 800:
        Rs_m3_m3 = 800
        BP_Pressure_MPa = Bubblepoint_Valko_McCainSI(Rs_m3_m3, gamma_gas, Temperature_K, gamma_oil)
    ropo = 845.8 - 0.9 * Rs_m3_m3
    pm = ropo
    pmmo = 0
    epsilon = 0.000001
    i = 0
    counter = 0
    MaxIter = 1000
    while (fabs(pmmo - pm) > epsilon and counter < MaxIter):
        i = i + 1
        pmmo = pm
        a0 = -799.21
        a1 = 1361.8
        a2 = -3.70373
        A3 = 0.003
        A4 = 2.98914
        A5 = -0.00223
        roa = a0 + a1 * gamma_gas + a2 * gamma_gas * ropo + A3 * gamma_gas * ropo ** 2 + A4 * ropo + A5 * ropo ** 2
        ropo = (Rs_m3_m3 * gamma_gas + 818.81 * gamma_oil) / (0.81881 + Rs_m3_m3 * gamma_gas / roa)
        pm = ropo
        counter = counter + 1
#поправка для правильной работы при низких температурах
    if Temperature_K < 520 / 1.8:
        Temperature_K = 520 / 1.8
    if Pressure_MPa <= BP_Pressure_MPa:
        dpp = (0.167 + 16.181 * (10 ** (-0.00265 * pm))) * (2.32328 * Pressure_MPa) - 0.16 * (0.299 + 263 * (10 ** (-0.00376 * pm))) * (0.14503774 * Pressure_MPa) ** 2
        pbs = pm + dpp
        dpt = (0.04837 + 337.094 * pbs ** (-0.951)) * (1.8 * Temperature_K - 520) ** 0.938 - (0.346 - 0.3732 * (10 ** (-0.001 * pbs))) * (1.8 * Temperature_K - 520) ** 0.475
        pm = pbs - dpt
        X = pm
    else:
        dpp = (0.167 + 16.181 * (10 ** (-0.00265 * pm))) * (2.32328 * BP_Pressure_MPa) - 0.16 * (0.299 + 263 * (10 ** (-0.00376 * pm))) * (0.14503774 * BP_Pressure_MPa) ** 2
        pbs = pm + dpp
        dpt = (0.04837 + 337.094 * pbs ** (-0.951)) * (1.8 * Temperature_K - 520) ** 0.938 - (0.346 - 0.3732 * (10 ** (-0.001 * pbs))) * (1.8 * Temperature_K - 520) ** 0.475
        pm = pbs - dpt
        X = pm * exp(Compressibility * (Pressure_MPa - BP_Pressure_MPa))
    return X
    
def DeadOilViscosity_Beggs_Robinson(Temperature_K, gamma_oil):
    x = (1.8 * Temperature_K - 460) ** (-1.163) * exp(13.108 - 6.591 / gamma_oil)
    Y = 10 ** (x) - 1
    return Y
    
def SaturatedOilViscosity_Beggs_Robinson(GOR_pb_m3m3, Dead_oil_viscosity):
    a = 10.715 * (5.615 * GOR_pb_m3m3 + 100) ** (-0.515)
    B = 5.44 * (5.615 * GOR_pb_m3m3 + 150) ** (-0.338)
    X = a * (Dead_oil_viscosity) ** B
    return X
    
def OilViscosity_Vasquez_Beggs(Saturated_oil_viscosity, Pressure_MPa, BP_Pressure_MPa):
    C1 = 957
    C2 = 1.187
    C3 = -11.513
    C4 = -0.01302
    m = C1 * Pressure_MPa ** C2 * exp(C3 + C4 * Pressure_MPa)
    X = Saturated_oil_viscosity * (Pressure_MPa / BP_Pressure_MPa) ** m
    return X
    
def OilViscosity_Beggs_Robinson_Vasques_Beggs(Rs_m3m3, GOR_pb_m3m3, Pressure_MPa, BP_Pressure_MPa, Dead_oil_viscosity):
    if (Pressure_MPa < BP_Pressure_MPa): #saturated
        X = SaturatedOilViscosity_Beggs_Robinson(Rs_m3m3, Dead_oil_viscosity)
    else: #undersaturated
        X = OilViscosity_Vasquez_Beggs(SaturatedOilViscosity_Beggs_Robinson(GOR_pb_m3m3, Dead_oil_viscosity), Pressure_MPa, BP_Pressure_MPa)
    return X  
    
def Viscosity_Grace(Pressure_MPa, Bubblepoint_Pressure_MPa, Density_kg_m3, BP_Density_kg_m3):
    density = Density_kg_m3 * 0.06243
    Bubblepoint_Density = BP_Density_kg_m3 * 0.06243
    rotr = 0.0008 * density ** 3 - 0.1017 * density ** 2 + 4.3344 * density - 63.001
    mu = exp(0.0281 * rotr ** 3 - 0.0447 * rotr ** 2 + 1.2802 * rotr + 0.0359)
    if Bubblepoint_Pressure_MPa < Pressure_MPa:
        robtr = -68.1067 * log(Bubblepoint_Density) ** 3 + 783.2173 * log(Bubblepoint_Density) ** 2 - 2992.2353 * log(Bubblepoint_Density) + 3797.6
        m = exp(0.1124 * robtr ** 3 - 0.0618 * robtr ** 2 + 0.7356 * robtr + 2.3328)
        mu = mu * (Density_kg_m3 / BP_Density_kg_m3) ** m
    X = mu
    return X    
    
def PseudoTemperature(gg):
    X = 95 + 171 * gg
    return X    

def PseudoPressure(gg):
    X = 4.9 - 0.4 * gg
    return X
    
def PseudoTemperatureStanding(gg):
    X = 93.3 + 180 * gg - 6.94 * gg ** 2
    return X   

def PseudoPressureStanding(gg):
    X = 4.6 + 0.1 * gg - 0.258 * gg ** 2
    return X    
    
def ZFactor(Tpr, Ppr):
    'Brill and Beggs, Standing'    
    a = 1.39 * (Tpr - 0.92) ** 0.5 - 0.36 * Tpr - 0.101
    B = Ppr * (0.62 - 0.23 * Tpr) + Ppr ** 2 * (0.006 / (Tpr - 0.86) - 0.037) + 0.32 * Ppr ** 6 / exp(20.723 * (Tpr - 1))
    c = 0.132 - 0.32 * log(Tpr) / log(10)
    d = exp(0.715 - 1.128 * Tpr + 0.42 * Tpr ** 2)
    X = a + (1 - a) * exp(-B) + c * Ppr ** d
    return X    
    
def GasFVF(T, P, z):
    X = 0.1 / P * T / 313 * z
    return X    
    
def GasFVF_gamma(T, P, gamma_g, PVT_correlations = 0):
    #function calculates gas formation volume factor
   
    if PVT_correlations == 0:
        T_pc = PseudoTemperatureStanding(gamma_g)
        P_pc = PseudoPressureStanding(gamma_g)
        z = ZFactorDranchuk(T / T_pc, P / P_pc)
    else:
        T_pc = PseudoTemperature(gamma_g)
        P_pc = PseudoPressure(gamma_g)
        z = ZFactor(T / T_pc, P / P_pc)
    X = GasFVF(T, P, z)
    return X    
    
def GVisc(T, P, z, gg):
    R = 1.8 * T
    mwg = 28.966 * gg
    gd = P * mwg / (z * T * 8.31)
    a = (9.379 + 0.01607 * mwg) * R ** 1.5 / (209.2 + 19.26 * mwg + R)
    B = 3.448 + 986.4 / R + 0.01009 * mwg
    c = 2.447 - 0.2224 * B
    X = 0.0001 * a * exp(B * gd ** c)
    return X    
    
def ZFactorEstimateDranchuk(Tpr, Ppr, z): 
    a1 = 0.3265
    a2 = -1.07
    a3 = -0.5339
    a4 = 0.01569
    a5 = -0.05165
    A6 = 0.5475
    A7 = -0.7361
    A8 = 0.1844
    A9 = 0.1056
    A10 = 0.6134
    A11 = 0.721
    rho_r = 0.27 * Ppr / (z * Tpr)
    X = -z + (a1 + a2 / Tpr + a3 / Tpr ** 3 + a4 / Tpr ** 4 + a5 / Tpr ** 5) * rho_r +\
    (A6 + A7 / Tpr + A8 / Tpr ** 2) * rho_r ** 2 - A9 * (A7 / Tpr + A8 / Tpr ** 2) * rho_r ** 5 + \
    A10 * (1 + A11 * rho_r ** 2) * rho_r ** 2 / Tpr ** 3 * exp(-A11 * rho_r ** 2) +1 
    return X     
    
def ZFactorDranchuk (Tpr, Ppr):
    Z_low = 0.1
    Z_hi = 5
    i = 0
    while (i<20 or fabs(Z_low - Z_hi) > 0.001):
        Z_mid = 0.5 * (Z_hi + Z_low)
        y_low = ZFactorEstimateDranchuk(Tpr, Ppr, Z_low)
        y_hi = ZFactorEstimateDranchuk(Tpr, Ppr, Z_mid)
        if y_low * y_hi < 0:
            Z_hi = Z_mid
        else:
            Z_low = Z_mid
        i = i + 1
    return Z_mid    
    
def WaterViscosity(Pressure_MPa, Temperature_K, Salinity_mg_liter, gamma_water):
    wpTDS = Salinity_mg_liter / (10000 * gamma_water)
    a = 109.574 - 8.40564 * wpTDS + 0.313314 * wpTDS ** 2 + 0.00872213 * wpTDS ** 3
    B = -1.12166 + 0.0263951 * wpTDS - 0.000679461 * wpTDS ** 2 - 5.47119 * 10 ** (-5) * wpTDS ** 3 + 1.55586 * 10 ** (-6) * wpTDS ** 4
    visc = a * (1.8 * Temperature_K - 460) ** B
    psi = Pressure_MPa * 145.04
    X = visc * (0.9994 + 4.0295 * 10 ** (-5) * psi + 3.1062 * 10 ** (-9) * psi ** 2)
    return X    
    
def WaterDensity(Pressure_MPa, Temperature_K, Salinity_mg_liter, gamma_water):
    wpTDS = Salinity_mg_liter / (10000 * gamma_water)
    wd = 0.0160185 * (62.368 + 0.438603 * wpTDS + 0.00160074 * wpTDS ** 2)
    X = wd / WaterFVF(Pressure_MPa, Temperature_K)
    return X   
    
def WaterFVF(Pressure_MPa, Temperature_K):
    f = 1.8 * Temperature_K - 460
    psi = Pressure_MPa * 145.04
    dvwp = -1.95301 * 10 ** (-9) * psi * f - 1.72834 * 10 ** (-13) * psi ** 2 * f - 3.58922 * 10 ** (-7) * psi - 2.25341 * 10 ** (-10) * psi ** 2
    dvwt = -1.0001 * 10 ** (-2) + 1.33391 * 10 ** (-4) * f + 5.50654 * 10 ** (-7) * f ** 2
    X = (1 + dvwp) * (1 + dvwt)
    return X    
    
def WaterCompressibilty(Pressure_MPa, Temperature_K, Salinity_mg_liter):
    f = 1.8 * Temperature_K - 460
    psi = Pressure_MPa * 145.04
    X = 0.1 * 145.04 / (7.033 * psi + 0.5415 * Salinity_mg_liter - 537 * f + 403300)
    return X   
    
def GWR(Pressure_MPa, Temperature_K, Salinity_mg_liter, gamma_water):
    f = 1.8 * Temperature_K - 460
    psi = Pressure_MPa * 145.04
    wpTDS = Salinity_mg_liter / (10000 * gamma_water)
    a = 8.15839 - 0.0612265 * f + 1.91663 * 10 ** (-4) * f ** 2 - 2.1654 * 10 ** (-7) * f ** 3
    B = 1.01021 * 10 ** (-2) - 7.44241 * 10 ** (-5) * f + 3.05553 * 10 ** (-7) * f ** 2 - 2.94883 * 10 ** (-10) * f ** 3
    c = (-9.02505 + 0.130237 * f - 8.53425 * 10 ** (-4) * f ** 2 + 2.34122 * 10 ** (-6) * f ** 3 - 2.37049 * 10 ** (-9) * f ** 4) * 10 ** (-7)
    Rswws = (a + B * psi + c * psi ** 2) * 0.1781
    X = Rswws * 10 ** (-0.0840655 * wpTDS * f ** (-0.285854))
    return X    
    
def WaterDensitySC(Salinity_mg_liter, gamma_water):
    wpTDS = Salinity_mg_liter / (10000 * gamma_water)
    X = 0.0160185 * (62.368 + 0.438603 * wpTDS + 0.00160074 * wpTDS ** 2)
    return X  
    
    
    
  # 2015/03/03
  # подготовлено для ОпНасос
  # набор функций для расчёта PVT свойств углеводородных газов  
    
def calc_Zgas(T_k, P_MPa, gamma_g, z_correlation):
    #calculus of z factor
    #rnt 20150303 не желательно использовать значение корреляции отличное от 0
    #http://petrowiki.org/Real_gases
    #расчет по Дранчуку или по Саттону
    #Dranchuk, P.M. and Abou-Kassem, H. 1975. Calculation of Z Factors For Natural Gases Using Equations of State. J Can Pet Technol 14 (3): 34. PETSOC-75-03-03. http://dx.doi.org/10.2118/75-03-03
    #Sutton, R.P. 1985. Compressibility Factors for High-Molecular-Weight Reservoir Gases. Presented at the SPE Annual Technical Conference and Exhibition, Las Vegas, Nevada, USA, 22-26 September. SPE-14265-MS. http://dx.doi.org/10.2118/14265-MS
    if z_correlation == 0:
        T_pc = PseudoTemperatureStanding(gamma_g)
        P_pc = PseudoPressureStanding(gamma_g)
        z = ZFactorDranchuk(T_k / T_pc, P_MPa / P_pc)
    else:
        T_pc = PseudoTemperature(gamma_g)
        P_pc = PseudoPressure(gamma_g)
        z = ZFactor(T_k / T_pc, P_MPa / P_pc)
    return z   
    
def calc_Mug_cP(t_K, p_MPa, z, gg):
    #rnt 20150303
    #расчет вязкости газа after Lee    http://petrowiki.org/Gas_viscosity
    #похоже, что отсюда
    #Lee, A.L., Gonzalez, M.H., and Eakin, B.E. 1966. The Viscosity of Natural Gases. J Pet Technol 18 (8): 997–1000. SPE-1340-PA. http://dx.doi.org/10.2118/1340-PA
    R = 1.8 * t_K
    mwg = 28.966 * gg
    gd = p_MPa * mwg / (z * t_K * 8.31)
    a = (9.379 + 0.01607 * mwg) * R ** 1.5 / (209.2 + 19.26 * mwg + R)
    b = 3.448 + 986.4 / R + 0.01009 * mwg
    c = 2.447 - 0.2224 * b
    X = 0.0001 * a * exp(b * gd ** c)
    return X    
          
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
