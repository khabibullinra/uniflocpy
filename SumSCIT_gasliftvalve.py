# -*- coding: utf-8 -*-
"""
Created on Sat Jun 20 03:41:03 2015

@author: KhalikovRuslan
"""


from pylab import *
import matplotlib as mpl 
import matplotlib as plt
from numpy import *
import sys
sys.path.insert(0, 'F:\\Python\\SumSCIT\\uniflocpy')
from SumSCIT_base import*
from pylab import *
mpl.rcParams['font.family'] = 'fantasy'
mpl.rcParams['font.fantasy'] = 'Times New Roman'

'Расчёт расстановки газлифтных клапанов'
'(пусковых и рабочего)'

'Скважина остановлена и заполнена пластовой продукцией'
'Вычислим среднюю плотность жидкости по стволу скважины'


'!!!!!!!!'
gamma_o = 0.836
gamma_g = 0.8
PVT_correlations = 0
r_sb = 100
P_rb = -1
b_ro = -1
Salinity = -1
gamma_w = 1
t_sn = 30
t_en = 40
Pr = 160
d = 0.062 #!!!!!!!!!!!!!!!!!!!!!!!!!
q_osc = 10 #!!!!!!!!!!!!!!!!!!!!!!!!!
teta = 90
ksi = 0.0002
length = 2550 #!!!!!!!!!!!!!!!!!!!!!!!!!
p_sn = 30 #!!!!!!!!!!!!!!!!!!!!!!!!!
WCT = 0.4 #!!!!!!!!!!!!!!!!!!!!!!!!!
R_p = 400 #!!!!!!!!!!!!!!!!!!!!!!!!!
n_n = 100
Calc_p_out = 0
MaxSegmLen = -1
grad_Calc = 5
Payne_et_all_holdup = 0
Payne_et_all_friction = 1
slip = 1
p_sn_2 = 110 #'целевое забойное давление' #!!!!!!!!!!!!!!!!!!!!!!!!!
t_sn_2 = 40
t_en_2 = 30

Calc_p_out_2 = 1

rho_air = 1.2217 # плотность воздуха при стандартных условиях
Hst = -1
Hw = length
R_p_2 = 100 #!!!!!!!!!!!!!!!!!!!!!!!!!
P_c = 75 #70
t_norm = 293

Pz = 1
D_c = 0.146
delta_p_gv = 3
delta_p_gv_2 = delta_p_gv + 1
delta_p_gv_3 = delta_p_gv_2 + 1
delta_p_gv_4 = delta_p_gv_3 + 1
n_pk = 6 # количество пусковых клапанов (минимум 1; максимум 9)
'!!!!!!!!'


'Значение плотности для расчёта плотности нефти из входного параметра удельная плотность нефти [усл. плот. воды при стандартных условиях]'
rho_ref = 1000

""" Вызываем блок PVT данных"""
PVT_gasvalve = Calc_PVT(gamma_o, gamma_g, PVT_correlations, r_sb, P_rb, b_ro, Salinity, gamma_w) 
PVT_gasvalve.CalcPVT(Pr, t_en, gamma_o, gamma_g, PVT_correlations, r_sb, P_rb, b_ro, Salinity, gamma_w) 
"""Выходными параметрами являются: PVT.p_b [МПа]; PVT.r_s [м3/м3]; PVT.b_o [д. ед.]; PVT.mu_o [сПз]; PVT.z; PVT.b_g; PVT.mu_g; PVT.b_w [д. ед.]; PVT.mu_w [сПз]"""                

rho_gsc = gamma_g * rho_air
rho_w_sc = WaterDensitySC(Salinity, gamma_w) * rho_ref
rho_osc = gamma_o * rho_ref
rho_w = rho_w_sc / PVT_gasvalve.b_w
rho_o = (rho_osc + PVT_gasvalve.r_s * rho_gsc) / PVT_gasvalve.b_o
rho_wm = (rho_w_sc + rho_w) / 2
rho_om = (rho_osc + rho_o) / 2
rho_lm = rho_om * (1 - WCT) + rho_wm * WCT  # не учитывается изменение объемной доли воды

if Hst == -1:
    Hst = Hw - 10**5 * Pr / (rho_lm * g)
    Pz = 1

'Строим распределение давления в скважине, в остановленной - равно гидростату'
'линия 1'
H_1_mas = []
P_1_mas = []
for H_1 in range(0, round(Hw), 50): 
    P_1 = (H_1 - Hst) * (Pr - Pz) / (Hw - Hst) + Pz 
    if P_1 <= Pz:
        P_1 = Pz
    H_1_mas.append(H_1)
    P_1_mas.append(P_1)
'линия 2'
KRD_2 = pressuregradient()
KRD_2.Pipe_pressure_drop(D_c, q_osc, teta, ksi, length, p_sn_2, t_sn_2, t_en_2, WCT, gamma_o, gamma_g, R_p_2, n_n, Salinity,
                         Calc_p_out_2, PVT_correlations, r_sb, P_rb, b_ro, MaxSegmLen, grad_Calc,
                         Payne_et_all_holdup, Payne_et_all_friction, slip) # КРД считает для НКТ спущенного до забоя
'линия 3'
KRD_3 = pressuregradient()
KRD_3.Pipe_pressure_drop(d, q_osc, teta, ksi, length, p_sn, t_sn, t_en, WCT, gamma_o, gamma_g, R_p, n_n, Salinity,
                         Calc_p_out, PVT_correlations, r_sb, P_rb, b_ro, MaxSegmLen, grad_Calc,
                         Payne_et_all_holdup, Payne_et_all_friction, slip) # КРД считает для НКТ спущенного до забоя

delta_t_4 = (t_en - t_sn) / (length/n_n)
t_4 = t_sn
P_4 = P_c
H_4_mas = []
P_4_mas = []
t_4_mas = []
for H_4 in range(0, length, n_n): 
     t_4 = t_4 + 0.5 * delta_t_4
     if H_4 == 0:
         t_4 = t_sn
     PVT_4 = Calc_PVT(gamma_o, gamma_g, PVT_correlations, r_sb, P_rb, b_ro, Salinity, gamma_w) 
     PVT_4.CalcPVT(P_4, t_4, gamma_o, gamma_g, PVT_correlations, r_sb, P_rb, b_ro, Salinity, gamma_w)    
     P_4 = P_c * exp(gamma_g * rho_air * g * H_4 * t_norm/(10**5*(t_4+273)*PVT_4.z)) # - P_c
     P_4_mas.append(P_4)
     H_4_mas.append(H_4)
     t_4_mas.append(t_4)


'''Превышение уровня жидкости при закачке газа над статическим'''
delta_Hst = 10**5*(P_c - p_sn)/(rho_lm * g)
H_A = Hst - delta_Hst
if delta_Hst < Hst:
    H_5_mas = []
    P_5_mas = []
    H_Ab =Hst + (p_sn-Pz)*(Hw-Hst)/(Pr-Pz)
    delta_H = H_Ab - H_A
    for P_5 in range(p_sn, round(P_c + 20), 1): 
        H_5 = (P_5 - Pz) * (Hw - Hst) / (Pr - Pz) - delta_H + Hst         
        H_5_mas.append(H_5)
        P_5_mas.append(P_5)


'кривая ||4, клапан 1'
delta_t_6 = (t_en - t_sn) / (length/n_n)
t_6 = t_sn
P_6 = P_c - delta_p_gv
H_6_mas = []
P_6_mas = []
for H_6 in range(0, round(length), n_n): 
    t_6 = t_6 + 0.5 * delta_t_6
    if H_6 == 0:
        t_6 = t_sn
    PVT_6 = Calc_PVT(gamma_o, gamma_g, PVT_correlations, r_sb, P_rb, b_ro, Salinity, gamma_w) 
    PVT_6.CalcPVT(P_6, t_6, gamma_o, gamma_g, PVT_correlations, r_sb, P_rb, b_ro, Salinity, gamma_w)    
    P_6 = P_c * exp(gamma_g * rho_air * g * H_6 * t_norm/(10**5*(t_6+273)*PVT_6.z)) - delta_p_gv
    P_6_mas.append(P_6)
    H_6_mas.append(H_6)
     

plt.title('Расстановка газлифтных клапанов', color='black', family='fantasy', fontsize='x-large')
plt.xlabel('Давление, атм', color='black', family='fantasy', fontsize='x-large')
plt.ylabel('Глубина, м', color='black', family='fantasy', fontsize='x-large')
plt.grid()
ylim(length, 0)
xlim(0, Pr)
x1 = P_1_mas
y1 = H_1_mas
x2 = KRD_2.p_n
KRD_2.md_n.reverse()
y2 = KRD_2.md_n
x3 = KRD_3.p_n
y3 = KRD_3.md_n
x4 = P_4_mas
y4 = H_4_mas
x5 = p_sn
y5 = H_A
x6 = P_5_mas
y6 = H_5_mas
x7 = P_6_mas
y7 = H_6_mas

legen1 = 'Гидростатическое распределение'
legen2 = 'КРД (снизу - вверх) при пластовом Гф'
legen3 = 'КРД (сверху - вниз) при оптимальном уд. расх. газа'
legen4 = 'КРД газа (барометрическая формула)'
legen5 = 'точка А (разность Нст и разностей уровней (Pзак и Ру))'
legen6 = 'Линия параллельная гидростату'
legen7 = 'Учёт перепада давления в клапане'
legen8 = 'Глубина установки пускового клапана'
legen9 = 'Глубина установки рабочего клапана'

y_r = 0
x_r = p_sn
P_9_mas = []
H_9_mas = []
for ii in range (0,len(y2)-1, 1):
    for ij in range (0,len(y3)-1, 1):
        if y_r<=y2[ii]<y3[ij] and x_r<x2[ii]<x3[ij]:
            H_rk = (y2[ii] -y2[ii+1])/2 + y2[ii+1]  # Продумать более грамотный подход нахождения точки пересечения
            #H_rk = y2[ii]
            P_rk = (x2[ii] -x2[ii+1])/2 + x2[ii+1]
            #P_rk = x2[ii]
        y_r = y3[ij]
        x_r = x3[ij]
for P_9 in range (round(p_sn), round(P_rk) + 1, 1):
            P_9_mas.append(P_9)
            H_9_mas.append(H_rk)

x9 = P_9_mas
y9 = H_9_mas

y_r = 0
x_r = P_c - delta_p_gv
P_7_mas = []
H_7_mas = []
for ii in range (0,len(y6)-1, 1):
    for ij in range (0,len(y7)-1, 1):
        if y_r<=y6[ii]<y7[ij] and x_r<x6[ii]<x7[ij]:
            H_gv_1 = y6[ii-1] + (-y6[ii-1] + y6[ii])/2
            P_gv_1 = x6[ii]
        y_r = y7[ij]
        x_r = x7[ij]
for P_7 in range (round(p_sn), round(P_gv_1) + 1, 1):
            P_7_mas.append(P_7)
            H_7_mas.append(H_gv_1)

x8 = P_7_mas
y8 = H_7_mas
'Блок расчётов пусковых клапанов 2 - 9'
x_1_2 = []; y_1_2 = []
x_1_3 = []; y_1_3 = []
x_1_4 = []; y_1_4 = []
x_1_5 = []; y_1_5 = []
x_1_6 = []; y_1_6 = []
x_1_7 = []; y_1_7 = []
x_1_8 = []; y_1_8 = []
x_1_9 = []; y_1_9 = []
x_2_2 = []; y_2_2 = []
x_2_3 = []; y_2_3 = []
x_2_4 = []; y_2_4 = []
x_2_5 = []; y_2_5 = []
x_2_6 = []; y_2_6 = []
x_2_7 = []; y_2_7 = []
x_2_8 = []; y_2_8 = []
x_2_9 = []; y_2_9 = []
x_3_2 = []; y_3_2 = []
x_3_3 = []; y_3_3 = []
x_3_4 = []; y_3_4 = []
x_3_5 = []; y_3_5 = []
x_3_6 = []; y_3_6 = []
x_3_7 = []; y_3_7 = []
x_3_8 = []; y_3_8 = []
x_3_9 = []; y_3_9 = []
x_4_2 = []; y_4_2 = []
x_4_3 = []; y_4_3 = []
x_4_4 = []; y_4_4 = []
x_4_5 = []; y_4_5 = []
x_4_6 = []; y_4_6 = []
x_4_7 = []; y_4_7 = []
x_4_8 = []; y_4_8 = []
x_4_9 = []; y_4_9 = []
PVT_pk = Calc_PVT(gamma_o, gamma_g, PVT_correlations, r_sb, P_rb, b_ro, Salinity, gamma_w)
KRD_pk = pressuregradient()
for i in range (1, n_pk, 1):
    'нахождение точeк A:'
    KRD_pk = pressuregradient()
    KRD_pk.Pipe_pressure_drop(d, q_osc, teta, ksi, H_gv_1, p_sn, t_sn, t_en, WCT, gamma_o, gamma_g, R_p, n_n, Salinity,
                                Calc_p_out, PVT_correlations, r_sb, P_rb, b_ro, MaxSegmLen, grad_Calc,
                                Payne_et_all_holdup, Payne_et_all_friction, slip)    
    P_A = KRD_pk.p_n[len(KRD_pk.p_n)-1]
    x = P_A
    y = H_gv_1
    if i == 1:
        x_1_2 = x
        y_1_2 = y
    elif i == 2:
        x_1_3 = x
        y_1_3 = y        
    elif i == 3:
        x_1_4 = x
        y_1_4 = y    
    elif i == 4:
        x_1_5 = x
        y_1_5 = y    
    elif i == 5:
        x_1_6 = x
        y_1_6 = y      
    elif i == 6:
        x_1_7 = x
        y_1_7 = y    
    elif i == 7:
        x_1_8 = x
        y_1_8 = y    
    elif i == 8:
        x_1_9 = x
        y_1_9 = y    
    '|| линия - 1, через A'
    H_A_pk_mas = []
    P_A_pk_mas = []
    H_Ab_pk =Hst + (P_A -Pz)*(Hw-Hst)/(Pr-Pz)
    delta_H = H_Ab_pk - H_gv_1
    for P_A_i in range(round(p_sn), round(P_c + 20), 1): 
        H_A_pk = (P_A_i - Pz) * (Hw - Hst) / (Pr - Pz) - delta_H + Hst         
        H_A_pk_mas.append(H_A_pk)
        P_A_pk_mas.append(P_A_i)
    if i == 1:
        x_2_2 = P_A_pk_mas
        y_2_2 = H_A_pk_mas
    elif i == 2:
        x_2_3 = P_A_pk_mas
        y_2_3 = H_A_pk_mas    
    elif i == 3:
        x_2_4 = P_A_pk_mas
        y_2_4 = H_A_pk_mas    
    elif i == 4:
        x_2_5 = P_A_pk_mas
        y_2_5 = H_A_pk_mas    
    elif i == 5:
        x_2_6 = P_A_pk_mas
        y_2_6 = H_A_pk_mas     
    elif i == 6:
        x_2_7 = P_A_pk_mas
        y_2_7 = H_A_pk_mas     
    elif i == 7:
        x_2_8 = P_A_pk_mas
        y_2_8 = H_A_pk_mas     
    elif i == 8:
        x_2_9 = P_A_pk_mas
        y_2_9 = H_A_pk_mas    
    'Барометрическая формула + перепад на клапан'
    delta_t_pk = (t_en - t_sn) / (length/n_n)
    t_pk = t_sn
    if i == 1:
        delta_p_pk = delta_p_gv + 1
    elif i == 2:
        delta_p_pk = delta_p_gv + 2
    elif i == 3:
        delta_p_pk = delta_p_gv + 3    
    elif i == 4:
        delta_p_pk = delta_p_gv + 4    
    elif i == 5:
        delta_p_pk = delta_p_gv + 5    
    elif i == 6:
        delta_p_pk = delta_p_gv + 6     
    elif i == 7:
        delta_p_pk = delta_p_gv + 7     
    elif i == 8:
        delta_p_pk = delta_p_gv + 8     
    P_pk = P_c - delta_p_pk
    H_pk_mas = []
    P_pk_mas = []
    for H_pk in range(0, round(length), n_n): 
        t_pk = t_pk + 0.5 * delta_t_pk
        if H_pk == 0:
            t_pk = t_sn
        PVT_pk.CalcPVT(P_pk, t_pk, gamma_o, gamma_g, PVT_correlations, r_sb, P_rb, b_ro, Salinity, gamma_w)    
        P_pk = P_c * exp(gamma_g * rho_air * g * H_pk * t_norm/(10**5*(t_pk+273)*PVT_pk.z)) - delta_p_pk
        P_pk_mas.append(P_pk)
        H_pk_mas.append(H_pk)
    if i == 1:
        x_3_2 = P_pk_mas
        y_3_2 = H_pk_mas
    elif i == 2:
        x_3_3 = P_pk_mas
        y_3_3 = H_pk_mas
    elif i == 3:
        x_3_4 = P_pk_mas
        y_3_4 = H_pk_mas        
    elif i == 4:
        x_3_5 = P_pk_mas
        y_3_5 = H_pk_mas        
    elif i == 5:
        x_3_6 = P_pk_mas
        y_3_6 = H_pk_mas        
    elif i == 6:
        x_3_7 = P_pk_mas
        y_3_7 = H_pk_mas         
    elif i == 7:
        x_3_8 = P_pk_mas
        y_3_8 = H_pk_mas        
    elif i == 8:
        x_3_9 = P_pk_mas
        y_3_9 = H_pk_mas        
    'Поиск глубины спуска пускового клапана - i'
    y_r = 0
    x_r = P_c - delta_p_pk
    P_pk_mas = []
    H_pk_mas = []
    if i == 1:
        x_i_1 = x_2_2
        y_i_1 = y_2_2
        x_i_2 = x_3_2
        y_i_2 = y_3_2
    elif i == 2:
        x_i_1 = x_2_3
        y_i_1 = y_2_3    
        x_i_2 = x_3_3
        y_i_2 = y_3_3
    elif i == 3:
        x_i_1 = x_2_4
        y_i_1 = y_2_4    
        x_i_2 = x_3_4
        y_i_2 = y_3_4    
    elif i == 4:
        x_i_1 = x_2_5
        y_i_1 = y_2_5    
        x_i_2 = x_3_5
        y_i_2 = y_3_5
    elif i == 5:
        x_i_1 = x_2_6
        y_i_1 = y_2_6    
        x_i_2 = x_3_6
        y_i_2 = y_3_6        
    elif i == 6:
        x_i_1 = x_2_7
        y_i_1 = y_2_7    
        x_i_2 = x_3_7
        y_i_2 = y_3_7         
    elif i == 7:
        x_i_1 = x_2_8
        y_i_1 = y_2_8    
        x_i_2 = x_3_8
        y_i_2 = y_3_8 
    elif i == 8:
        x_i_1 = x_2_9
        y_i_1 = y_2_9    
        x_i_2 = x_3_9
        y_i_2 = y_3_9         
    for ii in range (0,len(y_i_1)-1, 1):
        for ij in range (0,len(y_i_2)-1, 1):
            if y_r<=y_i_1[ii]<y_i_2[ij] and x_r<x_i_1[ii]<x_i_2[ij]:
                H_gv_1 = y_i_1[ii-1] + (-y_i_1[ii-1] + y_i_1[ii])/2
                P_gv_1 = x_i_1[ii]
            y_r = y_i_2[ij]
            x_r = x_i_2[ij]
    for P_pk in range (round(p_sn), round(P_gv_1) + 1, 1):
                P_pk_mas.append(P_pk)
                H_pk_mas.append(H_gv_1)
    if i == 1:
        x_4_2 = P_pk_mas
        y_4_2 = H_pk_mas
    elif i == 2:
        x_4_3 = P_pk_mas
        y_4_3 = H_pk_mas
    elif i == 3:
        x_4_4 = P_pk_mas
        y_4_4 = H_pk_mas
    elif i == 4:
        x_4_5 = P_pk_mas
        y_4_5 = H_pk_mas  
    elif i == 5:
        x_4_6 = P_pk_mas
        y_4_6 = H_pk_mas   
    elif i == 6:
        x_4_7 = P_pk_mas
        y_4_7 = H_pk_mas  
    elif i == 7:
        x_4_8 = P_pk_mas
        y_4_8 = H_pk_mas  
    elif i == 8:
        x_4_9 = P_pk_mas
        y_4_9 = H_pk_mas  
plt.plot(x1, y1,'go-',
         x2, y2,'b*-',
         x3, y3,'r*-',
         x4, y4, 'r^',
         x5, y5, 'ro',
         x6, y6,'g--',
         x7, y7, 'r--',
         x8, y8, 'k-',
         x9, y9, 'r-',
         x_1_2, y_1_2, 'bo',
         x_1_3, y_1_3, 'bo',
         x_1_4, y_1_4, 'bo',
         x_1_5, y_1_5, 'bo',
         x_1_6, y_1_6, 'bo',
         x_1_7, y_1_7, 'bo',
         x_1_8, y_1_8, 'bo',
         x_1_9, y_1_9, 'bo',
         x_2_2, y_2_2, 'g--',
         x_2_3, y_2_3, 'g--',
         x_2_4, y_2_4, 'g--',
         x_2_5, y_2_5, 'g--',
         x_2_6, y_2_6, 'g--',
         x_2_7, y_2_7, 'g--',
         x_2_8, y_2_8, 'g--',
         x_2_9, y_2_9, 'g--',
         x_3_2, y_3_2, 'r--',
         x_3_3, y_3_3, 'r--',
         x_3_4, y_3_4, 'r--',
         x_3_5, y_3_5, 'r--',
         x_3_6, y_3_6, 'r--',
         x_3_7, y_3_7, 'r--',
         x_3_8, y_3_8, 'r--',
         x_3_9, y_3_9, 'r--',
         x_4_2, y_4_2, 'k-',
         x_4_3, y_4_3, 'k-',
         x_4_4, y_4_4, 'k-',
         x_4_5, y_4_5, 'k-',
         x_4_6, y_4_6, 'k-',
         x_4_7, y_4_7, 'k-',
         x_4_8, y_4_8, 'k-',
         x_4_9, y_4_9, 'k-')
plt.legend([legen1,
            legen2,
            legen3,
            legen4,
            legen5,
            legen6,
            legen7,
            legen8,
            legen9],
            loc='best')
plt.show()
