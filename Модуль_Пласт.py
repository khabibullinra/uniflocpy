# -*- coding: utf-8 -*-
"""
@author: Халиков Руслан

Уравнение притока [IPR]
"""
import math

class Пласт():
    def __init__(self, Data):
        self.pi_m3atmday = Data.pi_m3atmday
        self.P_res_atm = Data.P_res_atm
        self.Q_m3day = None
        self.T_res_C = Data.T_res_C

    def Индикаторная_кривая(self, Data, PVT, Out):
        Out.IPR_array.clear()
        Out.P_test_atm_array.clear()                
        for P_test_atm in range(1, round(self.P_res_atm / 3), 5):
            self.IPR_Vogel_wct(Data, PVT, P_test_atm)                   
            Out.save_IPR(self, P_test_atm)
        for P_test_atm in range(round(self.P_res_atm / 3), 
                                round(self.P_res_atm), 1):
            self.IPR_Vogel_wct(Data, PVT, P_test_atm)                   
            Out.save_IPR(self, P_test_atm)
            
    def IPR_Vogel_wct(self, Data, PVT, P_test_atm):
        """Для уравнения Вогеля необходимо иметь одну экспериментальную пару
           значений динамическое забойное давление - дебит(3 сценария)"""        

        if (Data.WCT_d == 1) or (P_test_atm > PVT.p_b_MPa * 10):
            self.Q_m3day = self.pi_m3atmday * (self.P_res_atm - P_test_atm)
        else:
            'Доля нефти'            
            fo_d = 1 - Data.WCT_d
            
            'Дебит в точке насыщения'            
            Q_b_m3day = self.pi_m3atmday * (self.P_res_atm - PVT.p_b_MPa * 10)            
            
            'Максимальный дебит при 100% нефти'            
            Qo_max_m3day = Q_b_m3day + (self.pi_m3atmday * PVT.p_b_MPa * 10)\
            / 1.8
            
            'Давление для комбинированного уравнения притока при Qo_max'            
            Pwfg_atm = Data.WCT_d * (self.P_res_atm - Qo_max_m3day\
            / self.pi_m3atmday)
            
            if P_test_atm > Pwfg_atm:
                a = (P_test_atm + 0.125 * fo_d * PVT.p_b_MPa * 10\
                - Data.WCT_d * self.P_res_atm) / (0.125 * fo_d *\
                PVT.p_b_MPa * 10)
                b = Data.WCT_d / (0.125 * fo_d * PVT.p_b_MPa * 10\
                * self.pi_m3atmday)
                c = (2 * a * b) + 80 / (Qo_max_m3day - Q_b_m3day)
                d = (a ** 2) - (80 * Q_b_m3day / (Qo_max_m3day - Q_b_m3day))\
                - 81
                if b == 0:
                    self.Q_m3day = math.abs(d / c)
                else:
                    self.Q_m3day = (-c + (c ** 2 - 4 * b * b * d) ** 0.5)\
                    / (2 * b ** 2)
            else:
                'Геометрический факторы'                
                CG = 0.001 * Qo_max_m3day
                CD = Data.WCT_d * (CG / self.pi_m3atmday) + fo_d * 0.125\
                * PVT.p_b_MPa * 10\
                * (-1 + (1 + 80 * ((0.001 * Qo_max_m3day)/
                (Qo_max_m3day - Q_b_m3day))) ** 0.5)
                
                'Максимальный дебит по комбинированному уравнению притока'
                'Обводненность отлична от 100%'

                self.Q_m3day = (Pwfg_atm - P_test_atm + Qo_max_m3day\
                * (CD / CG)) / (CD / CG) 
                
if __name__ == "__main__":
    print("Вы запустили модуль напрямую, а не импортировали его.") 
    input ("\n\nНажмите Enter, чтобы выйти.")                  