# -*- coding: utf-8 -*-
"""
@author: Халиков Руслан
"""
import math, scipy.optimize

class Труба():
    def __init__(self, Data):
            self.theta = Data.theta
            self.d_m = Data.d_m
            self.ksi_m = Data.ksi_m
            self.length_m = Data.length_m 
            self.length_case_m = Data.L_m            
            self.D_c_m = Data.D_c_m 
            self.length_s_m = 0
           
    def Кривая_оттока(self, PVT_object, out, Data, Вспомогательные_данные):
        'марш - алгоритм вычисления давления на конечном узле'
        q_osc_m3day_входное = Data.q_osc_m3day
        for q_m3day in out.IPR_array:        
            L_m = 0 
            p_pvt_atm = Data.p_sn_atm; P_atm = p_pvt_atm 
            T_C = Data.t_sn_C; t_pvt_C = T_C
            delta_l_m = self.length_m / (Data.N_n - 1)
            delta_t_C = (Data.t_en_C - Data.t_sn_C)/(Data.N_n - 1)         
            
            Data.q_osc_m3day = q_m3day * (1 - Data.WCT_d)
            while L_m < self.length_m - 0.001:
                delta_p_atm = 0; 
                counter = 0; i = 0
                'Итеративный расчёт PVT на центре сегмента'    
                while abs(p_pvt_atm - (P_atm + 0.5 * delta_p_atm)) > 0.5\
                and counter < Data.Iter or i < 1:                
                    i+=1                
                    p_pvt_atm = P_atm + 0.5 * delta_p_atm
                    t_pvt_C = T_C + 0.5 * delta_t_C                        
                    PVT_object.calc_pvt(p_pvt_atm, t_pvt_C)
                    Вспомогательные_данные.Пересчитать_данные(PVT_object, self, Data)
                    self.Calc_Beggs_Brill(Вспомогательные_данные, out, p_pvt_atm)
                    'Перепад давления'
                    delta_p_atm = out.gradP_atmm * delta_l_m
                    counter += 1
                    if P_atm <=1:
                        delta_p_atm = 0
                        P_atm = 1
                        p_pvt_atm = 1
                P_atm += delta_p_atm           
                T_C += delta_t_C
                L_m += delta_l_m
            out.save_VLP(P_atm, q_m3day)          
        Data.q_osc_m3day = q_osc_m3day_входное
           
    def Профиль_давления(self, PVT_object, out, Data, Вспомогательные_данные):
        'марш - алгоритм расчёта КРД'
        L_m = self.length_s_m 
        p_pvt_atm = Data.p_sn_atm; P_atm = p_pvt_atm 
        T_C = Data.t_sn_C; t_pvt_C = T_C
        delta_l_m = self.length_m / (Data.N_n - 1)
        delta_t_C = (Data.t_en_C - Data.t_sn_C)/(Data.N_n - 1)         
        
        'подготовка массивов'
        out.md_m_array = [0]
        out.p_atm_array = [Data.p_sn_atm]
        out.gradP_g_atmm_array = [None]
        out.gradP_f_atmm_array = [None]
        out.gradP_atmm_array = [None]
        out.T_C_array = [Data.t_sn_C]
        out.flow_pattern_array = [None]
        out.P_b_atm_array = [None]
        out.r_s_m3m3_array = [None]
        out.b_o_m3m3_array = [None]
        out.mu_o_cP_array = [None]
        out.z_d_array = [None]
        out.b_g_m3m3_array = [None]
        out.mu_g_cP_array = [None]
        out.b_w_m3m3_array = [None]
        out.mu_w_cP_array = [None] 
        out.lambda_l_d_array = [None] 
        out.N_Fr_d_array = [None]
        out.d_m_array = [None]
        
        while L_m < self.length_m - 0.001:
            delta_p_atm = 0; 
            counter = 0; i = 0
            'Итеративный расчёт PVT на центре сегмента'    
            while (abs(p_pvt_atm - (P_atm + 0.5 * delta_p_atm)) > 0.5\
            and counter < Data.Iter) or i < 1:
                i+=1                
                p_pvt_atm = P_atm + 0.5 * delta_p_atm
                t_pvt_C = T_C + 0.5 * delta_t_C                        
                PVT_object.calc_pvt(p_pvt_atm, t_pvt_C)
                Вспомогательные_данные.Пересчитать_данные(PVT_object, self, Data)
                
                grad = 'Ansari'
                
                if grad == 'Brill':
                    self.Calc_Beggs_Brill(Вспомогательные_данные, out, p_pvt_atm)
                if grad == 'Ansari':    
                    self.Calc_Ansari(Вспомогательные_данные, out, p_pvt_atm, PVT_object)
                'Перепад давления'
                delta_p_atm = out.gradP_atmm * delta_l_m
                counter += 1
                if P_atm <=1:
                    delta_p_atm = 0
                    P_atm = 1
                    p_pvt_atm = 1
            P_atm += delta_p_atm           
            T_C += delta_t_C
            L_m += delta_l_m
            out.save(P_atm, T_C, L_m, PVT_object, Вспомогательные_данные, self)
        
        
#        ''' 
#        08.10.2015 Khalikov
#        код позволит расчитать физико - химические свойства и изменения 
#        градиента давление (как следствие перепада давления)
#        в диапазоне глубин (Башмак - ВДП)        
#        '''
#        D_box = self.d_m 
#        while L_m < self.length_case_m - 0.001:
#            self.d_m = self.D_c_m 
#            delta_p_atm = 0; 
#            counter = 0; i = 0
#            'Итеративный расчёт PVT на центре сегмента'    
#            while (abs(p_pvt_atm - (P_atm + 0.5 * delta_p_atm)) > 0.5\
#            and counter < Data.Iter) or i < 1:
#                i+=1                
#                p_pvt_atm = P_atm + 0.5 * delta_p_atm
#                t_pvt_C = T_C + 0.5 * delta_t_C                        
#                PVT_object.calc_pvt(p_pvt_atm, t_pvt_C)
#                Calc = Calculator.Calculator(PVT_object, self, Mode)
#                self.Calc_Beggs_Brill(Calc, self, out, p_pvt_atm)
#                'Перепад давления'
#                delta_p_atm = out.gradP_atmm * delta_l_m
#                counter += 1
#                if P_atm <=1:
#                    delta_p_atm = 0
#                    P_atm = 1
#                    p_pvt_atm = 1
#            P_atm += delta_p_atm           
#            T_C += delta_t_C
#            L_m += delta_l_m
#            out.save(P_atm, T_C, L_m, PVT_object, Calc, self)    
#        self.d_m = D_box
        
    
    
    """    
    Прогнозирование градиента давления
    эмпирическая корреляция Беггза и Брилла
    
    учитывается и эффект проскальзывания, и режим течения (Категория С)
    """    
    def Calc_Beggs_Brill(self, calculator, out, P_atm):        
        'Переходные границы режимов потока для модифицированной карты'
        L_1 = 316 * calculator.lambda_l_d ** 0.302
        L_2 = 0.000925 * calculator.lambda_l_d ** (-2.468)
        L_3 = 0.10 * calculator.lambda_l_d ** (-1.452)
        L_4 = 0.5 * calculator.lambda_l_d ** (-6.738)
        
        'Ниже приведены неравенства, в соответствии с которыми определяется\
        режим потока в горизонтальной трубе (может отличаться от реально\
        существующего режима потока в негоризонтальной трубе)'
        
        if (calculator.lambda_l_d < 0.01 and calculator.N_Fr_d < L_1)\
        or (calculator.lambda_l_d >= 0.01 and calculator.N_Fr_d < L_2):
            out.flow_pattern = "Кольцевой/расслоенный"
        
        elif (calculator.lambda_l_d < 0.4 and calculator.N_Fr_d >= L_1)\
        or (calculator.lambda_l_d >= 0.4 and calculator.N_Fr_d > L_4):
            out.flow_pattern = "Пузырьковый/эмульсионный/распределенный"   
       
        elif (0.01 <= calculator.lambda_l_d < 0.4\
        and L_3 < calculator.N_Fr_d <= L_1)\
        or (calculator.lambda_l_d >= 0.4 and L_3 < calculator.N_Fr_d <= L_4):
            out.flow_pattern = "Снарядный/пробковый/прерывистый"
       
        elif (calculator.lambda_l_d >= 0.01\
        and L_2 <= calculator.N_Fr_d < L_3):
            out.flow_pattern = "Переходный"
          
        if out.flow_pattern == "Переходный":
            A = (L_3 - calculator.N_Fr_d) / (L_3 - L_2)
            
            H_L_theta_1 = H_L_theta("Кольцевой/расслоенный", calculator, self, out)
            H_L_theta_2 = H_L_theta("Снарядный/пробковый/прерывистый", calculator, self, out)
            out.H_L_theta_d = A * H_L_theta_1 + (1 - A) * H_L_theta_2
        else:
            out.H_L_theta_d = H_L_theta(out.flow_pattern, calculator, self, out)
        
        'Нормализованный коэффициент трения'
        f_d = f_Moody(calculator, self)
        
        y = max(calculator.lambda_l_d / out.H_L_theta_d ** 2, 0.001)
        if y == 1:
            s = 0
        if 1 < y < 1.2:
            s = math.log(2.2 * y - 1.2)
        else:
            s = math.log(y) / (-0.0523 + 3.182 * math.log(y) - 0.8725\
            * (math.log(y)) ** 2 + 0.01853 * (math.log(y)) ** 4)
               
        'Коэффициент трения'
        f = f_d * math.exp(s) 
        
        'Переводной коэффициент из Па -> атм'
        c_p = 0.000009871668
        
        'Плотность смеси'
        rho_s = calculator.rho_l_kgm3 * out.H_L_theta_d + calculator.rho_g_kgm3\
        * (1 - out.H_L_theta_d)        
        
#        'обезразмеренная кинетическая энергия'
#        E_k_d = calculator.v_m_msec * calculator.v_sg_msec\
#        * calculator.rho_n_kgm3 / (P_atm / c_p)
#        if E_k_d > 1:
#            E_k_d = 0.999
        
#E_k_d принимала значения равные 3, хотя не должна превышать 1.
#
#Единица символизирует достижение сверхзвуковых скоростей, 
#при которых ударные волны приводят к разрыву давления 
#и градиент становится равным бесконечности.

        g_msec2 = 9.81
        
        'Градиент давления за счет гравитации'
        out.gradP_g_atmm = c_p * rho_s * g_msec2\
        * math.sin(math.pi / 180 * self.theta)                        
                   
        'Градиент давления за счет трения'
        out.gradP_f_atmm =  c_p * f * rho_s\
        * calculator.v_m_msec ** 2 / (2 * self.d_m)
        
        'Общий градиент давления'
        out.gradP_atmm = (out.gradP_g_atmm\
        + out.gradP_f_atmm) #/ (1 - E_k_d)
         

    def Calc_Ansari(self, calculator, out, P_atm, PVT_object):
        """Блок расчёта градиента давления по методике Ansari"""
        A = Ansari()
        A.Flow(calculator.v_sl_msec, 
               calculator.v_sg_msec, 
               calculator.rho_l_kgm3, 
               calculator.rho_g_kgm3, 
               calculator.mu_l_cP * 10**(-3), 
               PVT_object.mu_g_cP * 10**(-3), 
               self.theta, 
               self.d_m, 
               calculator.sigma_l_Newtonm, 
               self.ksi_m)
                
        if A.fpat == "slug":
            A.slug(calculator.v_sl_msec, 
               calculator.v_sg_msec, 
               calculator.rho_l_kgm3, 
               calculator.rho_g_kgm3, 
               A.fpat, 
               calculator.mu_l_cP * 10**(-3), 
               PVT_object.mu_g_cP * 10**(-3), 
               self.theta, 
               self.d_m, 
               calculator.sigma_l_Newtonm, 
               self.ksi_m)
            out.flow_pattern = "Снарядный/пробковый/прерывистый"
        elif A.fpat == "dbub" or A.fpat == "bub":
            A.bubble(calculator.v_sl_msec, 
               calculator.v_sg_msec, 
               calculator.rho_l_kgm3, 
               calculator.rho_g_kgm3, 
               A.fpat, 
               calculator.mu_l_cP * 10**(-3), 
               PVT_object.mu_g_cP * 10**(-3), 
               self.theta, 
               self.d_m, 
               calculator.sigma_l_Newtonm, 
               self.ksi_m)
        elif A.fpat == "annular":
            out.flow_pattern = "Кольцевой/расслоенный"
        elif A.fpat == "dbub":
            out.flow_pattern = "Пузырьковый(рассеянный)/эмульсионный/распределенный"
        elif A.fpat == "bub":
            out.flow_pattern = "Пузырьковый/эмульсионный/распределенный"
        elif A.fpat == "singleG":
            # Проверить вязкость, возможно *10**(-3) лишняя            
            A.single(self.theta * math.pi / 180, self.d_m, self.ksi_m / self.d_m, 
                     calculator.v_sg_msec, calculator.rho_g_kgm3, 
                     PVT_object.mu_g_cP * 10**(-3), P_atm)
            out.flow_pattern = "Однофазный(газ)"
        elif A.fpat == "singleL":
             # Проверить вязкость, возможно *10**(-3) лишняя            
            A.single(self.theta * math.pi / 180, self.d_m, 
                     self.ksi_m / self.d_m, calculator.v_sl_msec, calculator.rho_l_kgm3, 
                     calculator.mu_l_cP * 10**(-3), P_atm)
            out.flow_pattern = "Однофазный(жидкость)"
        out.gradP_atmm = A.pgt * 10 ** (-5)
        'elif A.fpat == "annular":' 
        '    A.annular(v_sl, v_sg, rho_l, deng, A.fpat, mu_l, PVT.mu_g, teta, d, sigma_l, ksi)' 
        ' Расчёт A.pgt - уже произведен в Flow, вызывать дополнительно не требуется' 
        """ Выходом будет являться A.pgt - Общий градиент давления"""
        


def H_L_theta(flow_pattern, calculator, pipe, out):
    
    'Эмпирические коэффициенты для объемного содержания жидкости\
    в горизонтальных режимах потока по Беггзу и Бриллу'
    
    if flow_pattern == "Кольцевой/расслоенный":
        a = 0.98; b = 0.4846; c = 0.0868
        e = 0.011; f = -3.768; g = 3.539; h = -1.614
        
    elif flow_pattern == "Снарядный/пробковый/прерывистый":
        a = 0.845; b = 0.5351; c = 0.0173            
        e = 2.96; f = 0.305; g = -0.4473; h = 0.0978
        
    elif flow_pattern == "Пузырьковый/эмульсионный/распределенный":
        a = 1.065; b = 0.5824; c = 0.0609             
        
    'Объемное содержание жидкости в горизонтальной трубе'    
    H_L_0 = a * calculator.lambda_l_d ** b / calculator.N_Fr_d ** c            
        
    'Коррекция на угол наклона'
    if flow_pattern == "Кольцевой/расслоенный"\
    or flow_pattern == "Снарядный/пробковый/прерывистый": 
        
        theta_d = math.pi / 180 * pipe.theta            

        C = max(0, (1 - calculator.lambda_l_d)\
        * math.log(e * calculator.lambda_l_d ** f\
        * calculator.N_Lv_d ** g * calculator.N_Fr_d ** h))
    
        psi = 1 + C * (math.sin(1.8 * theta_d)\
        - 0.333 * (math.sin(1.8 * theta_d)) ** 3)
        
    elif flow_pattern == "Пузырьковый/эмульсионный/распределенный": 
        C = 0; psi = 1
    
    'Объемное содержание жидкости с поправкой на угол + поправка Пэйна'        
    if pipe.theta > 0:
        'восходящее течение'
        out.H_L_theta_d = max(min(1, 0.924 * H_L_0 * psi), calculator.lambda_l_d)    
    else:
        out.H_L_theta_d = min(1, 0.685 * H_L_0 * psi)
    
    return out.H_L_theta_d
    
def f_Moody(calculator, pipe):
    if calculator.N_Re_d <= 2000:
        f = 64 / calculator.N_Re_d
    else:
        myfunc = lambda f: (1.74 - 2 * math.log(2 * pipe.ksi_m / pipe.d_m\
        + 18.7 / calculator.N_Re_d / math.sqrt(f) , 10)) ** (-2) - f     

        dermyfunc = lambda f: -1 - 37.4 / (f ** (3 / 2) * calculator.N_Re_d\
        * (18.7 / (math.sqrt(f) * calculator.N_Re_d)\
        + 2 * pipe.ksi_m / pipe.d_m) * (-2\
        * math.log(18.7 / (math.sqrt(f) * calculator.N_Re_d)\
        + 2 * pipe.ksi_m / pipe.d_m) / math.log(10) + 1.74) ** 3\
        * math.log(10))
        
        f = scipy.optimize.newton(myfunc, x0 = 0.025, fprime = dermyfunc, 
                                  tol=1e-12, maxiter=100)
    return f         
      
      
class Ansari:
    g = 9.81
    def Flow(self, vsl, vsg, denl, deng, visl, visg, teta, d, sigmal, ksi): 
        lyambdal = vsl / (vsl + vsg)
        pgc = 0 # для выхода, если fpat не annular        
        if lyambdal == 0:
            fpat = "singleG"    
        else:    
            if lyambdal >= 0.9999:
                fpat = "singleL"    
            else:
                dentp = denl * lyambdal + deng * (1 - lyambdal)
                vistp = visl * lyambdal + visg * (1 - lyambdal)
                vtp = vsl + vsg
                NRetp = dentp * vtp * d / vistp
                if NRetp < 2000: 
                    ftp = 64 / NRetp
                else:
                    myfunc = lambda ftp: 1.74-2*math.log(2*ksi/d+18.7/NRetp/math.sqrt(ftp),10)-1/math.sqrt(ftp)     
#                    dermyfunc = lambda ftp: 1/(2*ftp**(3/2)) + 18.7/(NRetp*ftp**(3/2)*(2*ksi/d + 18.7/(NRetp*sqrt(ftp)))*log(10))
#                    ftp = newton(myfunc, x0 = 0.02, fprime = dermyfunc, tol=1e-12, maxiter=50)
                    ftp = scipy.optimize.newton(myfunc, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
                
                'Переход "Рассеяный пузырьковый режим" - "Пузырьковый режим", кривая А карты режимов потока'
                if 2*(0.4*sigmal/(denl-deng)/Ansari.g)**0.5*(denl/sigmal)**(3/5)*(ftp/2/d)**(2/5)*(vsl+vsg)**1.2==0.725+4.15*(vsg/(vsg+vsl))**0.5:
                    fpat = "dbub"
                else:
                    'проверяем условие перехода к кольцевому режиму'
                    vsga = 3.1*(Ansari.g*sigmal*(denl-deng)/deng**2)**(1/4)
                    vcrit = 10000 * vsg * visg / sigmal * (deng / denl) ** (1 / 2)    
                    Fe = 1 - math.exp( - 0.125 * (vcrit - 1.5))
                    if Fe <0:
                        Fe = 0  # Предохранитель, Fe - часть общего объема жидкости, захваченная потоком газа (не может быть меньше 0)
                    lyambdalc = Fe * vsl / (Fe * vsl + vsg)
                    denc = denl * lyambdalc + deng * (1 - lyambdalc)
                    vissc = visl * lyambdalc + visg * (1 - lyambdalc)
                    vsc = Fe * vsl + vsg
                    NResc = denc * vsc * d / vissc
                    if NResc < 2000: 
                        '''поток ламинарный'''
                        fsc = 64 / NResc
                    else:
                        myfunc2 = lambda fsc: 1.74-2*math.log(2*ksi/d+18.7/NResc/math.sqrt(fsc),10)-1/math.sqrt(fsc)     
#                        dermyfunc2 = lambda fsc: 1/(2*fsc**(3/2)) + 18.7/(NResc*fsc**(3/2)*(2*ksi/d + 18.7/(NResc*sqrt(fsc)))*log(10))
#                        fsc = newton(myfunc2, x0 = 0.02, fprime = dermyfunc2, tol=1e-12, maxiter=50)
                        fsc = scipy.optimize.newton(myfunc2, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
                    
                    NResl = denl * vsl * d / visl
                    if NResl < 2000: 
                        '''поток ламинарный'''
                        fsl = 64 / NResl
                    else:
                        myfunc = lambda fsl: 1.74-2*math.log(2*ksi/d+18.7/NResl/math.sqrt(fsl),10)-1/math.sqrt(fsl)     
#                        dermyfunc = lambda fsl: 1/(2*fsl**(3/2)) + 18.7/(NResl*fsl**(3/2)*(2*ksi/d + 18.7/(NResl*sqrt(fsl)))*log(10))
#                        fsl = newton(myfunc, x0 = 0.02, fprime = dermyfunc, tol=1e-12, maxiter=50)
                        fsl = scipy.optimize.newton(myfunc, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
                    pgsc = fsc * denc * (vsc) ** 2 / (2 * d)
                    pgsl = fsl * denl * vsl ** 2 / (2 * d)
                    B = (1 - Fe) ** 2 * 1 # где 1 - это (ff/fsl) = 1 по примеру в Мукерджи Брилл, а как иначе найти ff?
                    xm = math.sqrt(B * pgsl / pgsc)
                    ym = Ansari.g * math.sin(teta * math.pi / 180) * (denl - denc) / pgsc
                    if Fe > 0.9:
                        myfunc3 = lambda dm: ym-(1+300*dm)/(4*dm*(1-dm)*(1-4*dm*(1-dm))**2.5)+xm**2/(4*dm*(1-dm))**3 
#                        dermyfunc3 = lambda dm: -(-20.0*dm + 10.0)*(300*dm + 1)*(-4*dm*(-dm + 1) + 1)**(-3.5)/(4*dm*(-dm + 1)) - 75*(-4*dm*(-dm + 1) + 1)**(-2.5)/(dm*(-dm + 1)) - (300*dm + 1)*(-4*dm*(-dm + 1) + 1)**(-2.5)/(4*dm*(-dm + 1)**2) + (300*dm + 1)*(-4*dm*(-dm + 1) + 1)**(-2.5)/(4*dm**2*(-dm + 1)) + 3*xm**2/(64*dm**3*(-dm + 1)**4) - 3*xm**2/(64*dm**4*(-dm + 1)**3)
        #                dermyfunc3 = lambda dm: float(-Decimal(-20.0*dm + 10.0)*Decimal(300*dm + 1)*Decimal(-4*dm*(-dm + 1) + 1)**Decimal(-3.5)/Decimal(4*dm*(-dm + 1)) - 75*Decimal(-4*dm*(-dm + 1) + 1)**Decimal(-2.5)/Decimal(dm*(-dm + 1)) - Decimal(300*dm + 1)*Decimal(-4*dm*(-dm + 1) + 1)**Decimal(-2.5)/Decimal(4*dm*(-dm + 1)**2) + Decimal(300*dm + 1)*Decimal(-4*dm*(-dm + 1) + 1)**Decimal(-2.5)/Decimal(4*dm**2*(-dm + 1)) + Decimal(3*xm)**Decimal(2)/Decimal(64*dm**3*(-dm + 1)**4) - Decimal(3*xm)**Decimal(2)/Decimal(64*dm**4*(-dm + 1)**3))
#                        dm = newton(myfunc3, x0 = 0.02, fprime = dermyfunc3, tol=1e-12, maxiter=50)
                        dm = scipy.optimize.newton(myfunc3, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
                        Z = 1 + 300 * dm
                    elif Fe < 0.9:
                        myfunc3 = lambda dm: ym-(1 + 24 * (denl / deng) ** (1 / 3) * dm)/(4*dm*(1-dm)*(1-4*dm*(1-dm))**2.5)+xm**2/(4*dm*(1-dm))**3 
        #                dermyfunc3 = lambda dm: -6*(denl/deng)**0.333333333333333*(-4*dm*(-dm + 1) + 1)**(-2.5)/(dm*(-dm + 1)) - (-20.0*dm + 10.0)*(24*dm*(denl/deng)**0.333333333333333 + 1)*(-4*dm*(-dm + 1) + 1)**(-3.5)/(4*dm*(-dm + 1)) - (24*dm*(denl/deng)**0.333333333333333 + 1)*(-4*dm*(-dm + 1) + 1)**(-2.5)/(4*dm*(-dm + 1)**2) + (24*dm*(denl/deng)**0.333333333333333 + 1)*(-4*dm*(-dm + 1) + 1)**(-2.5)/(4*dm**2*(-dm + 1)) + 3*xm**2/(64*dm**3*(-dm + 1)**4) - 3*xm**2/(64*dm**4*(-dm + 1)**3)
                        dm = scipy.optimize.newton(myfunc3, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
                        Z = 1 + 24 * (denl / deng) ** (1 / 3) * dm
                    hlf=4*dm*(1-dm) 
                    pgc = Z / (1 - 2 * dm) ** 5 * pgsc + denc * Ansari.g * math.sin (teta * math.pi / 180) 
                
                    if vsg<=vsga and (hlf+lyambdalc*(d-2*dm)**2/d**2)>0.12:
                        'по обоим критериям не кольцевой режим, проверим 2 критерия: может ли быть пузырьковый, и выполняется ли переход из пузырькового в пробковый'                
                        dmin=19.01*((denl-deng)*sigmal/denl**2/Ansari.g)**(1/2)
                        vs1=1.53 * (Ansari.g * sigmal * (denl - deng) / denl**2)**0.25
                        vsl1=3*(vsg-0.25*vs1*math.sin(teta * math.pi / 180))
                        
                        if dmin<d and vsl1<vsl:     
                            fpat="bub"
                        else:
                            fpat="slug"
                    elif vsg>vsga and (hlf+lyambdalc*(d-2*dm)**2/d**2)>0.12:   
                        'по первому критерию кольцевой, но по дополнительному - пленка не стабильна, пока предполагаем не кольцевой'
                        dmin=19.01*((denl-deng)*sigmal/denl**2/Ansari.g)**(1/2)
                        vs1=1.53 * (Ansari.g * sigmal * (denl - deng) / denl**2)**0.25
                        vsl1=3*(vsg-0.25*vs1*math.sin(teta * math.pi / 180))
                        
                        if dmin<d and vsl1<vsl:     
                            fpat="bub"
                        else:
                            fpat="slug"
                    else:    
                         fpat = "annular"
                         '''Градиент давления в плёнке и в газовом ядре одинаковые'''
        self.pgt = pgc        
        self.fpat = fpat
    
    def single(self, angr, di, ed, v, den, vis, P):
        pge = den * math.sin(angr) * Ansari.g
        RE = di * den * v / vis
        ff = 64 / RE
        if (RE > 2000):
            ff = 0.25 / (math.Log10(2 * ed / 3.7 - 5.02 / RE * math.Log10(2 * ed / 3.7 + 13 / RE))) ** 2
        pgf = 0.5 * den * ff * v * v / di
        ekk = den * v * v / P
        icrit = 0
        if (ekk > 0.95): 
            icrit = 1
        if (icrit == 1): 
            ekk = 0.95
        pgt = (pge + pgf) / (1 - ekk)
        pga = pgt * ekk
        pgt = (pge + pgf)    
        self.pge = pge; self.pgf = pgf; self.pga = pga; self.pgt = pgt        
        
    def bubble(self, vsl, vsg, denl, deng, fpat, visl, visg, teta, d, sigmal, ksi): 
        lyambdal = vsl / (vsl + vsg)            
        vm = vsl + vsg    #Rkh    
        if fpat == "dbub": 
            dentp = denl * lyambdal + deng * (1 - lyambdal)
            vistp = visl * lyambdal + visg * (1 - lyambdal)
            vtp = vsl + vsg # = vm
        '''Для учёта эффекта проскальзывания в аэрированном потоке, скорость
           подъема пузырьков газа связывают со скоростью смеси.
           Допуская существование турбулентного течения, можно предполагать,
           что концентрация поднимающихся пузырьков будет больше в центре трубы'''    
        if fpat == "bub":
            '''Неявно вычислим фактическое объемное содержание жидкости в аэрированном потоке'''              
            myfunc = lambda hl: 1.53 * (Ansari.g * sigmal * (denl - deng) / denl**2)**0.25*hl**0.5-vsg/(1-hl)+1.2*vm     
#            dermyfunc = lambda hl: 0.765*hl**(-0.5)*(g*sigmal*(-deng + denl)/denl**2)**0.25 - vsg/(-hl + 1)**2 - vsl/hl**2
#            hl = newton(myfunc, x0 = 0.5, fprime = dermyfunc, tol=1e-12, maxiter=50)    
            hl = scipy.optimize.newton(myfunc, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
            dentp = denl * hl + deng * (1 - hl)
            vistp = visl * hl + visg * (1 - hl)
            vtp = vsl + vsg # = vm # Rkh
        pge = dentp * Ansari.g * math.sin(teta * math.pi / 180)
        NRetp = dentp * vtp * d / vistp
        if NRetp < 2000: 
            '''поток ламинарный'''
            ftp = 64 / NRetp
        else:
            myfunc2 = lambda ftp: 1.74-2*math.log(2*ksi/d+18.7/NRetp/math.sqrt(ftp),10)-1/math.sqrt(ftp)     
#            dermyfunc2 = lambda ftp: 1/(2*ftp**(3/2)) + 18.7/(NRetp*ftp**(3/2)*(2*ksi/d + 18.7/(NRetp*sqrt(ftp)))*log(10))
#            ftp = newton(myfunc2, x0 = 0.02, fprime = dermyfunc2, tol=1e-12, maxiter=50)
            ftp = scipy.optimize.newton(myfunc2, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
        pgf = ftp * dentp * (vtp) ** 2 / (2 * d)
        '''Поскольку в пузырьковом режиме доминирует жидкая фаза, которая
           является относительно несжимаемой, значительного изменения плотности
           флюидов не происходит. Скорость флюида остается постоянной, 
           поэтому падения давления вследствии ускорения не наблюдается.
           Составляющей по ускорению можно пренебречь.'''
        pga = 0
        pgt = pge + pgf + pga
        self.pge = pge; self.pgf = pgf; self.pga = pga; self.pgt = pgt
                    
        
         
    def slug(self, vsl, vsg, denl, deng, fpat, visl, visg, teta, d, sigmal, ksi): 
        vm = vsl + vsg # Rkh        
        vtb = 1.2 * vm + 0.35 * (Ansari.g * d * (denl - deng) / denl) ** 0.5
        hgls = vsg / (0.425 + 2.65 * vm)
        hlls = 1 - hgls        
        vgls = 1.2 * vm + 1.53 * (Ansari.g * sigmal * (denl - deng) / (denl) ** 2) ** (1 / 4) * (hlls) ** 0.5
        A = hgls*(vtb-vgls)+vm
        myfunc = lambda hltb: (9.916*math.sqrt(Ansari.g*d))*(1-math.sqrt(1-hltb))**0.5*hltb-vtb*(1-hltb)+A     
#        dermyfunc = lambda hltb: 2.479*hltb*sqrt(d*g)*(-sqrt(-hltb + 1) + 1)**(-0.5)/sqrt(-hltb + 1) + vtb + 9.916*sqrt(d*g)*(-sqrt(-hltb + 1) + 1)**0.5
#        hltb = newton(myfunc, x0 = 0.5, fprime = dermyfunc, tol=1e-12, maxiter=50)    
        hltb = scipy.optimize.newton(myfunc, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
        
        #hgtb = 1 - hltb
        #vltb = 9.916 * (g * d * (1 - sqrt(hgtb))) ** (1 / 2)
        #vlls = (vtb * hlls - (vtb + vltb) * hltb) / hlls
        vgtb = (vtb * (1 - hltb) - (vtb - vgls) * (1 - hlls)) / (1 - hltb)
        beta = (vsg - vgls * (1 - hlls)) / (vgtb - vgtb * hltb - vgls * (1 - hlls))
        ''' ? принимается Lls = 30d ? - длина пробки жидкости'''
        #lls = 30 * d
        #lsu = lls / (1 - beta)
        #ltb = lsu - lls
        denls = denl * hlls + deng * (1 - hlls)
        pge = ((1 - beta) * denls + beta * deng) * Ansari.g * math.sin(teta * math.pi / 180)
        visls = visl * hlls + visg * (1 - hlls)
        NRels = denls * vm * d / visls
        if NRels < 2000: 
            '''поток ламинарный'''
            fls = 64 / NRels
        else:
            myfunc2 = lambda fls: 1.74-2*math.log(2*ksi/d+18.7/NRels/math.sqrt(fls),10)-1/math.sqrt(fls)     
#            dermyfunc2 = lambda fls: 1/(2*fls**(3/2)) + 18.7/(NRels*fls**(3/2)*(2*ksi/d + 18.7/(NRels*sqrt(fls)))*log(10))
#            fls = newton(myfunc2, x0 = 0.02, fprime = dermyfunc2, tol=1e-12, maxiter=50)
            fls = scipy.optimize.newton(myfunc2, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
        pgf = fls * denls * (vm) ** (2) / (2 * d) * (1 - beta)
        '''составляющей градиента давления по ускорению пренебрегаем'''
        pga = 0
        pgt = pge + pgf + pga
        self.pge = pge; self.pgf = pgf; self.pga = pga; self.pgt = pgt
             
                
                
    def annular(self, vsl, vsg, denl, deng, fpat, visl, visg, teta, d, sigmal, ksi):
        vcrit = 10000 * vsg * visg / sigmal * (deng / denl) ** (1 / 2)
        Fe = 1 - math.exp( - 0.125 * (vcrit - 1.5))
        lyambdalc = Fe * vsl / (Fe * vsl + vsg)
        denc = denl * lyambdalc + deng * (1 - lyambdalc)
        NResl = denl * vsl * d / visl
        if NResl < 2000: 
            '''поток ламинарный'''
            fsl = 64 / NResl
        else:
            myfunc = lambda fsl: 1.74-2*math.log(2*ksi/d+18.7/NResl/math.sqrt(fsl),10)-1/math.sqrt(fsl)     
#            dermyfunc = lambda fsl: 1/(2*fsl**(3/2)) + 18.7/(NResl*fsl**(3/2)*(2*ksi/d + 18.7/(NResl*sqrt(fsl)))*log(10))
#            fsl = newton(myfunc, x0 = 0.02, fprime = dermyfunc, tol=1e-12, maxiter=50)
            fsl = scipy.optimize.newton(myfunc, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
        pgsl = fsl * denl * vsl ** 2 / (2 * d)
        vsc = Fe * vsl + vsg
        vissc = visl * lyambdalc + visg * (1 - lyambdalc)
        NResc = denc * vsc * d / vissc
        if NResc < 2000: 
            '''поток ламинарный'''
            fsc = 64 / NResc
        else:
            myfunc2 = lambda fsc: 1.74-2*math.log(2*ksi/d+18.7/NResc/math.sqrt(fsc),10)-1/math.sqrt(fsc)     
#            dermyfunc2 = lambda fsc: 1/(2*fsc**(3/2)) + 18.7/(NResc*fsc**(3/2)*(2*ksi/d + 18.7/(NResc*sqrt(fsc)))*log(10))
#            fsc = newton(myfunc2, x0 = 0.02, fprime = dermyfunc2, tol=1e-12, maxiter=50)
            fsc = scipy.optimize.newton(myfunc2, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
        pgsc = fsc * denc * (vsc) ** 2 / (2 * d)
        B = (1 - Fe) ** 2 * 1 # где 1 - это (ff/fsl) = 1 по примеру в Мукерджи Брилл, а как иначе найти ff?
        xm = math.sqrt(B * pgsl / pgsc)
        ym = Ansari.g * math.sin(teta * math.pi / 180) * (denl - denc) / pgsc
        if Fe > 0.9:
            myfunc3 = lambda dm: ym-(1+300*dm)/(4*dm*(1-dm)*(1-4*dm*(1-dm))**2.5)+xm**2/(4*dm*(1-dm))**3 
#            dermyfunc3 = lambda dm: -(-20.0*dm + 10.0)*(300*dm + 1)*(-4*dm*(-dm + 1) + 1)**(-3.5)/(4*dm*(-dm + 1)) - 75*(-4*dm*(-dm + 1) + 1)**(-2.5)/(dm*(-dm + 1)) - (300*dm + 1)*(-4*dm*(-dm + 1) + 1)**(-2.5)/(4*dm*(-dm + 1)**2) + (300*dm + 1)*(-4*dm*(-dm + 1) + 1)**(-2.5)/(4*dm**2*(-dm + 1)) + 3*xm**2/(64*dm**3*(-dm + 1)**4) - 3*xm**2/(64*dm**4*(-dm + 1)**3)
#            dm = newton(myfunc3, x0 = 0.02, fprime = dermyfunc3, tol=1e-12, maxiter=50)
            dm = scipy.optimize.newton(myfunc3, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
            Z = 1 + 300 * dm
        elif Fe < 0.9:
            myfunc3 = lambda dm: ym-(1 + 24 * (denl / deng) ** (1 / 3) * dm)/(4*dm*(1-dm)*(1-4*dm*(1-dm))**2.5)+xm**2/(4*dm*(1-dm))**3 
#            dermyfunc3 = lambda dm: -6*(denl/deng)**0.333333333333333*(-4*dm*(-dm + 1) + 1)**(-2.5)/(dm*(-dm + 1)) - (-20.0*dm + 10.0)*(24*dm*(denl/deng)**0.333333333333333 + 1)*(-4*dm*(-dm + 1) + 1)**(-3.5)/(4*dm*(-dm + 1)) - (24*dm*(denl/deng)**0.333333333333333 + 1)*(-4*dm*(-dm + 1) + 1)**(-2.5)/(4*dm*(-dm + 1)**2) + (24*dm*(denl/deng)**0.333333333333333 + 1)*(-4*dm*(-dm + 1) + 1)**(-2.5)/(4*dm**2*(-dm + 1)) + 3*xm**2/(64*dm**3*(-dm + 1)**4) - 3*xm**2/(64*dm**4*(-dm + 1)**3)
            dm = scipy.optimize.newton(myfunc3, x0 = 1*10**(-12), fprime = None, tol=1e-12, maxiter=50)
            Z = 1 + 24 * (denl / deng) ** (1 / 3) * dm
        pgc = Z / (1 - 2 * dm) ** 5 * pgsc + denc * Ansari.g * math.sin (teta * math.pi / 180) 
        '''Градиент давления в плёнке и в газовом ядре одинаковые'''
        self.pgt = pgc
      
      
      
if __name__ == "__main__":
    print("Вы запустили модуль напрямую, а не импортировали его.") 
    input ("\n\nНажмите Enter, чтобы выйти.")     