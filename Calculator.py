# -*- coding: utf-8 -*-
"""
Created on Thu Aug 27 13:55:32 2015

@author: KhalikovRuslan

Расчет вспомогательных величин

"""
import math

class Calculator():
    
    g_msec2 = 9.81 
    
    'Значение плотности для расчёта плотности нефти из входного параметра\
    удельная плотность нефти [усл. плот. воды при стандартных условиях]'    
    rho_ref = 1000
    
    rho_air = 1.2217 # плотность воздуха при стандартных условиях    
    
    def __init__(self, pvt, pipe, mode_of_working):

        self.q_gsc_m3day = None; self.q_gsc(mode_of_working.R_p_m3m3, 
                                            mode_of_working.q_osc_m3day)
             
        self.q_wsc_m3day = None; self.q_wsc(mode_of_working.WCT_d, 
                                            mode_of_working.q_osc_m3day)     
             
        self.rho_osc_kgm3 = None; self.rho_osc(pvt.gamma_o_d) 
        
        self.rho_gsc_kgm3 = None; self.rho_gsc(pvt.gamma_g_d)
             
        self.a_p_m2 = None; self.a_p(pipe.d_m)
        
        self.q_o_m3day = None; self.q_o(mode_of_working.q_osc_m3day, 
                                        pvt.b_o_m3m3)
        
        self.q_w_m3day = None; self.q_w(pvt.b_w_m3m3)
        
        self.q_l_m3day = None; self.q_l()
        
        self.q_g_m3day = None; self.q_g(pvt.b_g_m3m3, pvt.r_s_m3m3, 
                                        mode_of_working.q_osc_m3day)
        self.f_w_d = None; self.f_w()
        
        self.lambda_l_d = None; self.lambda_l()
        
        self.rho_o_kgm3 = None; self.rho_o(pvt.r_s_m3m3, pvt.b_o_m3m3)
        
        self.rho_w_kgm3 = None; self.rho_w(pvt.rho_w_sc_kgm3, pvt.b_w_m3m3)
        
        self.rho_l_kgm3 = None; self.rho_l()
        
        self.rho_g_kgm3 = None; self.rho_g(pvt.b_g_m3m3)
        
        self.sigma_l_Newtonm = None; self.sigma_l(pvt.sigma_o_Newtonm, 
                                                  pvt.sigma_w_Newtonm)        
        self.mu_l_cP = None; self.mu_l(pvt.mu_o_cP, pvt.mu_w_cP)
        
        self.rho_n_kgm3 = None; self.rho_n()
        
        self.mu_n_cP = None; self.mu_n(pvt.mu_g_cP)
        
        self.v_sl_msec = None; self.v_sl()
        
        self.v_sg_msec = None; self.v_sg()
                
        self.v_m_msec = None; self.v_m()
        
        self.N_Re_d = None; self.N_Re(pipe.d_m)        
        
        self.N_Fr_d = None; self.N_Fr(pipe.d_m)
        
        self.N_Lv_d = None; self.N_Lv()
        
        self.E_d = None; self.E(pipe.ksi_m, pipe.d_m)
        
    'Плотность газа в стандартных условиях'
    def rho_gsc(self, gamma_g_d):
        self.rho_gsc_kgm3 = gamma_g_d * Calculator.rho_air                 
               
    'Плотность нефти в стандартных условиях'       
    def rho_osc(self, gamma_o_d):       
        self.rho_osc_kgm3 = gamma_o_d * Calculator.rho_ref
            
    'Дебит воды в стандартных условиях'    
    def q_wsc(self, WCT_d, q_osc_m3day):
        self.q_wsc_m3day = WCT_d * q_osc_m3day / (1 - WCT_d)
        
    'Дебит газа в стандартных условиях'            
    def q_gsc(self, R_p_m3m3, q_osc_m3day):        
        self.q_gsc_m3day = R_p_m3m3 * q_osc_m3day
        
    'поперечное сечение трубы'
    def a_p(self, d_m):
        self.a_p_m2 = math.pi * d_m ** 2 / 4
        
    'дебит нефти при опорном давлении'    
    def q_o(self, q_osc_m3day, b_o_m3m3):
        self.q_o_m3day = q_osc_m3day * b_o_m3m3
    
    'дебит воды при опорном давлении'    
    def q_w(self,  b_w_m3m3):        
        self.q_w_m3day = self.q_wsc_m3day * b_w_m3m3
     
    'дебит жидкости при опорном давлении'   
    def q_l(self):
        self.q_l_m3day = self.q_o_m3day + self.q_w_m3day
        
    'дебит газа при опорном давлении'
    def q_g(self, b_g_m3m3, r_s_m3m3, q_osc_m3day):
        self.q_g_m3day = b_g_m3m3 * (self.q_gsc_m3day - r_s_m3m3 * q_osc_m3day)        
            
    'объемное содержание воды при опорном давлении'
    def f_w(self):
        self.f_w_d = self.q_w_m3day / self.q_l_m3day
        
    'объемное содержание жидкости в предположении отсутствия проскальзывания'
    def lambda_l(self):
        self.lambda_l_d = self.q_l_m3day /(self.q_l_m3day + self.q_g_m3day)

    'плотность нефти'        
    def rho_o(self, r_s_m3m3, b_o_m3m3):
        self.rho_o_kgm3 = (self.rho_osc_kgm3 + r_s_m3m3\
        * self.rho_gsc_kgm3) / b_o_m3m3

    'плотность воды'
    def rho_w(self, rho_w_sc_kgm3, b_w_m3m3):
        self.rho_w_kgm3 = rho_w_sc_kgm3 / b_w_m3m3
    
    'плотность жидкости'    
    def rho_l(self):
        self.rho_l_kgm3 = self.rho_o_kgm3 * (1 - self.f_w_d)\
        + self.rho_w_kgm3 * self.f_w_d
    
    'плотность газа'   
    def rho_g(self, b_g_m3m3):
        self.rho_g_kgm3 = self.rho_gsc_kgm3 / b_g_m3m3
    
    'поверхностное натяжение в системе жидкость-газ'
    def sigma_l(self, sigma_o_Newtonm, sigma_w_Newtonm):
        self.sigma_l_Newtonm = sigma_o_Newtonm * (1 - self.f_w_d)\
        + sigma_w_Newtonm * self.f_w_d 

    'вязкость жидкости'
    def mu_l(self, mu_o_cP, mu_w_cP):
        self.mu_l_cP = mu_o_cP * (1 - self.f_w_d) + mu_w_cP * self.f_w_d

    'плотность нефти при отсутствии проскальзывания'  
    def rho_n(self):
        self.rho_n_kgm3 = self.rho_l_kgm3 * self.lambda_l_d + self.rho_g_kgm3\
        * (1 - self.lambda_l_d)
    
    'вязкость жидкости при отсутствии проскальзывания'    
    def mu_n(self, mu_g_cP):
        self.mu_n_cP = self.mu_l_cP * self.lambda_l_d + mu_g_cP\
        * (1 - self.lambda_l_d)

    'приведенная скорость жидкости'   
    def v_sl(self):
        self.v_sl_msec = 0.000011574 * self.q_l_m3day / self.a_p_m2
    
    'приведенная скорость газа'
    def v_sg(self):
        self.v_sg_msec = 0.000011574 * self.q_g_m3day / self.a_p_m2
    
    'приведенная скорость смеси'
    def v_m(self):
        self.v_m_msec = self.v_sl_msec + self.v_sg_msec
    
    'число Рейнольдса'    
    def N_Re(self, d_m):
        self.N_Re_d = 1000 * self.rho_n_kgm3 * self.v_m_msec * d_m\
        / self.mu_n_cP

    'число Фруда'
    def N_Fr(self, d_m):
        self.N_Fr_d = self.v_m_msec ** 2 / (Calculator.g_msec2 * d_m)

    'число скорости жидкости'
    def N_Lv(self):
        self.N_Lv_d = self.v_sl_msec * (self.rho_l_kgm3\
        / (Calculator.g_msec2 * self.sigma_l_Newtonm)) ** 0.25 

    'относительная шероховатость трубы'
    def E(self, ksi_m, d_m):
        self.E_d = ksi_m / d_m
        
if __name__ == "__main__":
    print("Вы запустили модуль напрямую, а не импортировали его.") 
    input ("\n\nНажмите Enter, чтобы выйти.")        