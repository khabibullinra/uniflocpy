# -*- coding: utf-8 -*-
"""
Created on Wed Sep  2 11:51:51 2015

@author: KhalikovRuslan

Модуль выходных параметров:
1) Запись массивов;
2) Вывод по запросу;
3) Отображение.
"""

import matplotlib.pyplot as plt
import matplotlib.pyplot as mpl
import matplotlib.text as mtext
from mpl_toolkits.mplot3d import Axes3D
mpl.rcParams['font.family'] = 'fantasy'
mpl.rcParams['font.fantasy'] = 'Times New Roman'

class Output():
    def __init__(self):
        self.flow_pattern = None
        self.gradP_g_atmm = None
        self.gradP_f_atmm = None
        self.gradP_atmm = None
        self.H_L_theta_d = None
        self.md_m_array = []
        self.p_atm_array = []
        self.gradP_g_atmm_array = []
        self.gradP_f_atmm_array = []
        self.gradP_atmm_array = []
        self.T_C_array = []
        self.flow_pattern_array = []
        self.P_b_atm_array = []
        self.r_s_m3m3_array = []
        self.b_o_m3m3_array = []
        self.mu_o_cP_array = []
        self.z_d_array = []
        self.b_g_m3m3_array = []
        self.mu_g_cP_array = []
        self.b_w_m3m3_array = []
        self.mu_w_cP_array = []
        self.IPR_array = []
        self.P_test_atm_array = []
        self.P_VLP_atm_array = []
        self.VLP_array = []        
        self.lambda_l_d_array = []        
        self.N_Fr_d_array = []        
        self.d_m_array = []        
        self.H_L_theta_d = None
        
        self.L_1_1_array = []; self.lambda_l_d_1_1 = []
        self.L_1_2_array = []; self.lambda_l_d_1_2 = []        
        self.L_2_array = []; self.lambda_l_d_2 = []
        self.L_3_array = []; self.lambda_l_d_3 = []
        self.L_4_array = []; self.lambda_l_d_4 = []        
        
    def save(self, P_atm, T_C, L_m, pvt, calculator, pipe):
        self.md_m_array.append('%5.0f' % L_m)
        self.p_atm_array.append(P_atm)
        self.gradP_g_atmm_array.append(self.gradP_g_atmm)
        self.gradP_f_atmm_array.append(self.gradP_f_atmm)
        self.gradP_atmm_array.append(self.gradP_atmm)
        self.T_C_array.append(T_C)
        self.flow_pattern_array.append(self.flow_pattern)
        self.P_b_atm_array.append(pvt.p_b_MPa * 10)
        self.r_s_m3m3_array.append(pvt.r_s_m3m3)
        self.b_o_m3m3_array.append(pvt.b_o_m3m3)
        self.mu_o_cP_array.append(pvt.mu_o_cP)
        self.z_d_array.append(pvt.z_d)
        self.b_g_m3m3_array.append(pvt.b_g_m3m3)
        self.mu_g_cP_array.append(pvt.mu_g_cP)        
        self.b_w_m3m3_array.append(pvt.b_w_m3m3)
        self.mu_w_cP_array.append(pvt.mu_w_cP)
        self.lambda_l_d_array.append(calculator.lambda_l_d)        
        self.N_Fr_d_array.append(calculator.N_Fr_d)
        self.d_m_array.append(pipe.d_m)        
        
    def save_PVT(self, P_atm, T_C, pvt):
        self.p_atm_array.append(P_atm)

        self.T_C_array.append(T_C)

        self.P_b_atm_array.append(pvt.p_b_MPa * 10)
        self.r_s_m3m3_array.append(pvt.r_s_m3m3)
        self.b_o_m3m3_array.append(pvt.b_o_m3m3)
        self.mu_o_cP_array.append(pvt.mu_o_cP)
        self.z_d_array.append(pvt.z_d)
        self.b_g_m3m3_array.append(pvt.b_g_m3m3)
        self.mu_g_cP_array.append(pvt.mu_g_cP)        
        self.b_w_m3m3_array.append(pvt.b_w_m3m3)
        self.mu_w_cP_array.append(pvt.mu_w_cP)
        
        
        
        
        
        
        
    def save_massive(self, A, B):
        A.append(B)        
                
    def save_IPR(self, Reservoir, P_test_atm):
        self.IPR_array.append(Reservoir.Q_m3day)
        self.P_test_atm_array.append(P_test_atm)        
        
    def save_VLP(self, P_atm, q_m3day):
        self.VLP_array.append(q_m3day)
        self.P_VLP_atm_array.append(P_atm)        
        
    def plot(self, i, title, xlabel, ylabel, ylim_sn, ylim_en, xlim_sn, 
             xlim_en, x, y, c, legen):
        plt.figure(i)
        plt.title(title, color='black', family='fantasy', fontsize='x-large')
        plt.xlabel(xlabel, color='black', family='fantasy', fontsize='x-large')
        plt.ylabel(ylabel, color='black', family='fantasy', fontsize='x-large')       
        plt.grid(True)
        plt.ylim(ylim_sn, ylim_en)
        plt.xlim(xlim_sn, xlim_en)        
        plt.plot(x, y, c, linewidth=3, label=legen)
        plt.legend(loc='best')
        plt.show()

    def plot_3d(self, i, title, xlabel, ylabel, ylim_sn, ylim_en, xlim_sn, 
             xlim_en, zlim_sn, zlim_en, x, y, z, legen):
        fig = plt.figure(i)
        ax = fig.gca(projection='3d')
        ax.set_xlim3d(xlim_sn, xlim_en)
        ax.set_ylim3d(ylim_sn, ylim_en)
        ax.set_zlim3d(ylim_sn, ylim_en)        
        ax.set_zlim3d(0, 10)        
        ax.set_xlabel('X axis')
        ax.set_ylabel('Y axis')
        ax.set_zlabel('Z axis')
        plt.plot(x, y, z, label='parametric curve')
        plt.show()
        
    def plot_flow_map_Beggs_Brill_3d(self, i, Pipe):
        self.L_1_1_array = []; self.lambda_l_d_1_1 = []
        self.L_1_2_array = []; self.lambda_l_d_1_2 = []        
        self.L_2_array = []; self.lambda_l_d_2 = []
        self.L_3_array = []; self.lambda_l_d_3 = []
        self.L_4_array = []; self.lambda_l_d_4 = []
        fig = plt.figure(i)
        ax = fig.gca(projection='3d')
        ax.set_title('Карта режимов потока', color='black', family='fantasy', fontsize='x-large')        
        ax.set_xlim3d(0, 1)
        ax.set_ylim3d(0, 1000)
        ax.set_zlim3d(Pipe.length_m, 0)
        ax.set_xlabel('Входное содержание жидкости', color='black', family='fantasy', fontsize='x-large')
        ax.set_ylabel('Число Фруда', color='black', family='fantasy', fontsize='x-large')
        ax.set_zlabel('Глубина', color='black', family='fantasy', fontsize='x-large')
        plt.grid()        
        for lambda_l_d in (0.0001, 0.001, 0.01005):        
            L_1 = 316 * lambda_l_d ** 0.302
            self.L_1_1_array.append(L_1)
            self.lambda_l_d_1_1.append(lambda_l_d)
        for lambda_l_d in (0.01005, 0.1, 0.4001):        
            L_1 = 316 * lambda_l_d ** 0.302
            self.L_1_2_array.append(L_1)
            self.lambda_l_d_1_2.append(lambda_l_d)         
        for lambda_l_d in (0.01005, 0.1, 1):        
            L_2 = 0.000925 * lambda_l_d ** (-2.468)
            self.L_2_array.append(L_2)
            self.lambda_l_d_2.append(lambda_l_d)
        for lambda_l_d in (0.0101, 0.1, 1):    
            L_3 = 0.10 * lambda_l_d ** (-1.452)
            self.L_3_array.append(L_3)
            self.lambda_l_d_3.append(lambda_l_d)            
        for lambda_l_d in (0.4001, 1):        
            L_4 = 0.5 * lambda_l_d ** (-6.738)
            self.L_4_array.append(L_4)
            self.lambda_l_d_4.append(lambda_l_d)
        ax.text(0.01, 25, Pipe.length_m, 'Расслоенный', fontsize = 10, color = 'red')             
        ax.text(0.3, 75, Pipe.length_m, 'Прерывистый', fontsize = 18, color = 'green')     
        ax.text(0.2, 250, Pipe.length_m, 'Распределенный', fontsize = 18, color = 'black') 
        ax.text(0.09, 0.3, Pipe.length_m, 'Переходный', fontsize = 10, color = 'blue') 
        plt.plot(self.lambda_l_d_1_1, self.L_1_1_array, Pipe.length_m, 'r', linewidth=3)
        plt.plot(self.lambda_l_d_1_2, self.L_1_2_array, Pipe.length_m, 'k', linewidth=3)
        plt.plot(self.lambda_l_d_2, self.L_2_array, Pipe.length_m, 'b', linewidth=3)
        plt.plot(self.lambda_l_d_3, self.L_3_array, Pipe.length_m, 'g', linewidth=3)
        plt.plot(self.lambda_l_d_4, self.L_4_array, Pipe.length_m, 'k', linewidth=3)        
        
        x_massive_1 = []
        x_massive_2 = []
        x_massive_3 = []
        x_massive_4 = []
        
        y_massive_1 = []
        y_massive_2 = []
        y_massive_3 = []
        y_massive_4 = []            
        
        z_massive_1 = []
        z_massive_2 = []
        z_massive_3 = []
        z_massive_4 = []
        
        
        for i in range(1, len(self.flow_pattern_array) - 1):
            if self.flow_pattern_array[i] == 'Расслоенный':
                x_massive_1.append(self.lambda_l_d_array[i])
                y_massive_1.append(self.N_Fr_d_array[i])        
                z_massive_1.append(self.md_m_array[i])
            if self.flow_pattern_array[i] == 'Прерывистый':
                x_massive_2.append(self.lambda_l_d_array[i])
                y_massive_2.append(self.N_Fr_d_array[i])        
                z_massive_2.append(self.md_m_array[i])        
            if self.flow_pattern_array[i] == 'Распределенный':
                x_massive_3.append(self.lambda_l_d_array[i])
                y_massive_3.append(self.N_Fr_d_array[i])        
                z_massive_3.append(self.md_m_array[i])        
            if self.flow_pattern_array[i] == 'Переходный':
                x_massive_4.append(self.lambda_l_d_array[i])
                y_massive_4.append(self.N_Fr_d_array[i])        
                z_massive_4.append(self.md_m_array[i])       
        
      
        plt.plot(x_massive_1, y_massive_1, z_massive_1, 'r', linewidth=3)
        plt.plot(x_massive_2, y_massive_2, z_massive_2, 'g', linewidth=3)        
        plt.plot(x_massive_3, y_massive_3, z_massive_3, 'k', linewidth=3)        
        plt.plot(x_massive_4, y_massive_4, z_massive_4, 'b', linewidth=3)        
        plt.show()

    def plot_flow_map_Beggs_Brill(self, i):
        self.L_1_1_array = []; self.lambda_l_d_1_1 = []
        self.L_1_2_array = []; self.lambda_l_d_1_2 = []        
        self.L_2_array = []; self.lambda_l_d_2 = []
        self.L_3_array = []; self.lambda_l_d_3 = []
        self.L_4_array = []; self.lambda_l_d_4 = [] 
        
        plt.figure(i)
        plt.title('Карта режимов потока', color='black', family='fantasy', fontsize='x-large')
        plt.xscale('log')
        plt.yscale('log')        
        plt.xlabel('Входное содержание жидкости', color='black', family='fantasy', fontsize='x-large')
        plt.ylabel('Число Фруда', color='black', family='fantasy', fontsize='x-large')       
        plt.grid()
        plt.ylim(0.1, 1000)
        plt.xlim(0.0001, 1)        
        for lambda_l_d in (0.0001, 0.001, 0.01005):        
            L_1 = 316 * lambda_l_d ** 0.302
            self.L_1_1_array.append(L_1)
            self.lambda_l_d_1_1.append(lambda_l_d)
        for lambda_l_d in (0.01005, 0.1, 0.4001):        
            L_1 = 316 * lambda_l_d ** 0.302
            self.L_1_2_array.append(L_1)
            self.lambda_l_d_1_2.append(lambda_l_d)         
        for lambda_l_d in (0.01005, 0.1, 1):        
            L_2 = 0.000925 * lambda_l_d ** (-2.468)
            self.L_2_array.append(L_2)
            self.lambda_l_d_2.append(lambda_l_d)
        for lambda_l_d in (0.0101, 0.1, 1):    
            L_3 = 0.10 * lambda_l_d ** (-1.452)
            self.L_3_array.append(L_3)
            self.lambda_l_d_3.append(lambda_l_d)            
        for lambda_l_d in (0.4001, 1):        
            L_4 = 0.5 * lambda_l_d ** (-6.738)
            self.L_4_array.append(L_4)
            self.lambda_l_d_4.append(lambda_l_d)
        plt.text(0.0004, 3, 'Расслоенный', fontsize = 20, color = 'red')             
        plt.text(0.03, 25, 'Прерывистый', fontsize = 20, color = 'green')     
        plt.text(0.001, 400, 'Распределенный', fontsize = 20, color = 'black') 
        plt.text(0.09, 0.3, 'Переходный', fontsize = 20, color = 'blue') 
        plt.plot(self.lambda_l_d_1_1, self.L_1_1_array, 'r', linewidth=3)
        plt.plot(self.lambda_l_d_1_2, self.L_1_2_array, 'k', linewidth=3)
        plt.plot(self.lambda_l_d_2, self.L_2_array, 'b', linewidth=3)
        plt.plot(self.lambda_l_d_3, self.L_3_array, 'g', linewidth=3)
        plt.plot(self.lambda_l_d_4, self.L_4_array, 'k', linewidth=3)        
        plt.show()

if __name__ == "__main__":
    print("Вы запустили модуль напрямую, а не импортировали его.") 
    input ("\n\nНажмите Enter, чтобы выйти.")          