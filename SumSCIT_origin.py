# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 00:42:07 2015
@author: KhalikovRuslan
"""
from PyQt4 import QtCore, QtGui, uic
from pylab import *
import matplotlib as mpl
import sys
sys.path.insert(0, 'F:\\Python\\SumSCIT\\uniflocpy')
from SumSCIT_base import*
mpl.rcParams['font.family'] = 'fantasy'
mpl.rcParams['font.fantasy'] = 'Times New Roman'
# '''
#                 ВНИМАНИЕ
#              ИСПРАВЬТЕ ПУТЬ
#              РАБОЧЕГО МОДУЛЯ
#        (путь к файлу Рабочий_модуль.py)
# '''
class MyWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        uic.loadUi("SumSCIT_form.ui", self)
        self.Input_Data_2.setVisible(0)
        self.MC_2.setVisible(0)
        self.Result_2.setVisible(0)
        self.setWindowTitle("SumSCIT - 2015, Python Edition")
        self.setWindowIcon(QtGui.QIcon("SumSCIT_icon.png"))
        app.setWindowIcon(QtGui.QIcon("SumSCIT_icon.png"))
        self.connect(self.pushButton_4, QtCore.SIGNAL("clicked()"),
                     self.invisibleresult)
        self.connect(self.Result, QtCore.SIGNAL("triggered()"),
                     self.visibleresult)
        self.connect(self.QPushButton, QtCore.SIGNAL("clicked()"),
                     self.invisibledata)
        self.connect(self.pushButton, QtCore.SIGNAL("clicked()"),
                     self.invisiblemc)
        self.connect(self.Input_Data, QtCore.SIGNAL("triggered()"),
                     self.visibledata)
        self.connect(self.MC, QtCore.SIGNAL("triggered()"),
                     self.visiblemc)
        self.connect(self.pushButton_2, QtCore.SIGNAL("clicked()"),
                     self.calculate)
        self.connect(self.pushButton_3, QtCore.SIGNAL("clicked()"),
                     self.opt_valve)                     

    def visibleresult(self):
        self.Result_2.setVisible(1)
        self.Input_Data_2.setVisible(0)
        self.MC_2.setVisible(0)

    def invisibleresult(self):
        self.Result_2.setVisible(0)

    def visibledata(self):
        self.Input_Data_2.setVisible(1)
        self.Result_2.setVisible(0)
        self.MC_2.setVisible(0)

    def invisibledata(self):
        self.Input_Data_2.setVisible(0)

    def visiblemc(self):
        self.MC_2.setVisible(1)
        self.Input_Data_2.setVisible(0)
        self.Result_2.setVisible(0)

    def invisiblemc(self):
        self.MC_2.setVisible(0)

    def calculate(self):
        d = float(self.lineEdit.text())  # Пример: 0.062   # [м] Внутренний диаметр трубы
        q_osc = float(self.lineEdit_2.text())  # Пример: 100    # [м3/сут] Дебит нефти в стандартных условиях
        teta = float(self.lineEdit_3.text())  # Пример: 90      # [градусы] Угол наклона трубы
        ksi = float(self.lineEdit_4.text())  # 0.0002 # [м] Шероховатость стенки
        length = float(self.lineEdit_16.text())  # Пример: 2400   # [м] Длина трубы
        p_sn = float(self.lineEdit_5.text())  # 28 # [атм] Давление на начальном узле
        t_sn = float(self.lineEdit_6.text())  # 30 # [*C] Температура начального узла
        t_en = float(self.lineEdit_7.text())  # 40 # [*C] Температура конечного узла
        WCT = float(self.lineEdit_8.text())  # 0.08 # [доли ед.] Обводненность в стандартных условиях
        gamma_o = float(self.lineEdit_9.text())  # 0.836# [доли ед.] Удельная плотность нефти по воде
        gamma_g = float(self.lineEdit_10.text())  # 0.8 # [доли ед.] Удельная плотность газа по воздуху
        R_p = float(self.lineEdit_11.text())  # 400 # [м3/м3] Отношение добываемого газа к жидкости
        n_n = float(self.lineEdit_12.text())  # 10 # [-] число расчётных узлов
        Salinity = float(self.lineEdit_13.text())  # -1 # [мг/л] Солёность воды. Если не задана солёность воды,
        # то принимаем опциональное значение = 50000 мг/л.
        Calc_p_out = float(self.lineEdit_14.text())  # 0  # [-] Если индикатор = 0, то рассчитываем поток сверху вниз,
        # иначе снизу вверх
        PVT_correlations = float(self.lineEdit_15.text())  # 0  # [] Индикатор выбора корреляций (0 - Standing)
        (' PVT индикатор = 0\n'
         '           1) Z-фактор                                   - Dranchuk\n'
         '           2) Вязкость дегазированной и насыщенной нефти - Beggs Robinson\n'
         '           3) Вязкость газа                              - Lee    \n'
         '           4) Вязкость недонасыщенной нефти              - Vasquez - Beggs\n'
         '           5) Давление насыщения                         - Standing\n'
         '           6) Объемный коэффициент нефти                 - Standing\n'
         '           7) Сжимаемость нефти                          - Vasquez & Beggs\n'
         '           8) Газонасыщенность                           - Standing    \n'
         '            \n'
         '            PVT индикатор = 1\n'
         '           1) Z-фактор                                   - Brill and Beggs, Standing\n'
         '           2) Вязкость дегазированной нефти              - Standing\n'
         '           3) Вязкость насыщенной нефти                  - Beggs Robinson\n'
         '           4) Вязкость газа                              - Lee\n'
         '           5) Вязкость недонасыщенной нефти              - Vasquez - Beggs\n'
         '           6) Вязкость нефти при r_sb <350               - Standing\n'
         '           7) Давление насыщения                         - Valko - McCainSI\n'
         '           8) Объемный коэффициент нефти                 - McCainSI\n'
         '           9) Сжимаемость нефти                          - Vasquez & Beggs\n'
         '           10) Газонасыщенность                          - VelardeSI   \n'
         '        ')
        r_sb = float(self.lineEdit_17.text())  # 250 # [м3/м3] газонасыщенность при давлении насыщения,
        # если не задан лабораторный параметр, то принимаем Газовый фактор равный производственному 
        # (если r_sb < 0 - то = R_p)
        P_rb = float(self.lineEdit_18.text())  # 200  # [атм] Калибровочное давление насыщения
        # (расчёт по корреляции + корректировка на данное значение)
        b_ro = float(self.lineEdit_19.text())  # -1  # [м3/м3] Калибровочный объемный коэффициент нефти при давлении насыщения
        # (расчёт по корреляции + корректировка на данное значение)
        MaxSegmLen = float(self.lineEdit_22.text())  # -1  # [м] Длина расчётного участка
        # (если задаем, то параметр n_n не нужен, число узлов расчитывается)
        grad_Calc = float(self.lineEdit_23.text())  # 5
        ('\n'
         '        grad индикатор = 1\n'
         '            Методика Ansari\n'
         '            Режимы течения:    \n'
         '            \'1 - однофазный\'\n'
         '            \'2 - рассеяный пузырьковый поток\'\n'
         '            \'3 - пузырьковый (аэрированный)\'\n'
         '            \'4 - пробковый режим потока\'\n'
         '            \'5 - кольцевой режим потока\'\n'
         '        \n'
         '        grad индикатор = 2\n'
         '            Методика Beggs Brill    \n'
         '            \'1 -Расслоенный\' \n'
         '            \'2 - Перемежающийся\'\n'
         '            \'3 - Распределенный\'\n'
         '            \'4- Переходный\'\n'
         '        \n'
         '        grad индикатор = 3   \n'
         '            Эмпирическая корреляция В.А. Сахарова, М. А. Мохова, Воловодова \n'
         '            Режимы течения - None \n'
         '        \n'
         '        grad индикатор = 4   \n'
         '            Эмпирическая корреляция Gray \n'
         '            Режимы течения - None\n'
         '        \n'
         '        grad индикатор = 5   \n'
         '            Эмпирическая корреляция R. Hagedorn and  E. Brown (modified)\n'
         '            Режимы течения - None\n'
         '         \n'
         '        grad индикатор = 6\n'
         '            Эмпирическая корреляция В. Г. Грон \n'
         '            1 - пробковая; 2 - кольцевая\n'
         '        ')
        Payne_et_all_holdup = float(self.lineEdit_24.text())  # 0 # Если 0 - оригинальный Beggs Brill, иначе модификация Payne
        Payne_et_all_friction = float(self.lineEdit_25.text())  # 1 # Если 0 - оригинальный Beggs Brill, иначе модификация Payne
        slip = float(self.lineEdit_26.text())  # 1 # 1 - учитывать проскальзывание газа в Beggs Brill; 0 - не учитывать проскальзывание газа

        (' \n'
         '         Блок подбора оптимального режима работы газлифтной скважины\n'
         '         \n'
         '        1) Включите индикатор Opt_rejim = 1\n'
         '        2) Введите входные параметры pi; Pr\n'
         '        3) Продолжайте работу с другими блоками\n'
         '        \n'
         '        ')

        pi = float(self.lineEdit_20.text())  # 2.4 # [м3/атм/сут]
        Pr = float(self.lineEdit_21.text())  # 270 # [атм]

        KRD = pressuregradient()
        KRD.Pipe_pressure_drop(d, q_osc, teta, ksi, length, p_sn, t_sn, t_en, WCT, gamma_o, gamma_g, R_p, n_n, Salinity,
                               Calc_p_out, PVT_correlations, r_sb, P_rb, b_ro, MaxSegmLen, grad_Calc,
                               Payne_et_all_holdup, Payne_et_all_friction, slip)

        if self.checkBox.isChecked():  # Кривая распределения в НКТ
            'Построение кривой распределения давления в НКТ'
            if grad_Calc == 1:
                legen = 'Механистическая модель Ansari'
            if grad_Calc == 2:
                legen = 'Эмпирическая корреляция Beggs Brill'
            if grad_Calc == 3:
                legen = 'Эмпирическая корреляция В.А. Сахарова, М. А. Мохова, Воловодова'
            if grad_Calc == 4:
                legen = 'Эмпирическая корреляция Gray'
            if grad_Calc == 5:
                legen = 'Эмпирическая корреляция HagedornBrown'
            if grad_Calc == 6:
                legen = 'Эмпирическая корреляция В. Г. Грон'
            figure(1)
            plt.title('Кривая распределения давления в НКТ', color='black', family='fantasy', fontsize='x-large')
            plt.xlabel('Давление, атм', color='black', family='fantasy', fontsize='x-large')
            plt.ylabel('Глубина, м', color='black', family='fantasy', fontsize='x-large')
            plt.grid()
            ylim(length, 0)
            x = KRD.p_n
            y = KRD.md_n
            plt.plot(x, y, linewidth=3, label=legen)
            plt.legend(loc='best')
            plt.show()
            
        if self.checkBox_3.isChecked():  # Кривая распределения в НКТ    
            'Построение кривой распределения градиента давления в НКТ'
            if grad_Calc == 1:
                legen = 'Механистическая модель Ansari'
            if grad_Calc == 2:
                legen = 'Эмпирическая корреляция Beggs Brill'
            if grad_Calc == 3:
                legen = 'Эмпирическая корреляция В.А. Сахарова, М. А. Мохова, Воловодова'
            if grad_Calc == 4:
                legen = 'Эмпирическая корреляция Gray'
            if grad_Calc == 5:
                legen = 'Эмпирическая корреляция HagedornBrown'
            if grad_Calc == 6:
                legen = 'Эмпирическая корреляция В. Г. Грон'
            figure(2)
            plt.title('Кривая распределения градиента давления в НКТ', color='black', family='fantasy',
                      fontsize='x-large')
            plt.xlabel('Общий градиент давления, атм/м', color='black', family='fantasy', fontsize='x-large')
            plt.ylabel('Глубина, м', color='black', family='fantasy', fontsize='x-large')
            plt.grid()
            ylim(length, 0)
            x = KRD.gradP
            y = KRD.md_n
            plt.plot(x, y, 'g', linewidth=3, label=legen)
            plt.legend(loc='best')
            plt.show()

        if self.checkBox_2.isChecked():  #Кривая распределения температуры в НКТ
            figure(3)
            plt.title('Кривая распределения температуры в НКТ', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.xlabel('Температура, *С', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.ylabel('Глубина, м', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.grid()
            ylim(length,0)
            legen = 'Температура'
            x = KRD.t_n
            y = KRD.md_n
            plt.plot(x, y, 'r', linewidth=3, label = legen)
            plt.legend(loc = 'best')
            plt.show()
            
        if self.checkBox_4.isChecked():#Кривая распределения режимов течения в НКТ     
            if grad_Calc == 1:
                legen = 'Механистическая модель Ansari'
            if grad_Calc == 2:
                legen = 'Beggs Brill'
            if grad_Calc == 3:
                legen = 'Эмпирическая корреляция В.А. Сахарова, М. А. Мохова, Воловодова' 
            if grad_Calc == 4:
                legen = 'Эмпирическая корреляция Gray'
            if grad_Calc == 5:
                legen = 'Эмпирическая корреляция HagedornBrown'
            if grad_Calc == 6:
                legen = 'Эмпирическая корреляция В. Г. Грон'        
            figure(4)    
            plt.title('Кривая распределения режимов течения в НКТ', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.xlabel('индекс течения', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.ylabel('Глубина, м', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.grid()
            ylim(length,0)
            x = KRD.fp_m
            y = KRD.md_n
            xlim(0,6)    
            plt.plot(x, y,'ro-', linewidth=0, label = legen)
            plt.legend(loc = 'best')    
            plt.show()  
        
        if self.checkBox_5.isChecked(): #Давление насыщения (при изменении температуры от глубины)
            figure(5)    
            plt.title('Давление насыщения', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.xlabel('Температура, *С', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.ylabel('Давление насыщения, атм', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.grid()
            x = KRD.t_n
            y = KRD.p_b_m
            plt.plot(x, y, 'r', linewidth=3)
            plt.show()
        
        if self.checkBox_6.isChecked(): #Газосодержание
            figure(6)    
            plt.title('Газосодержание', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.xlabel('Давление, атм', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.ylabel('Газосодержание, м3/м3', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.grid()
            x = KRD.p_n
            y = KRD.r_s_m
            plt.plot(x, y, 'r', linewidth=3)
            plt.show()  
                
        if self.checkBox_7.isChecked(): #Объемный коэффициент нефти
            figure(7)    
            plt.title('Объемный коэффициент нефти', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.xlabel('Давление, атм', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.ylabel('Объемный коэффициент нефти, м3/м3', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.grid()
            x = KRD.p_n
            y = KRD.b_o_m
            plt.plot(x, y, 'black', linewidth=3)
            plt.show() 
        
        if self.checkBox_8.isChecked(): #Вязкость нефти
            figure(8)    
            plt.title('Вязкость нефти', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.xlabel('Давление, атм', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.ylabel('Вязкость нефти, сПз', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.grid()
            x = KRD.p_n
            y = KRD.mu_o_m
            plt.plot(x, y, 'black', linewidth=3)
            plt.show()        
        
        if self.checkBox_9.isChecked(): #Z - фактор
            figure(9)    
            plt.title('Z - фактор', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.xlabel('Давление, атм', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.ylabel('Z - фактор', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.grid()
            x = KRD.p_n
            y = KRD.z_m
            plt.plot(x, y, 'r', linewidth=3)
            plt.show()        
        
        if self.checkBox_10.isChecked(): #Объемный коэффициент газа
            figure(10)    
            plt.title('Объемный коэффициент газа', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.xlabel('Давление, атм', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.ylabel('Объемный коэффициент газа, м3/м3', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.grid()
            x = KRD.p_n
            y = KRD.b_g_m
            plt.plot(x, y, 'r', linewidth=3)
            plt.show()        
        
        if self.checkBox_11.isChecked(): #Вязкость газа
            figure(11)    
            plt.title('Вязкость газа', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.xlabel('Давление, атм', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.ylabel('Вязкость газа, сПз', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.grid()
            x = KRD.p_n
            y = KRD.mu_g_m
            plt.plot(x, y, 'r', linewidth=3)
            plt.show()           
        
        if self.checkBox_12.isChecked(): #Объемный коэффициент воды
            figure(12)    
            plt.title('Объемный коэффициент воды', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.xlabel('Давление, атм', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.ylabel('Объемный коэффициент воды, м3/м3', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.grid()
            x = KRD.p_n
            y = KRD.b_w_m
            plt.plot(x, y, 'b', linewidth=3)
            plt.show()      
        
        if self.checkBox_13.isChecked(): #Вязкость воды
            figure(13)    
            plt.title('Вязкость воды', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.xlabel('Давление, атм', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.ylabel('Вязкость воды, сПз', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.grid()
            x = KRD.p_n
            y = KRD.mu_w_m
            plt.plot(x, y, 'b', linewidth=3)
            plt.show() 

        if self.checkBox_14.isChecked(): #Индикаторная кривая
            '''Узловой анализ'''
            P_1 = 1
            P_2 = round(Pr)
            deltaP = 10
            VLP  = pressuregradient()
            VLP_m = []
            VLP_m2 = []
            P_mass = []
            IPR_m = []    
            IPR = Qvogel()    
            q_m = []    
            deltaR_p = 150    
            for P in range(P_1, P_2, deltaP):
        #       R_p_m.append(R_p_opt)    
                P_mass.append(P)
                IPR.Qvogel_wcF(P, Pr, KRD.p_b_m[len(KRD.p_b_m)-1], pi, WCT * 100)
                IPR_m.append(IPR.Qvogel_wc)
            for q in range (0, round(max(IPR_m)), round(max(IPR_m) * 0.05)):    
                VLP.Pipe_pressure_drop(d, q*(1-WCT), teta, ksi, length, p_sn, t_sn, t_en, WCT, gamma_o, gamma_g, R_p, n_n, Salinity, Calc_p_out, PVT_correlations, r_sb, P_rb, b_ro, MaxSegmLen, grad_Calc, Payne_et_all_holdup, Payne_et_all_friction, slip)
                VLP_m.append(VLP.p_n[len(VLP.p_n)-1])
                VLP.Pipe_pressure_drop(d, q*(1-WCT), teta, ksi, length, p_sn, t_sn, t_en, WCT, gamma_o, gamma_g, (R_p + deltaR_p), n_n, Salinity, Calc_p_out, PVT_correlations, r_sb, P_rb, b_ro, MaxSegmLen, grad_Calc, Payne_et_all_holdup, Payne_et_all_friction, slip)
                VLP_m2.append(VLP.p_n[len(VLP.p_n)-1])            
                q_m.append(q)
            y1 = P_mass
            x1 = IPR_m
            y2 = VLP_m
            x2 = q_m
            y3 = VLP_m2    
            fig, ax1 = plt.subplots()
            ax2 = ax1.twinx()
            ax1.plot(x1, y1, 'g-')
            legen_part1 = 'Газовый фактор = '
            legen_part2 = R_p
            legen_part3 = ' м3/м3'
            legen_part4 = (R_p + deltaR_p)    
            legen = legen_part1 + str(legen_part2) + legen_part3    
            legen2 = legen_part1 + str(legen_part4) + legen_part3    
            ax2.plot(x2, y2, 'b-', label = legen)
            ax2.plot(x2, y3, 'r-', label = legen2)
            ax1.set_xlabel('Дебит, м3/сут', family = 'fantasy', fontsize = 'x-large')
            ax1.set_ylabel('Давление (приток [ур. забой]), атм', color='g', family = 'fantasy', fontsize = 'x-large')
            ax2.set_ylabel('Давление (отток [ур. башмак]), атм', color='b', family = 'fantasy', fontsize = 'x-large')
            ax1.set_xlim(0, max(IPR_m))
            ax1.set_ylim(0, Pr)
            ax2.set_ylim(0, Pr)  
            plt.grid()
            plt.legend(loc = 'best')    
            plt.title('Узловой анализ', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.show()
         #   print(x2, y2)
         #   print("Рнас =", KRD.p_b_m[len(KRD.p_b_m)-1], "атм")
        
        if self.checkBox_15.isChecked(): #Оптимальный режим
            ''' Поиск оптимального режима'''
            R_p1 = 20
            R_p2 = 1300
            deltaRp = 10
            Rejim  = pressuregradient()
            R_p_m = []
            P_m = []
            Q_m = []    
            Qv = Qvogel()    
            conter = 0    
            Qnext = 1            
            for R_p_opt in range(R_p1, R_p2, deltaRp): 
                conter = conter + 1        
                Rejim.Pipe_pressure_drop(d, Qnext*(1 - WCT), teta, ksi, length, p_sn, t_sn, t_en, WCT, gamma_o, gamma_g, R_p_opt, n_n, Salinity, Calc_p_out, PVT_correlations, r_sb, P_rb, b_ro, MaxSegmLen, grad_Calc, Payne_et_all_holdup, Payne_et_all_friction, slip)
                R_p_m.append(R_p_opt)    
                P_m.append(Rejim.p_n[len(Rejim.p_n)-1])
                Qv.Qvogel_wcF(P_m[conter-1], Pr, Rejim.p_b_m[len(Rejim.p_b_m)-1], pi, WCT * 100)
                Q_m.append(Qv.Qvogel_wc)
                Qnext = Qv.Qvogel_wc
            x = R_p_m
            y1 = P_m
            y2 = Q_m    
            fig, ax1 = plt.subplots()
            ax2 = ax1.twinx()
            ax1.plot(x, y1, 'g-')
            ax2.plot(x, y2, 'b-')
            ax1.set_xlabel('Удельный расход газа, м3/м3', family = 'fantasy', fontsize = 'x-large')
            ax1.set_ylabel('Давление, атм', color='g', family = 'fantasy', fontsize = 'x-large')
            ax2.set_ylabel('Дебит жидкости, м3/сут', color='b', family = 'fantasy', fontsize = 'x-large')
            xlim(0, R_p2)    
            ylim(0, max(Q_m))
            plt.grid()
            plt.title('Оптимальный режим работы', color = 'black', family = 'fantasy', fontsize = 'x-large')
            plt.show()
            label = round(max(Q_m))    
            #print (label)

    def opt_valve(self):
        d = float(self.lineEdit.text())  # Пример: 0.062   # [м] Внутренний диаметр трубы
        q_osc = float(self.lineEdit_2.text())  # Пример: 100    # [м3/сут] Дебит нефти в стандартных условиях
        teta = float(self.lineEdit_3.text())  # Пример: 90      # [градусы] Угол наклона трубы
        ksi = float(self.lineEdit_4.text())  # 0.0002 # [м] Шероховатость стенки
        length = float(self.lineEdit_16.text())  # Пример: 2400   # [м] Длина трубы
        p_sn = float(self.lineEdit_5.text())  # 28 # [атм] Давление на начальном узле
        t_sn = float(self.lineEdit_6.text())  # 30 # [*C] Температура начального узла
        t_en = float(self.lineEdit_7.text())  # 40 # [*C] Температура конечного узла
        WCT = float(self.lineEdit_8.text())  # 0.08 # [доли ед.] Обводненность в стандартных условиях
        gamma_o = float(self.lineEdit_9.text())  # 0.836# [доли ед.] Удельная плотность нефти по воде
        gamma_g = float(self.lineEdit_10.text())  # 0.8 # [доли ед.] Удельная плотность газа по воздуху
        R_p = float(self.lineEdit_11.text())  # 400 # [м3/м3] Отношение добываемого газа к жидкости
        n_n = float(self.lineEdit_12.text())  # 10 # [-] число расчётных узлов
        Salinity = float(self.lineEdit_13.text())  # -1 # [мг/л] Солёность воды. Если не задана солёность воды,
        # то принимаем опциональное значение = 50000 мг/л.
        Calc_p_out = float(self.lineEdit_14.text())  # 0  # [-] Если индикатор = 0, то рассчитываем поток сверху вниз,
        # иначе снизу вверх
        PVT_correlations = float(self.lineEdit_15.text())  # 0  # [] Индикатор выбора корреляций (0 - Standing)
        (' PVT индикатор = 0\n'
         '           1) Z-фактор                                   - Dranchuk\n'
         '           2) Вязкость дегазированной и насыщенной нефти - Beggs Robinson\n'
         '           3) Вязкость газа                              - Lee    \n'
         '           4) Вязкость недонасыщенной нефти              - Vasquez - Beggs\n'
         '           5) Давление насыщения                         - Standing\n'
         '           6) Объемный коэффициент нефти                 - Standing\n'
         '           7) Сжимаемость нефти                          - Vasquez & Beggs\n'
         '           8) Газонасыщенность                           - Standing    \n'
         '            \n'
         '            PVT индикатор = 1\n'
         '           1) Z-фактор                                   - Brill and Beggs, Standing\n'
         '           2) Вязкость дегазированной нефти              - Standing\n'
         '           3) Вязкость насыщенной нефти                  - Beggs Robinson\n'
         '           4) Вязкость газа                              - Lee\n'
         '           5) Вязкость недонасыщенной нефти              - Vasquez - Beggs\n'
         '           6) Вязкость нефти при r_sb <350               - Standing\n'
         '           7) Давление насыщения                         - Valko - McCainSI\n'
         '           8) Объемный коэффициент нефти                 - McCainSI\n'
         '           9) Сжимаемость нефти                          - Vasquez & Beggs\n'
         '           10) Газонасыщенность                          - VelardeSI   \n'
         '        ')
        r_sb = float(self.lineEdit_17.text())  # 250 # [м3/м3] газонасыщенность при давлении насыщения,
        # если не задан лабораторный параметр, то принимаем Газовый фактор равный производственному 
        # (если r_sb < 0 - то = R_p)
        P_rb = float(self.lineEdit_18.text())  # 200  # [атм] Калибровочное давление насыщения
        # (расчёт по корреляции + корректировка на данное значение)
        b_ro = float(self.lineEdit_19.text())  # -1  # [м3/м3] Калибровочный объемный коэффициент нефти при давлении насыщения
        # (расчёт по корреляции + корректировка на данное значение)
        MaxSegmLen = float(self.lineEdit_22.text())  # -1  # [м] Длина расчётного участка
        # (если задаем, то параметр n_n не нужен, число узлов расчитывается)
        grad_Calc = float(self.lineEdit_23.text())  # 5
        ('\n'
         '        grad индикатор = 1\n'
         '            Методика Ansari\n'
         '            Режимы течения:    \n'
         '            \'1 - однофазный\'\n'
         '            \'2 - рассеяный пузырьковый поток\'\n'
         '            \'3 - пузырьковый (аэрированный)\'\n'
         '            \'4 - пробковый режим потока\'\n'
         '            \'5 - кольцевой режим потока\'\n'
         '        \n'
         '        grad индикатор = 2\n'
         '            Методика Beggs Brill    \n'
         '            \'1 -Расслоенный\' \n'
         '            \'2 - Перемежающийся\'\n'
         '            \'3 - Распределенный\'\n'
         '            \'4- Переходный\'\n'
         '        \n'
         '        grad индикатор = 3   \n'
         '            Эмпирическая корреляция В.А. Сахарова, М. А. Мохова, Воловодова \n'
         '            Режимы течения - None \n'
         '        \n'
         '        grad индикатор = 4   \n'
         '            Эмпирическая корреляция Gray \n'
         '            Режимы течения - None\n'
         '        \n'
         '        grad индикатор = 5   \n'
         '            Эмпирическая корреляция R. Hagedorn and  E. Brown (modified)\n'
         '            Режимы течения - None\n'
         '         \n'
         '        grad индикатор = 6\n'
         '            Эмпирическая корреляция В. Г. Грон \n'
         '            1 - пробковая; 2 - кольцевая\n'
         '        ')
        Payne_et_all_holdup = float(self.lineEdit_24.text())  # 0 # Если 0 - оригинальный Beggs Brill, иначе модификация Payne
        Payne_et_all_friction = float(self.lineEdit_25.text())  # 1 # Если 0 - оригинальный Beggs Brill, иначе модификация Payne
        slip = float(self.lineEdit_26.text())  # 1 # 1 - учитывать проскальзывание газа в Beggs Brill; 0 - не учитывать проскальзывание газа

        (' \n'
         '         Блок подбора оптимального режима работы газлифтной скважины\n'
         '         \n'
         '        1) Включите индикатор Opt_rejim = 1\n'
         '        2) Введите входные параметры pi; Pr\n'
         '        3) Продолжайте работу с другими блоками\n'
         '        \n'
         '        ')

        pi = float(self.lineEdit_20.text())  # 2.4 # [м3/атм/сут]
        Pr = float(self.lineEdit_21.text())  # 270 # [атм]        
        
        

        p_sn_2 = float(self.lineEdit_27.text())   #110 #'целевое забойное давление' #!!!!!!!!!!!!!!!!!!!!!!!!!
        t_sn_2 = float(self.lineEdit_7.text())
        t_en_2 = float(self.lineEdit_6.text())
        
        Calc_p_out_2 = 1
        
        rho_air = 1.2217 # плотность воздуха при стандартных условиях
        Hst = float(self.lineEdit_28.text())  # -1
        Hw = length
        R_p_2 = float(self.lineEdit_29.text()) #100!!!!!!!!!!!!!!!!!!!!!!!!!
        P_c = float(self.lineEdit_30.text()) #75
        t_norm = 293
        
        Pz = float(self.lineEdit_31.text())
        D_c = float(self.lineEdit_31.text()) # 0.146
        delta_p_gv = float(self.lineEdit_33.text()) #3
        delta_p_gv_2 = delta_p_gv + 1
        delta_p_gv_3 = delta_p_gv_2 + 1
        delta_p_gv_4 = delta_p_gv_3 + 1
        n_pk = float(self.lineEdit_34.text()) # количество пусковых клапанов (минимум 1; максимум 9)
        '!!!!!!!!'
        
        
        'Значение плотности для расчёта плотности нефти из входного параметра удельная плотность нефти [усл. плот. воды при стандартных условиях]'
        rho_ref = 1000
        
        """ Вызываем блок PVT данных"""
        PVT_gasvalve = Calc_PVT(gamma_o, gamma_g, PVT_correlations, r_sb, P_rb, b_ro, Salinity, gamma_w) 
        PVT_gasvalve.CalcPVT(Pr, t_en, gamma_o, gamma_g, PVT_correlations, r_sb, P_rb, b_ro, Salinity, gamma_w) 
        """Выходными параметрами являются: PVT.p_b [МПа]; PVT.r_s [м3/м3]; PVT.b_o [д. ед.]; PVT.mu_o [сПз]; PVT.z; PVT.b_g; PVT.mu_g; PVT.b_w [д. ед.]; PVT.mu_w [сПз]"""                
        
        rho_gsc = gamma_g * rho_air
        rho_w_sc = WaterDensitySC(Salinity, gamma_w) * rho_ref
        rho_osc = gamma_o * rho_ref
        rho_w = rho_w_sc / PVT_gasvalve.b_w
        rho_o = (rho_osc + PVT_gasvalve.r_s * rho_gsc) / PVT_gasvalve.b_o
        rho_wm = (rho_w_sc + rho_w) / 2
        rho_om = (rho_osc + rho_o) / 2
        rho_lm = rho_om * (1 - WCT) + rho_wm * WCT  # не учитывается изменение объемной доли воды
        
        if Hst == -1:
            Hst = Hw - 10**5 * Pr / (rho_lm * g)
            
        
        'Строим распределение давления в скважине, в остановленной - равно гидростату'
        'линия 1'
        H_1_mas = []
        P_1_mas = []
        for H_1 in range(0, round(Hw), 50): 
            P_1 = (H_1 - Hst) * (Pr - Pz) / (Hw - Hst) + Pz 
            if P_1 <= Pz:
                P_1 = Pz
            H_1_mas.append(H_1)
            P_1_mas.append(P_1)
        'линия 2'
        KRD_2 = pressuregradient()
        KRD_2.Pipe_pressure_drop(D_c, q_osc, teta, ksi, length, p_sn_2, t_sn_2, t_en_2, WCT, gamma_o, gamma_g, R_p_2, n_n, Salinity,
                                 Calc_p_out_2, PVT_correlations, r_sb, P_rb, b_ro, MaxSegmLen, grad_Calc,
                                 Payne_et_all_holdup, Payne_et_all_friction, slip) # КРД считает для НКТ спущенного до забоя
        'линия 3'
        KRD_3 = pressuregradient()
        KRD_3.Pipe_pressure_drop(d, q_osc, teta, ksi, length, p_sn, t_sn, t_en, WCT, gamma_o, gamma_g, R_p, n_n, Salinity,
                                 Calc_p_out, PVT_correlations, r_sb, P_rb, b_ro, MaxSegmLen, grad_Calc,
                                 Payne_et_all_holdup, Payne_et_all_friction, slip) # КРД считает для НКТ спущенного до забоя
        
        delta_t_4 = (t_en - t_sn) / (length/n_n)
        t_4 = t_sn
        P_4 = P_c
        H_4_mas = []
        P_4_mas = []
        t_4_mas = []
        for H_4 in range(0, round(length), round(n_n)): 
             t_4 = t_4 + 0.5 * delta_t_4
             if H_4 == 0:
                 t_4 = t_sn
             PVT_4 = Calc_PVT(gamma_o, gamma_g, PVT_correlations, r_sb, P_rb, b_ro, Salinity, gamma_w) 
             PVT_4.CalcPVT(P_4, t_4, gamma_o, gamma_g, PVT_correlations, r_sb, P_rb, b_ro, Salinity, gamma_w)    
             P_4 = P_c * exp(gamma_g * rho_air * g * H_4 * t_norm/(10**5*(t_4+273)*PVT_4.z)) # - P_c
             P_4_mas.append(P_4)
             H_4_mas.append(H_4)
             t_4_mas.append(t_4)
        
        
        '''Превышение уровня жидкости при закачке газа над статическим'''
        delta_Hst = 10**5*(P_c - p_sn)/(rho_lm * g)
        H_A = Hst - delta_Hst
        if delta_Hst < Hst:
            H_5_mas = []
            P_5_mas = []
            H_Ab =Hst + (p_sn-Pz)*(Hw-Hst)/(Pr-Pz)
            delta_H = H_Ab - H_A
            for P_5 in range(round(p_sn), round(P_c + 20), 1):
                H_5 = (P_5 - Pz) * (Hw - Hst) / (Pr - Pz) - delta_H + Hst         
                H_5_mas.append(H_5)
                P_5_mas.append(P_5)
        
        
        'кривая ||4, клапан 1'
        delta_t_6 = (t_en - t_sn) / (length/n_n)
        t_6 = t_sn
        P_6 = P_c - delta_p_gv
        H_6_mas = []
        P_6_mas = []
        for H_6 in range(0, round(length), round(n_n)): 
            t_6 = t_6 + 0.5 * delta_t_6
            if H_6 == 0:
                t_6 = t_sn
            PVT_6 = Calc_PVT(gamma_o, gamma_g, PVT_correlations, r_sb, P_rb, b_ro, Salinity, gamma_w) 
            PVT_6.CalcPVT(P_6, t_6, gamma_o, gamma_g, PVT_correlations, r_sb, P_rb, b_ro, Salinity, gamma_w)    
            P_6 = P_c * exp(gamma_g * rho_air * g * H_6 * t_norm/(10**5*(t_6+273)*PVT_6.z)) - delta_p_gv
            P_6_mas.append(P_6)
            H_6_mas.append(H_6)
             
        
        plt.title('Расстановка газлифтных клапанов', color='black', family='fantasy', fontsize='x-large')
        plt.xlabel('Давление, атм', color='black', family='fantasy', fontsize='x-large')
        plt.ylabel('Глубина, м', color='black', family='fantasy', fontsize='x-large')
        plt.grid()
        ylim(length, 0)
        xlim(0, Pr)
        x1 = P_1_mas
        y1 = H_1_mas
        x2 = KRD_2.p_n
        KRD_2.md_n.reverse()
        y2 = KRD_2.md_n
        x3 = KRD_3.p_n
        y3 = KRD_3.md_n
        x4 = P_4_mas
        y4 = H_4_mas
        x5 = p_sn
        y5 = H_A
        x6 = P_5_mas
        y6 = H_5_mas
        x7 = P_6_mas
        y7 = H_6_mas
        
        legen1 = 'Гидростатическое распределение'
        legen2 = 'КРД (снизу - вверх) при пластовом Гф'
        legen3 = 'КРД (сверху - вниз) при оптимальном уд. расх. газа'
        legen4 = 'КРД газа (барометрическая формула)'
        legen5 = 'точка А (разность Нст и разностей уровней (Pзак и Ру))'
        legen6 = 'Линия параллельная гидростату'
        legen7 = 'Учёт перепада давления в клапане'
        legen8 = 'Глубина установки пускового клапана'
        legen9 = 'Глубина установки рабочего клапана'
        
        y_r = 0
        x_r = p_sn
        P_9_mas = []
        H_9_mas = []
        for ii in range (0,len(y2)-1, 1):
            for ij in range (0,len(y3)-1, 1):
                if y_r<=y2[ii]<y3[ij] and x_r<x2[ii]<x3[ij]:
                    H_rk = (y2[ii] -y2[ii+1])/2 + y2[ii+1]  # Продумать более грамотный подход нахождения точки пересечения
                    #H_rk = y2[ii]
                    P_rk = (x2[ii] -x2[ii+1])/2 + x2[ii+1]
                    #P_rk = x2[ii]
                y_r = y3[ij]
                x_r = x3[ij]
        for P_9 in range (round(p_sn), round(P_rk) + 1, 1):
                    P_9_mas.append(P_9)
                    H_9_mas.append(H_rk)
        
        x9 = P_9_mas
        y9 = H_9_mas
        
        y_r = 0
        x_r = P_c - delta_p_gv
        P_7_mas = []
        H_7_mas = []
        for ii in range (0,len(y6)-1, 1):
            for ij in range (0,len(y7)-1, 1):
                if y_r<=y6[ii]<y7[ij] and x_r<x6[ii]<x7[ij]:
                    H_gv_1 = y6[ii-1] + (-y6[ii-1] + y6[ii])/2
                    P_gv_1 = x6[ii]
                y_r = y7[ij]
                x_r = x7[ij]
        for P_7 in range (round(p_sn), round(P_gv_1) + 1, 1):
                    P_7_mas.append(P_7)
                    H_7_mas.append(H_gv_1)
        
        x8 = P_7_mas
        y8 = H_7_mas
        'Блок расчётов пусковых клапанов 2 - 9'
        x_1_2 = []; y_1_2 = []
        x_1_3 = []; y_1_3 = []
        x_1_4 = []; y_1_4 = []
        x_1_5 = []; y_1_5 = []
        x_1_6 = []; y_1_6 = []
        x_1_7 = []; y_1_7 = []
        x_1_8 = []; y_1_8 = []
        x_1_9 = []; y_1_9 = []
        x_2_2 = []; y_2_2 = []
        x_2_3 = []; y_2_3 = []
        x_2_4 = []; y_2_4 = []
        x_2_5 = []; y_2_5 = []
        x_2_6 = []; y_2_6 = []
        x_2_7 = []; y_2_7 = []
        x_2_8 = []; y_2_8 = []
        x_2_9 = []; y_2_9 = []
        x_3_2 = []; y_3_2 = []
        x_3_3 = []; y_3_3 = []
        x_3_4 = []; y_3_4 = []
        x_3_5 = []; y_3_5 = []
        x_3_6 = []; y_3_6 = []
        x_3_7 = []; y_3_7 = []
        x_3_8 = []; y_3_8 = []
        x_3_9 = []; y_3_9 = []
        x_4_2 = []; y_4_2 = []
        x_4_3 = []; y_4_3 = []
        x_4_4 = []; y_4_4 = []
        x_4_5 = []; y_4_5 = []
        x_4_6 = []; y_4_6 = []
        x_4_7 = []; y_4_7 = []
        x_4_8 = []; y_4_8 = []
        x_4_9 = []; y_4_9 = []
        PVT_pk = Calc_PVT(gamma_o, gamma_g, PVT_correlations, r_sb, P_rb, b_ro, Salinity, gamma_w)
        KRD_pk = pressuregradient()
        for i in range (1, round(n_pk), 1):
            'нахождение точeк A:'
            KRD_pk = pressuregradient()
            KRD_pk.Pipe_pressure_drop(d, q_osc, teta, ksi, H_gv_1, p_sn, t_sn, t_en, WCT, gamma_o, gamma_g, R_p, n_n, Salinity,
                                        Calc_p_out, PVT_correlations, r_sb, P_rb, b_ro, MaxSegmLen, grad_Calc,
                                        Payne_et_all_holdup, Payne_et_all_friction, slip)    
            P_A = KRD_pk.p_n[len(KRD_pk.p_n)-1]
            x = P_A
            y = H_gv_1
            if i == 1:
                x_1_2 = x
                y_1_2 = y
            elif i == 2:
                x_1_3 = x
                y_1_3 = y        
            elif i == 3:
                x_1_4 = x
                y_1_4 = y    
            elif i == 4:
                x_1_5 = x
                y_1_5 = y    
            elif i == 5:
                x_1_6 = x
                y_1_6 = y      
            elif i == 6:
                x_1_7 = x
                y_1_7 = y    
            elif i == 7:
                x_1_8 = x
                y_1_8 = y    
            elif i == 8:
                x_1_9 = x
                y_1_9 = y    
            '|| линия - 1, через A'
            H_A_pk_mas = []
            P_A_pk_mas = []
            H_Ab_pk =Hst + (P_A -Pz)*(Hw-Hst)/(Pr-Pz)
            delta_H = H_Ab_pk - H_gv_1
            for P_A_i in range(round(p_sn), round(P_c + 20), 1): 
                H_A_pk = (P_A_i - Pz) * (Hw - Hst) / (Pr - Pz) - delta_H + Hst         
                H_A_pk_mas.append(H_A_pk)
                P_A_pk_mas.append(P_A_i)
            if i == 1:
                x_2_2 = P_A_pk_mas
                y_2_2 = H_A_pk_mas
            elif i == 2:
                x_2_3 = P_A_pk_mas
                y_2_3 = H_A_pk_mas    
            elif i == 3:
                x_2_4 = P_A_pk_mas
                y_2_4 = H_A_pk_mas    
            elif i == 4:
                x_2_5 = P_A_pk_mas
                y_2_5 = H_A_pk_mas    
            elif i == 5:
                x_2_6 = P_A_pk_mas
                y_2_6 = H_A_pk_mas     
            elif i == 6:
                x_2_7 = P_A_pk_mas
                y_2_7 = H_A_pk_mas     
            elif i == 7:
                x_2_8 = P_A_pk_mas
                y_2_8 = H_A_pk_mas     
            elif i == 8:
                x_2_9 = P_A_pk_mas
                y_2_9 = H_A_pk_mas    
            'Барометрическая формула + перепад на клапан'
            delta_t_pk = (t_en - t_sn) / (length/n_n)
            t_pk = t_sn
            if i == 1:
                delta_p_pk = delta_p_gv + 1
            elif i == 2:
                delta_p_pk = delta_p_gv + 2
            elif i == 3:
                delta_p_pk = delta_p_gv + 3    
            elif i == 4:
                delta_p_pk = delta_p_gv + 4    
            elif i == 5:
                delta_p_pk = delta_p_gv + 5    
            elif i == 6:
                delta_p_pk = delta_p_gv + 6     
            elif i == 7:
                delta_p_pk = delta_p_gv + 7     
            elif i == 8:
                delta_p_pk = delta_p_gv + 8     
            P_pk = P_c - delta_p_pk
            H_pk_mas = []
            P_pk_mas = []
            for H_pk in range(0, round(length), round(n_n)):
                t_pk = t_pk + 0.5 * delta_t_pk
                if H_pk == 0:
                    t_pk = t_sn
                PVT_pk.CalcPVT(P_pk, t_pk, gamma_o, gamma_g, PVT_correlations, r_sb, P_rb, b_ro, Salinity, gamma_w)    
                P_pk = P_c * exp(gamma_g * rho_air * g * H_pk * t_norm/(10**5*(t_pk+273)*PVT_pk.z)) - delta_p_pk
                P_pk_mas.append(P_pk)
                H_pk_mas.append(H_pk)
            if i == 1:
                x_3_2 = P_pk_mas
                y_3_2 = H_pk_mas
            elif i == 2:
                x_3_3 = P_pk_mas
                y_3_3 = H_pk_mas
            elif i == 3:
                x_3_4 = P_pk_mas
                y_3_4 = H_pk_mas        
            elif i == 4:
                x_3_5 = P_pk_mas
                y_3_5 = H_pk_mas        
            elif i == 5:
                x_3_6 = P_pk_mas
                y_3_6 = H_pk_mas        
            elif i == 6:
                x_3_7 = P_pk_mas
                y_3_7 = H_pk_mas         
            elif i == 7:
                x_3_8 = P_pk_mas
                y_3_8 = H_pk_mas        
            elif i == 8:
                x_3_9 = P_pk_mas
                y_3_9 = H_pk_mas        
            'Поиск глубины спуска пускового клапана - i'
            y_r = 0
            x_r = P_c - delta_p_pk
            P_pk_mas = []
            H_pk_mas = []
            if i == 1:
                x_i_1 = x_2_2
                y_i_1 = y_2_2
                x_i_2 = x_3_2
                y_i_2 = y_3_2
            elif i == 2:
                x_i_1 = x_2_3
                y_i_1 = y_2_3    
                x_i_2 = x_3_3
                y_i_2 = y_3_3
            elif i == 3:
                x_i_1 = x_2_4
                y_i_1 = y_2_4    
                x_i_2 = x_3_4
                y_i_2 = y_3_4    
            elif i == 4:
                x_i_1 = x_2_5
                y_i_1 = y_2_5    
                x_i_2 = x_3_5
                y_i_2 = y_3_5
            elif i == 5:
                x_i_1 = x_2_6
                y_i_1 = y_2_6    
                x_i_2 = x_3_6
                y_i_2 = y_3_6        
            elif i == 6:
                x_i_1 = x_2_7
                y_i_1 = y_2_7    
                x_i_2 = x_3_7
                y_i_2 = y_3_7         
            elif i == 7:
                x_i_1 = x_2_8
                y_i_1 = y_2_8    
                x_i_2 = x_3_8
                y_i_2 = y_3_8 
            elif i == 8:
                x_i_1 = x_2_9
                y_i_1 = y_2_9    
                x_i_2 = x_3_9
                y_i_2 = y_3_9         
            for ii in range (0,len(y_i_1)-1, 1):
                for ij in range (0,len(y_i_2)-1, 1):
                    if y_r<=y_i_1[ii]<y_i_2[ij] and x_r<x_i_1[ii]<x_i_2[ij]:
                        H_gv_1 = y_i_1[ii-1] + (-y_i_1[ii-1] + y_i_1[ii])/2
                        P_gv_1 = x_i_1[ii]
                    y_r = y_i_2[ij]
                    x_r = x_i_2[ij]
            for P_pk in range (round(p_sn), round(P_gv_1) + 1, 1):
                        P_pk_mas.append(P_pk)
                        H_pk_mas.append(H_gv_1)
            if i == 1:
                x_4_2 = P_pk_mas
                y_4_2 = H_pk_mas
            elif i == 2:
                x_4_3 = P_pk_mas
                y_4_3 = H_pk_mas
            elif i == 3:
                x_4_4 = P_pk_mas
                y_4_4 = H_pk_mas
            elif i == 4:
                x_4_5 = P_pk_mas
                y_4_5 = H_pk_mas  
            elif i == 5:
                x_4_6 = P_pk_mas
                y_4_6 = H_pk_mas   
            elif i == 6:
                x_4_7 = P_pk_mas
                y_4_7 = H_pk_mas  
            elif i == 7:
                x_4_8 = P_pk_mas
                y_4_8 = H_pk_mas  
            elif i == 8:
                x_4_9 = P_pk_mas
                y_4_9 = H_pk_mas  
        plt.plot(x1, y1,'go-',
                 x2, y2,'b*-',
                 x3, y3,'r*-',
                 x4, y4, 'r^',
                 x5, y5, 'ro',
                 x6, y6,'g--',
                 x7, y7, 'r--',
                 x8, y8, 'k-',
                 x9, y9, 'r-',
                 x_1_2, y_1_2, 'bo',
                 x_1_3, y_1_3, 'bo',
                 x_1_4, y_1_4, 'bo',
                 x_1_5, y_1_5, 'bo',
                 x_1_6, y_1_6, 'bo',
                 x_1_7, y_1_7, 'bo',
                 x_1_8, y_1_8, 'bo',
                 x_1_9, y_1_9, 'bo',
                 x_2_2, y_2_2, 'g--',
                 x_2_3, y_2_3, 'g--',
                 x_2_4, y_2_4, 'g--',
                 x_2_5, y_2_5, 'g--',
                 x_2_6, y_2_6, 'g--',
                 x_2_7, y_2_7, 'g--',
                 x_2_8, y_2_8, 'g--',
                 x_2_9, y_2_9, 'g--',
                 x_3_2, y_3_2, 'r--',
                 x_3_3, y_3_3, 'r--',
                 x_3_4, y_3_4, 'r--',
                 x_3_5, y_3_5, 'r--',
                 x_3_6, y_3_6, 'r--',
                 x_3_7, y_3_7, 'r--',
                 x_3_8, y_3_8, 'r--',
                 x_3_9, y_3_9, 'r--',
                 x_4_2, y_4_2, 'k-',
                 x_4_3, y_4_3, 'k-',
                 x_4_4, y_4_4, 'k-',
                 x_4_5, y_4_5, 'k-',
                 x_4_6, y_4_6, 'k-',
                 x_4_7, y_4_7, 'k-',
                 x_4_8, y_4_8, 'k-',
                 x_4_9, y_4_9, 'k-')
        plt.legend([legen1,
                    legen2,
                    legen3,
                    legen4,
                    legen5,
                    legen6,
                    legen7,
                    legen8,
                    legen9],
                    loc='best')
        plt.show()
        
if __name__ == "__main__":
    import sys

    app = QtGui.QApplication(sys.argv)
    window = MyWindow()
    window.show()
    sys.exit(app.exec_())
