# -*- coding: utf-8 -*-
"""
Created on Mon Sep 14 13:02:59 2015

@author: Гость

Входной модуль проектирования скважины

Изменение параметров и алгоритмов осуществлять прямо в сценарном редакторе
"""
import Mode_of_working, PVT, Pipe, Output, Reservoir, Data

Data = Data.Data()
Mode = Mode_of_working.Mode_of_working(Data)
                                   
PVT_object = PVT.PVT(Data)
Pipe_Tube = Pipe.Pipe(Data)                 
Reservoir_object = Reservoir.Reservoir(Data)                
Out = Output.Output()
Pipe_Tube.Pipe_pressure_drop(Data.N_n, Data.Iter, PVT_object, Mode, Out)
Reservoir_object.Calc_IPR(Mode, PVT_object, Out)
Pipe_Tube.Calc_VLP(Data.N_n, Data.Iter, PVT_object, Mode, Out, Data)



Out.plot(5, 'Кривая распределения давления в НКТ', \
'Давление (отток [ур. башмак]), атм',
'Глубина, м',  Data.length_m, 0, min(Out.p_atm_array), max(Out.p_atm_array), \
Out.p_atm_array, Out.md_m_array, 'b', 'Beggs Brill') 

Out.plot(4, 'Узловой анализ', 'Дебит, м3/сут', \
'Давление (приток [ур. забой]), атм',  min(Out.P_test_atm_array),\
max(Out.P_test_atm_array), min(Out.IPR_array), max(Out.IPR_array),\
Out.IPR_array, Out.P_test_atm_array, 'b', 'Вогель') 

Out.plot(4, 'Узловой анализ', 'Дебит, м3/сут', \
'Давление (отток [ур. башмак]), атм',  min(Out.P_test_atm_array), \
max(Out.P_test_atm_array), min(Out.VLP_array), max(Out.VLP_array), \
Out.VLP_array, Out.P_VLP_atm_array, 'r', 'VLP') 




#'Для построения карты режимов потока по методике Беггза и Брилла'
#Out.plot_flow_map_Beggs_Brill(1)
#
#Out.plot(1, 'Параметры оценки режима потока', 'Входное содержание жидкости', \
#'Число Фруда',  0.1, \
#1000, 0.0001, 1, \
#Out.lambda_l_d_array, Out.N_Fr_d_array, 'y.', 'По стволу')


#x = [1,2,3,4,5]
#y = [5,4,3,2,1]
#z = [1,2,3,4,5]
#Out.plot_3d(1, x, y, z)



#Out.plot_flow_map_Beggs_Brill_3d(2)

