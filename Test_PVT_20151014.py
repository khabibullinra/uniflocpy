# -*- coding: utf-8 -*-
"""
Модуль тест PVT
"""
import Output, PVT

class Данные:
    def __init__(self):
        Температура_пласта_С = 100
        ''' Физико - химические свойства скважинной продукции '''
        Удельная_плотность_нефти = 0.7
        Удельная_плотность_газа = 0.9
        Удельная_плотность_воды = 1.01
        Газовый_фактор_нефти_пластовые_условия_м3_м3 = 100
        Давление_насыщение_экспериметальное_атм = 100
        Объемный_коэффициент_нефти_экспериментальный_м3_м3 = 1.4 # -1: отсутствует
        Солёность_мг_л = 0 # по умолчанию 50000 мг/л
        Коэффициент_сжимаемости_нефти_1_МПа = None # None - расчёт по корреляции        
        
        ''' PVT - эмпирические корреляции '''
        # Выбор: 1) "Standing"; 2) "Unknown"        
        Псевдокритические_PT = "Standing" 
        
        # Выбор: 1) "Dranchuk"; 2) "Beggs_Brill_Standing"
        Z_фактор = "Dranchuk" 
        
        # Выбор: 1) "Beggs_Robinson" - Завышает; 2) "Standing"
        Вязкость_дегазированной_нефти = "Standing"

        # Выбор: 1) "Standing"; 2) "Valko_McCainSI"
        Давление_насыщения = "Standing"
    
        # Выбор: 1) "Standing"; 2) "McCainSI"
        Объемный_коэффициент_нефти = "Standing"

        # Выбор: 1) "Standing"; 2) "VelardeSI"
        Газосодержание = "VelardeSI"
        
        
        ''' ДАЛЕЕ КОМАНДЫ НЕ ИЗМЕНЯТЬ'''
        'PVT - свойства продукции'
        self.gamma_o_d = Удельная_плотность_нефти
        self.gamma_g_d = Удельная_плотность_газа
        self.gamma_w_d = Удельная_плотность_воды
        self.r_sb_m3m3 = Газовый_фактор_нефти_пластовые_условия_м3_м3
        self.P_rb_atm = Давление_насыщение_экспериметальное_атм
        self.b_ro_m3m3 = Объемный_коэффициент_нефти_экспериментальный_м3_м3 
        self.s_mgl = Солёность_мг_л
        '''
        1) Псевдокритические PT:
            1.1) Standing
            1.2) Unknown
        
        2) Z - фактор:
            2.1) Dranchuk
            2.2) Beggs_Brill_Standing
        
        3) Вязкость дегазированной нефти:
            3.1) Beggs_Robinson
            3.2) Standing
        
        4) Давление насыщения:
            4.1) Standing
            4.2) Valko_McCainSI
        
        5) Объемный коэффициент нефти:
            5.1) Standing
            5.2) McCainSI
        
        6) Газосодержание:
            6.1) Standing
            6.2) VelardeSI
        '''
        self.PVT_correlations = Псевдокритические_PT, Z_фактор, \
        Вязкость_дегазированной_нефти, \
        Давление_насыщения, Объемный_коэффициент_нефти, Газосодержание
        self.C_o_1MPa = Коэффициент_сжимаемости_нефти_1_МПа
        self.T_res_C = Температура_пласта_С
        
''' СОЗДАНИЕ ОБЪЕКТОВ ''' 
Data = Данные()   
PVT_object = PVT.PVT(Data)  
Out = Output.Output()
''' РАБОЧИЙ АЛГОРИТМ '''

for T in range (-30, 41, 5):
    for P in range (1, 100, 1):
        PVT_object.calc_pvt(P, T)
        Out.save_massive(Out.z_d_array, PVT_object.z_d)
        Out.save_massive(Out.r_s_m3m3_array, PVT_object.r_s_m3m3)
        Out.save_massive(Out.p_atm_array, P)
        Out.save_massive(Out.b_o_m3m3_array, PVT_object.b_o_m3m3)
        Out.save_massive(Out.T_C_array, T)
        print(P, PVT_object.z_d)
    label = 'Т = ' + str(T)    
#    Out.plot(1, 'газосодержание в нефти', 'Давление, атм', \
#'Газосодержание, м3/м3',  0,\
#120, 0, 200,\
#Out.p_atm_array, Out.r_s_m3m3_array, '', label)
#    Out.plot(2, 'Объемный коэффициент нефти', 'Давление, атм', \
#'Объемный коэффициент, м3/м3',  0.9,\
#1.6, 0, 200,\
#Out.p_atm_array, Out.b_o_m3m3_array, '', label)
    
    Out.plot(3, 'Z-Factor', 'Давление, атм', \
'Z - factor',  0.01,\
1.1, 0, 100,\
Out.p_atm_array, Out.z_d_array, '', label)    
    
    Out.r_s_m3m3_array.clear()   
    Out.b_o_m3m3_array.clear()
    Out.p_atm_array.clear()
    Out.T_C_array.clear()
    Out.z_d_array.clear()






   
        
        
        