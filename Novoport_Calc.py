# -*- coding: utf-8 -*-
"""
Created on Mon Sep 21 13:06:58 2015

@author: Гость
"""

class Data:
    def __init__(self):

        self.N_n = 100 #число расчётных узлов
        self.Iter = 10 # граница итерационных вычислений
        'Режим работы'
        self.q_osc_m3day = 26 * (1 - 8.1/100) 
        self.WCT_d = 0.081
        self.R_p_m3m3 = 1278.6
        self.t_en_C = 56
        self.t_sn_C = 16.3
        self.p_sn_atm = 28
        
        'PVT - свойства продукции'
        self.gamma_o_d = 0.841
        self.gamma_g_d = 0.8
        self.gamma_w_d = 1
        self.r_sb_m3m3 = 83 * 0.779 
        self.P_rb_atm = 145
        self.b_ro_m3m3 = 1.149 
        self.s_mgl = 50000
        self.PVT_correlations = "Standing", "Dranchuk", "Beggs_Robinson", \
        "Standing", "Standing", "Standing"
        self.C_o_1MPa = 8.5 * 10**(-4)        
        
        'Конструкция'
        self.theta = 90
        self.d_m = 0.073
        self.ksi_m = 0.0002
        self.length_m = 2335
        self.D_c_m = 0.178
        self.D_p_m = 0.092
        'Приток'
        self.pi_m3atmday = 0.79 
        self.P_res_atm = 184.8
        self.P_test_atm = 90
        
        self.T_res_C = 60        
        self.L_m = 2800       
        
        
if __name__ == "__main__":
    print("Вы запустили модуль напрямую, а не импортировали его.") 
    input ("\n\nНажмите Enter, чтобы выйти.")          