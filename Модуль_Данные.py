# -*- coding: utf-8 -*-
"""
@author: Халиков Руслан
Модуль предназначен для работой с исходными, вспомогательными и выходными данными
"""
import csv, numpy, math
import matplotlib.pyplot as plt
import matplotlib.pyplot as mpl
import matplotlib.text as mtext
from mpl_toolkits.mplot3d import Axes3D
mpl.rcParams['font.family'] = 'fantasy'
mpl.rcParams['font.fantasy'] = 'Times New Roman'

class Исходные_данные:
    def Считать_csv_Данные(self, csv_file):
        infile = open(csv_file, 'r')
        data_table = [row for row in csv.reader(infile, delimiter=';')]
        infile.close()
        ''' Параметры скорость/точность вычислений'''
        self.N_n = 100 #Число_расчётных_узлов_скважины 
        self.Iter = 10 #Максимальное_количество_повторений_циклов 
        ''' Режим работы скважины ''' 
        self.q_osc_m3day = float(data_table[18][1]) * (1 - float(data_table[19][1]))   #Дебит_нефти_СтУсл_м3_сут 
        self.WCT_d = float(data_table[19][1]) #Обводненность_доли_ед
        self.R_p_m3m3 = float(data_table[20][1])    #Газовый_фактор_скважины_м3_м3 
        self.t_en_C = float(data_table[25][1]) #Температура_забоя_градусы_С
        self.t_sn_C = float(data_table[24][1]) #Температура_устья_градусы_С
        self.p_sn_atm = float(data_table[30][1])   #Давление_буферное_атм
        ''' Физико - химические свойства скважинной продукции '''
        self.gamma_o_d = float(data_table[11][1])#Удельная_плотность_нефти
        self.gamma_g_d = float(data_table[12][1])#Удельная_плотность_газа
        self.gamma_w_d = float(data_table[13][1])#Удельная_плотность_воды
        self.r_sb_m3m3 = float(data_table[14][1])#Газосодержание при давлении насыщения_м3_м3
        self.P_rb_atm = float(data_table[15][1])#Давление_насыщение_атм
        self.b_ro_m3m3 = -1#Объемный_коэффициент_нефти_экспериментальный_м3_м3 
        self.s_mgl = 0 #Солёность_мг_л
        '''
        1) Псевдокритические PT:
            1.1) Standing
            1.2) Unknown
        2) Z - фактор:
            2.1) Dranchuk
            2.2) Beggs_Brill_Standing
        3) Вязкость дегазированной нефти:
            3.1) Beggs_Robinson
            3.2) Standing
        4) Давление насыщения:
            4.1) Standing
            4.2) Valko_McCainSI
        5) Объемный коэффициент нефти:
            5.1) Standing
            5.2) McCainSI
        6) Газосодержание:
            6.1) Standing
            6.2) VelardeSI
        '''
        ''' PVT - эмпирические корреляции '''
        # Выбор: 1) "Standing"; 2) "Unknown"        
        Псевдокритические_PT = "Standing" 
        # Выбор: 1) "Dranchuk"; 2) "Beggs_Brill_Standing"
        Z_фактор = "Dranchuk" 
        # Выбор: 1) "Beggs_Robinson"; 2) "Standing"
        Вязкость_дегазированной_нефти = "Beggs_Robinson" 
        # Выбор: 1) "Standing"; 2) "Valko_McCainSI"
        Давление_насыщения = "Standing"
        # Выбор: 1) "Standing"; 2) "McCainSI"
        Объемный_коэффициент_нефти = "Standing"
        # Выбор: 1) "Standing"; 2) "VelardeSI"
        Газосодержание = "Standing"
        self.PVT_correlations = Псевдокритические_PT, Z_фактор, \
        Вязкость_дегазированной_нефти, \
        Давление_насыщения, Объемный_коэффициент_нефти, Газосодержание
        self.C_o_1MPa = None #Коэффициент_сжимаемости_нефти_1_МПа       
        ''' Конструкция скважины'''
        self.theta = 90 #Угол_наклона_трубы_градусы
        self.d_m =  float(data_table[2][3])#Внутренний_диаметр_подъемника_м 
        self.ksi_m = 0.0002 #Шероховатость_трубы_м
        self.length_m = float(data_table[6][0])#Длина_трубы_м 
        self.D_c_m = float(data_table[2][2])#Внутренний_диаметр_эксп_колонны_м 
        self.L_m = float(data_table[7][0])#Глубина_верхних_дыр_перфорации_измеренная_м         
        ''' Параметры для расчёта притока'''
        self.pi_m3atmday = float(data_table[35][1])#Коэффициент_продуктивности_скважины_м3_атм_сут 
        self.P_res_atm = float(data_table[34][1])#Пластовое_давление_текущее_атм
        self.P_test_atm = 90#Давление_эксперимент_атм
        self.T_res_C = 100#Температура_пласта_С
        ''' Параметры, характерные для газлифтного С/Э''' 
        self.H_gv_w_m = 2000#Глубина_спуска_рабочего_клапана_м 
 
class Выходные_данные():
    def __init__(self):
        self.flow_pattern = None
        self.gradP_g_atmm = None
        self.gradP_f_atmm = None
        self.gradP_atmm = None
        self.H_L_theta_d = None
        self.md_m_array = []
        self.p_atm_array = []
        self.gradP_g_atmm_array = []
        self.gradP_f_atmm_array = []
        self.gradP_atmm_array = []
        self.T_C_array = []
        self.flow_pattern_array = []
        self.P_b_atm_array = []
        self.r_s_m3m3_array = []
        self.b_o_m3m3_array = []
        self.mu_o_cP_array = []
        self.z_d_array = []
        self.b_g_m3m3_array = []
        self.mu_g_cP_array = []
        self.b_w_m3m3_array = []
        self.mu_w_cP_array = []
        self.IPR_array = []
        self.P_test_atm_array = []
        self.P_VLP_atm_array = []
        self.VLP_array = []        
        self.lambda_l_d_array = []        
        self.N_Fr_d_array = []        
        self.d_m_array = []        
        self.H_L_theta_d = None
        
        self.L_1_1_array = []; self.lambda_l_d_1_1 = []
        self.L_1_2_array = []; self.lambda_l_d_1_2 = []        
        self.L_2_array = []; self.lambda_l_d_2 = []
        self.L_3_array = []; self.lambda_l_d_3 = []
        self.L_4_array = []; self.lambda_l_d_4 = []        
      
    def Запись_в_csv_файл(self, csv_file):
        ''' вывод в csv - файл'''
        outfile = open(csv_file, 'w')
        writer = csv.writer(outfile, delimiter=';')
        table = numpy.column_stack((self.md_m_array, self.flow_pattern_array))
        for row in table:
            writer.writerow(row)
        outfile.close()
      
    def save(self, P_atm, T_C, L_m, pvt, calculator, pipe):
        self.md_m_array.append('%5.0f' % L_m)
        self.p_atm_array.append(P_atm)
        self.gradP_g_atmm_array.append(self.gradP_g_atmm)
        self.gradP_f_atmm_array.append(self.gradP_f_atmm)
        self.gradP_atmm_array.append(self.gradP_atmm)
        self.T_C_array.append(T_C)
        self.flow_pattern_array.append(self.flow_pattern)
        self.P_b_atm_array.append(pvt.p_b_MPa * 10)
        self.r_s_m3m3_array.append(pvt.r_s_m3m3)
        self.b_o_m3m3_array.append(pvt.b_o_m3m3)
        self.mu_o_cP_array.append(pvt.mu_o_cP)
        self.z_d_array.append(pvt.z_d)
        self.b_g_m3m3_array.append(pvt.b_g_m3m3)
        self.mu_g_cP_array.append(pvt.mu_g_cP)        
        self.b_w_m3m3_array.append(pvt.b_w_m3m3)
        self.mu_w_cP_array.append(pvt.mu_w_cP)
        self.lambda_l_d_array.append(calculator.lambda_l_d)        
        self.N_Fr_d_array.append(calculator.N_Fr_d)
        self.d_m_array.append(pipe.d_m)        
        
    def save_PVT(self, P_atm, T_C, pvt):
        self.p_atm_array.append(P_atm)
        self.T_C_array.append(T_C)
        self.P_b_atm_array.append(pvt.p_b_MPa * 10)
        self.r_s_m3m3_array.append(pvt.r_s_m3m3)
        self.b_o_m3m3_array.append(pvt.b_o_m3m3)
        self.mu_o_cP_array.append(pvt.mu_o_cP)
        self.z_d_array.append(pvt.z_d)
        self.b_g_m3m3_array.append(pvt.b_g_m3m3)
        self.mu_g_cP_array.append(pvt.mu_g_cP)        
        self.b_w_m3m3_array.append(pvt.b_w_m3m3)
        self.mu_w_cP_array.append(pvt.mu_w_cP)
        
    def save_massive(self, A, B):
        A.append(B)        
                
    def save_IPR(self, Reservoir, P_test_atm):
        self.IPR_array.append(Reservoir.Q_m3day)
        self.P_test_atm_array.append(P_test_atm)        
        
    def save_VLP(self, P_atm, q_m3day):
        self.VLP_array.append(q_m3day)
        self.P_VLP_atm_array.append(P_atm)        
        
    def Построить_график(self, i, title, xlabel, ylabel, xlim_sn, xlim_en, 
                         ylim_sn, ylim_en, x, y, c, legen):
        plt.figure(i)
        plt.title(title, color='black', family='fantasy', fontsize='x-large')
        plt.xlabel(xlabel, color='black', family='fantasy', fontsize='x-large')
        plt.ylabel(ylabel, color='black', family='fantasy', fontsize='x-large')       
        plt.grid(True)
        plt.ylim(ylim_sn, ylim_en)
        plt.xlim(xlim_sn, xlim_en)        
        plt.plot(x, y, c, linewidth=3, label=legen)
        plt.legend(loc='best')
        plt.show()
        
    def Построить_графики(self, Data, 
                          plot_1 = None,
                          plot_2 = None,
                          plot_3 = None,
                          plot_4 = None,
                          plot_5 = None,
                          plot_6 = None,
                          plot_7 = None,
                          plot_8 = None,
                          plot_9 = None,
                          plot_10 = None,
                          plot_11 = None,
                          plot_12 = None):
        i = 0        
        plot_i = [plot_1, plot_2, plot_3, plot_4, plot_5, plot_6, 
                  plot_7, plot_8, plot_9, plot_10, plot_11, plot_12]        
                
        if 'Профиль_давления' in plot_i:                      
            i+=1            
            self.Построить_график(i, 'Кривая распределения давления в НКТ', 
            'Давление (отток [ур. башмак]), атм', 'Глубина, м',
            min(self.p_atm_array),
            max(self.p_atm_array), 
            Data.L_m, 0, 
            self.p_atm_array, self.md_m_array, 
            'b', 'Beggs Brill')
            
        if 'Индикаторная_кривая' in plot_i:
            i+=1
            self.Построить_график(i, 'Индикаторная кривая', 'Дебит, м3/сут', 
            'Давление (приток [ур. забой]), атм', min(self.IPR_array), 
            max(self.IPR_array), min(self.P_test_atm_array), 
            max(self.P_test_atm_array), 
            self.IPR_array, self.P_test_atm_array, 'g', 'Вогель')

        if 'Узловой_анализ' in plot_i:
            i+=1
            self.Построить_график(i, 'Узловой анализ', 'Дебит, м3/сут', \
            'Давление (приток [ур. забой]), атм', min(self.IPR_array), 
            max(self.IPR_array), min(self.P_test_atm_array),\
            max(self.P_test_atm_array), \
            self.IPR_array, self.P_test_atm_array, 'b', 'Вогель')          
            
            self.Построить_график(i, 'Узловой анализ', 'Дебит, м3/сут', \
            'Давление (отток [ур. башмак]), атм', min(self.VLP_array), 
            max(self.VLP_array), min(self.P_test_atm_array), \
            max(self.P_test_atm_array), self.VLP_array, 
            self.P_VLP_atm_array, 'r', 'VLP')            
            



    def plot_3d(self, i, title, xlabel, ylabel, ylim_sn, ylim_en, xlim_sn, 
             xlim_en, zlim_sn, zlim_en, x, y, z, legen):
        fig = plt.figure(i)
        ax = fig.gca(projection='3d')
        ax.set_xlim3d(xlim_sn, xlim_en)
        ax.set_ylim3d(ylim_sn, ylim_en)
        ax.set_zlim3d(ylim_sn, ylim_en)        
        ax.set_zlim3d(0, 10)        
        ax.set_xlabel('X axis')
        ax.set_ylabel('Y axis')
        ax.set_zlabel('Z axis')
        plt.plot(x, y, z, label='parametric curve')
        plt.show()
        
    def plot_flow_map_Beggs_Brill_3d(self, i, Pipe):
        self.L_1_1_array = []; self.lambda_l_d_1_1 = []
        self.L_1_2_array = []; self.lambda_l_d_1_2 = []        
        self.L_2_array = []; self.lambda_l_d_2 = []
        self.L_3_array = []; self.lambda_l_d_3 = []
        self.L_4_array = []; self.lambda_l_d_4 = []
        fig = plt.figure(i)
        ax = fig.gca(projection='3d')
        ax.set_title('Карта режимов потока', color='black', family='fantasy', fontsize='x-large')        
        ax.set_xlim3d(0, 1)
        ax.set_ylim3d(0, 1000)
        ax.set_zlim3d(Pipe.length_m, 0)
        ax.set_xlabel('Входное содержание жидкости', color='black', family='fantasy', fontsize='x-large')
        ax.set_ylabel('Число Фруда', color='black', family='fantasy', fontsize='x-large')
        ax.set_zlabel('Глубина', color='black', family='fantasy', fontsize='x-large')
        plt.grid()        
        for lambda_l_d in (0.0001, 0.001, 0.01005):        
            L_1 = 316 * lambda_l_d ** 0.302
            self.L_1_1_array.append(L_1)
            self.lambda_l_d_1_1.append(lambda_l_d)
        for lambda_l_d in (0.01005, 0.1, 0.4001):        
            L_1 = 316 * lambda_l_d ** 0.302
            self.L_1_2_array.append(L_1)
            self.lambda_l_d_1_2.append(lambda_l_d)         
        for lambda_l_d in (0.01005, 0.1, 1):        
            L_2 = 0.000925 * lambda_l_d ** (-2.468)
            self.L_2_array.append(L_2)
            self.lambda_l_d_2.append(lambda_l_d)
        for lambda_l_d in (0.0101, 0.1, 1):    
            L_3 = 0.10 * lambda_l_d ** (-1.452)
            self.L_3_array.append(L_3)
            self.lambda_l_d_3.append(lambda_l_d)            
        for lambda_l_d in (0.4001, 1):        
            L_4 = 0.5 * lambda_l_d ** (-6.738)
            self.L_4_array.append(L_4)
            self.lambda_l_d_4.append(lambda_l_d)
        ax.text(0.01, 25, Pipe.length_m, 'Расслоенный', fontsize = 10, color = 'red')             
        ax.text(0.3, 75, Pipe.length_m, 'Прерывистый', fontsize = 18, color = 'green')     
        ax.text(0.2, 250, Pipe.length_m, 'Распределенный', fontsize = 18, color = 'black') 
        ax.text(0.09, 0.3, Pipe.length_m, 'Переходный', fontsize = 10, color = 'blue') 
        plt.plot(self.lambda_l_d_1_1, self.L_1_1_array, Pipe.length_m, 'r', linewidth=3)
        plt.plot(self.lambda_l_d_1_2, self.L_1_2_array, Pipe.length_m, 'k', linewidth=3)
        plt.plot(self.lambda_l_d_2, self.L_2_array, Pipe.length_m, 'b', linewidth=3)
        plt.plot(self.lambda_l_d_3, self.L_3_array, Pipe.length_m, 'g', linewidth=3)
        plt.plot(self.lambda_l_d_4, self.L_4_array, Pipe.length_m, 'k', linewidth=3)        
        
        x_massive_1 = []
        x_massive_2 = []
        x_massive_3 = []
        x_massive_4 = []
        
        y_massive_1 = []
        y_massive_2 = []
        y_massive_3 = []
        y_massive_4 = []            
        
        z_massive_1 = []
        z_massive_2 = []
        z_massive_3 = []
        z_massive_4 = []
        
        for i in range(1, len(self.flow_pattern_array) - 1):
            if self.flow_pattern_array[i] == 'Расслоенный':
                x_massive_1.append(self.lambda_l_d_array[i])
                y_massive_1.append(self.N_Fr_d_array[i])        
                z_massive_1.append(self.md_m_array[i])
            if self.flow_pattern_array[i] == 'Прерывистый':
                x_massive_2.append(self.lambda_l_d_array[i])
                y_massive_2.append(self.N_Fr_d_array[i])        
                z_massive_2.append(self.md_m_array[i])        
            if self.flow_pattern_array[i] == 'Распределенный':
                x_massive_3.append(self.lambda_l_d_array[i])
                y_massive_3.append(self.N_Fr_d_array[i])        
                z_massive_3.append(self.md_m_array[i])        
            if self.flow_pattern_array[i] == 'Переходный':
                x_massive_4.append(self.lambda_l_d_array[i])
                y_massive_4.append(self.N_Fr_d_array[i])        
                z_massive_4.append(self.md_m_array[i])       
        
        plt.plot(x_massive_1, y_massive_1, z_massive_1, 'r', linewidth=3)
        plt.plot(x_massive_2, y_massive_2, z_massive_2, 'g', linewidth=3)        
        plt.plot(x_massive_3, y_massive_3, z_massive_3, 'k', linewidth=3)        
        plt.plot(x_massive_4, y_massive_4, z_massive_4, 'b', linewidth=3)        
        plt.show()

    def plot_flow_map_Beggs_Brill(self, i):
        self.L_1_1_array = []; self.lambda_l_d_1_1 = []
        self.L_1_2_array = []; self.lambda_l_d_1_2 = []        
        self.L_2_array = []; self.lambda_l_d_2 = []
        self.L_3_array = []; self.lambda_l_d_3 = []
        self.L_4_array = []; self.lambda_l_d_4 = [] 
        
        plt.figure(i)
        plt.title('Карта режимов потока', color='black', family='fantasy', fontsize='x-large')
        plt.xscale('log')
        plt.yscale('log')        
        plt.xlabel('Входное содержание жидкости', color='black', family='fantasy', fontsize='x-large')
        plt.ylabel('Число Фруда', color='black', family='fantasy', fontsize='x-large')       
        plt.grid()
        plt.ylim(0.1, 1000)
        plt.xlim(0.0001, 1)        
        for lambda_l_d in (0.0001, 0.001, 0.01005):        
            L_1 = 316 * lambda_l_d ** 0.302
            self.L_1_1_array.append(L_1)
            self.lambda_l_d_1_1.append(lambda_l_d)
        for lambda_l_d in (0.01005, 0.1, 0.4001):        
            L_1 = 316 * lambda_l_d ** 0.302
            self.L_1_2_array.append(L_1)
            self.lambda_l_d_1_2.append(lambda_l_d)         
        for lambda_l_d in (0.01005, 0.1, 1):        
            L_2 = 0.000925 * lambda_l_d ** (-2.468)
            self.L_2_array.append(L_2)
            self.lambda_l_d_2.append(lambda_l_d)
        for lambda_l_d in (0.0101, 0.1, 1):    
            L_3 = 0.10 * lambda_l_d ** (-1.452)
            self.L_3_array.append(L_3)
            self.lambda_l_d_3.append(lambda_l_d)            
        for lambda_l_d in (0.4001, 1):        
            L_4 = 0.5 * lambda_l_d ** (-6.738)
            self.L_4_array.append(L_4)
            self.lambda_l_d_4.append(lambda_l_d)
        plt.text(0.0004, 3, 'Расслоенный', fontsize = 20, color = 'red')             
        plt.text(0.03, 25, 'Прерывистый', fontsize = 20, color = 'green')     
        plt.text(0.001, 400, 'Распределенный', fontsize = 20, color = 'black') 
        plt.text(0.09, 0.3, 'Переходный', fontsize = 20, color = 'blue') 
        plt.plot(self.lambda_l_d_1_1, self.L_1_1_array, 'r', linewidth=3)
        plt.plot(self.lambda_l_d_1_2, self.L_1_2_array, 'k', linewidth=3)
        plt.plot(self.lambda_l_d_2, self.L_2_array, 'b', linewidth=3)
        plt.plot(self.lambda_l_d_3, self.L_3_array, 'g', linewidth=3)
        plt.plot(self.lambda_l_d_4, self.L_4_array, 'k', linewidth=3)        
        plt.show() 
 
 
 
 
 
 
 
class Вспомогательные_данные():
    g_msec2 = 9.81 
    'Значение плотности для расчёта плотности нефти из входного параметра\
    удельная плотность нефти [усл. плот. воды при стандартных условиях]'    
    rho_ref = 1000
    rho_air = 1.2217 # плотность воздуха при стандартных условиях    
    def __init__(self):
        self.q_gsc_m3day = None
        self.q_wsc_m3day = None
        self.rho_osc_kgm3 = None
        self.rho_gsc_kgm3 = None
        self.a_p_m2 = None
        self.q_o_m3day = None
        self.q_w_m3day = None
        self.q_l_m3day = None
        self.q_g_m3day = None
        self.f_w_d = None
        self.lambda_l_d = None
        self.rho_o_kgm3 = None
        self.rho_w_kgm3 = None
        self.rho_l_kgm3 = None
        self.rho_g_kgm3 = None
        self.sigma_l_Newtonm = None
        self.mu_l_cP = None
        self.rho_n_kgm3 = None
        self.mu_n_cP = None
        self.v_sl_msec = None
        self.v_sg_msec = None
        self.v_m_msec = None
        self.N_Re_d = None
        self.N_Fr_d = None
        self.N_Lv_d = None
        self.E_d = None
        
    def Пересчитать_данные(self, pvt, pipe, Исходные_данные):
        self.q_gsc(Исходные_данные.R_p_m3m3, Исходные_данные.q_osc_m3day)
        self.q_wsc(Исходные_данные.WCT_d, Исходные_данные.q_osc_m3day)     
        self.rho_osc(pvt.gamma_o_d) 
        self.rho_gsc(pvt.gamma_g_d)
        self.a_p(pipe.d_m)
        self.q_o(Исходные_данные.q_osc_m3day, pvt.b_o_m3m3)
        self.q_w(pvt.b_w_m3m3)
        self.q_l()
        self.q_g(pvt.b_g_m3m3, pvt.r_s_m3m3, Исходные_данные.q_osc_m3day)
        self.f_w()
        self.lambda_l()
        self.rho_o(pvt.r_s_m3m3, pvt.b_o_m3m3)
        self.rho_w(pvt.rho_w_sc_kgm3, pvt.b_w_m3m3)
        self.rho_l()
        self.rho_g(pvt.b_g_m3m3)
        self.sigma_l(pvt.sigma_o_Newtonm, pvt.sigma_w_Newtonm)        
        self.mu_l(pvt.mu_o_cP, pvt.mu_w_cP)
        self.rho_n()
        self.mu_n(pvt.mu_g_cP)
        self.v_sl()
        self.v_sg()
        self.v_m()
        self.N_Re(pipe.d_m)        
        self.N_Fr(pipe.d_m)
        self.N_Lv()
        self.E(pipe.ksi_m, pipe.d_m)
        
    'Плотность газа в стандартных условиях'
    def rho_gsc(self, gamma_g_d):
        self.rho_gsc_kgm3 = gamma_g_d * self.rho_air                 
               
    'Плотность нефти в стандартных условиях'       
    def rho_osc(self, gamma_o_d):       
        self.rho_osc_kgm3 = gamma_o_d * self.rho_ref
            
    'Дебит воды в стандартных условиях'    
    def q_wsc(self, WCT_d, q_osc_m3day):
        self.q_wsc_m3day = WCT_d * q_osc_m3day / (1 - WCT_d)
        
    'Дебит газа в стандартных условиях'            
    def q_gsc(self, R_p_m3m3, q_osc_m3day):        
        self.q_gsc_m3day = R_p_m3m3 * q_osc_m3day
        
    'поперечное сечение трубы'
    def a_p(self, d_m):
        self.a_p_m2 = math.pi * d_m ** 2 / 4
        
    'дебит нефти при опорном давлении'    
    def q_o(self, q_osc_m3day, b_o_m3m3):
        self.q_o_m3day = q_osc_m3day * b_o_m3m3
    
    'дебит воды при опорном давлении'    
    def q_w(self,  b_w_m3m3):        
        self.q_w_m3day = self.q_wsc_m3day * b_w_m3m3
     
    'дебит жидкости при опорном давлении'   
    def q_l(self):
        self.q_l_m3day = self.q_o_m3day + self.q_w_m3day
        
    'дебит газа при опорном давлении'
    def q_g(self, b_g_m3m3, r_s_m3m3, q_osc_m3day):
        self.q_g_m3day = b_g_m3m3 * (self.q_gsc_m3day - r_s_m3m3 * q_osc_m3day)        
            
    'объемное содержание воды при опорном давлении'
    def f_w(self):
        self.f_w_d = self.q_w_m3day / self.q_l_m3day
        
    'объемное содержание жидкости в предположении отсутствия проскальзывания'
    def lambda_l(self):
        self.lambda_l_d = self.q_l_m3day /(self.q_l_m3day + self.q_g_m3day)

    'плотность нефти'        
    def rho_o(self, r_s_m3m3, b_o_m3m3):
        self.rho_o_kgm3 = (self.rho_osc_kgm3 + r_s_m3m3\
        * self.rho_gsc_kgm3) / b_o_m3m3

    'плотность воды'
    def rho_w(self, rho_w_sc_kgm3, b_w_m3m3):
        self.rho_w_kgm3 = rho_w_sc_kgm3 / b_w_m3m3
    
    'плотность жидкости'    
    def rho_l(self):
        self.rho_l_kgm3 = self.rho_o_kgm3 * (1 - self.f_w_d)\
        + self.rho_w_kgm3 * self.f_w_d
    
    'плотность газа'   
    def rho_g(self, b_g_m3m3):
        self.rho_g_kgm3 = self.rho_gsc_kgm3 / b_g_m3m3
    
    'поверхностное натяжение в системе жидкость-газ'
    def sigma_l(self, sigma_o_Newtonm, sigma_w_Newtonm):
        self.sigma_l_Newtonm = sigma_o_Newtonm * (1 - self.f_w_d)\
        + sigma_w_Newtonm * self.f_w_d 

    'вязкость жидкости'
    def mu_l(self, mu_o_cP, mu_w_cP):
        self.mu_l_cP = mu_o_cP * (1 - self.f_w_d) + mu_w_cP * self.f_w_d

    'плотность нефти при отсутствии проскальзывания'  
    def rho_n(self):
        self.rho_n_kgm3 = self.rho_l_kgm3 * self.lambda_l_d + self.rho_g_kgm3\
        * (1 - self.lambda_l_d)
    
    'вязкость жидкости при отсутствии проскальзывания'    
    def mu_n(self, mu_g_cP):
        self.mu_n_cP = self.mu_l_cP * self.lambda_l_d + mu_g_cP\
        * (1 - self.lambda_l_d)

    'приведенная скорость жидкости'   
    def v_sl(self):
        self.v_sl_msec = 0.000011574 * self.q_l_m3day / self.a_p_m2
    
    'приведенная скорость газа'
    def v_sg(self):
        self.v_sg_msec = 0.000011574 * self.q_g_m3day / self.a_p_m2
    
    'приведенная скорость смеси'
    def v_m(self):
        self.v_m_msec = self.v_sl_msec + self.v_sg_msec
    
    'число Рейнольдса'    
    def N_Re(self, d_m):
        self.N_Re_d = 1000 * self.rho_n_kgm3 * self.v_m_msec * d_m\
        / self.mu_n_cP

    'число Фруда'
    def N_Fr(self, d_m):
        self.N_Fr_d = self.v_m_msec ** 2 / (self.g_msec2 * d_m)

    'число скорости жидкости'
    def N_Lv(self):
        self.N_Lv_d = self.v_sl_msec * (self.rho_l_kgm3\
        / (self.g_msec2 * self.sigma_l_Newtonm)) ** 0.25 

    'относительная шероховатость трубы'
    def E(self, ksi_m, d_m):
        self.E_d = ksi_m / d_m 
        
if __name__ == "__main__":
    print("Вы запустили модуль напрямую, а не импортировали его.") 
    input ("\n\nНажмите Enter, чтобы выйти.")          