# -*- coding: utf-8 -*-
"""
Created on Mon Aug 17 21:11:50 2015

@author: KhalikovRuslan

PVT - модуль
Рассчитывает неизвестные свойства нефти, газа и воды по приведенным
корреляционным зависимостям.

Функции рассчитывающие величину, либо параметры в уравнении заканчиваются 
суффиксом указывающим на размерность рассчитываемой величины _atm. 
Для безразмерных величин суффикс _d.

Названия функции состоят из: [имя]_[расчитываемая величина]_[размерность]
"""

import math,csv

class PVT():
    '''Класс физико-химических свойств скважинной продукции'''
    
    'Переводной параметр [*C] -> [K]'
    T_C_K = 273
    'Переводной параметр [атм] -> [МПа]'
    P_atm_MPa = 0.1013
    'усл. плот. воды при стандартных условиях'
    rho_ref = 1000
    
    def __init__(self):
        
         # Выбор: 1) "Standing"; 2) "Unknown"        
        Псевдокритические_PT = "Standing" 
        # Выбор: 1) "Dranchuk"; 2) "Beggs_Brill_Standing"
        Z_фактор = "Dranchuk" 
        # Выбор: 1) "Beggs_Robinson"; 2) "Standing"
        Вязкость_дегазированной_нефти = "Beggs_Robinson" 
        # Выбор: 1) "Standing"; 2) "Valko_McCainSI"
        Давление_насыщения = "Standing"
        # Выбор: 1) "Standing"; 2) "McCainSI"
        Объемный_коэффициент_нефти = "Standing"
        # Выбор: 1) "Standing"; 2) "VelardeSI"
        Газосодержание = "Standing"
        self.P_rb_MPa = 90 * self.P_atm_MPa;\
        self.b_ro_m3m3 = -1 
        self.Salinity_mg_liter = 0;
        self.pseudo_PT, self.correl_Z, self.correl_mu_do, self.correl_p_b,\
        self.correl_b_o_sat, self.correl_r_s = Псевдокритические_PT, Z_фактор, \
        Вязкость_дегазированной_нефти, \
        Давление_насыщения, Объемный_коэффициент_нефти, Газосодержание
        self.T_res_K = 100 + self.T_C_K
        
        self.C_o_1MPa = None #Коэффициент_сжимаемости_нефти_1_МПа   
        self.WCT_d = 0.9 #Обводненность_доли_ед
        self.R_p_m3m3 = 300    #Газовый_фактор_скважины_м3_м3 
        self.t_en_C = 100 #Температура_забоя_градусы_С
        self.t_sn_C = 45 #Температура_устья_градусы_С
        self.p_sn_atm = 20   #Давление_буферное_атм
        ''' Физико - химические свойства скважинной продукции '''
        self.gamma_o_d = 0.835#Удельная_плотность_нефти
        self.gamma_g_d = 0.9#Удельная_плотность_газа
        self.gamma_w_d = 1#Удельная_плотность_воды
        self.r_sb_m3m3 = 300#Газосодержание при давлении насыщения_м3_м3
        self.P_rb_atm = 90#Давление_насыщение_атм
        
        self.b_w_m3m3 = None; self.mu_w_cP = None; self.z_d = None; 
        self.b_g_m3m3 = None; self.mu_g_cP = None; self.mu_do_cP = None; 
        self.p_b_MPa = None; self.P_pc_MPa = None; self.T_pc_K = None; 
        self.P_pr_d = None; self.T_pr_d = None;  
        self.b_o_sat_m3m3 = None; self.rho_o_sat_kgm3 = None;
        self.r_s_m3m3 = None; self.b_o_m3m3 = None; self.mu_o_sat_cP = None;
        self.mu_o_cP = None; self.rho_w_sc_kgm3 = None; 
        'Коэффициент поверхностного натяжения вода - газ [Н/м]'
        self.sigma_w_Newtonm = 0.01
        'Коэффициент поверхностного натяжения нефть - газ [Н/м]'
        self.sigma_o_Newtonm = 0.00841
        print("Свойства флюида по умолчанию")
        
    def read_from_data(self, Data):
        infile = open(Data, 'r')
        data_table = [row for row in csv.reader(infile, delimiter=';')]
        infile.close()
        
        self.C_o_1MPa = None #Коэффициент_сжимаемости_нефти_1_МПа   
        self.WCT_d = float(data_table[0][1]) #Обводненность_доли_ед
        self.R_p_m3m3 = float(data_table[1][1])    #Газовый_фактор_скважины_м3_м3 
        self.t_en_C = float(data_table[2][1]) #Температура_забоя_градусы_С
        self.t_sn_C = float(data_table[3][1]) #Температура_устья_градусы_С
        self.p_sn_atm = float(data_table[4][1])   #Давление_буферное_атм
        ''' Физико - химические свойства скважинной продукции '''
        self.gamma_o_d = float(data_table[6][1])#Удельная_плотность_нефти
        self.gamma_g_d = float(data_table[7][1])#Удельная_плотность_газа
        self.gamma_w_d = float(data_table[8][1])#Удельная_плотность_воды
        self.r_sb_m3m3 = float(data_table[9][1])#Газосодержание при давлении насыщения_м3_м3
        self.P_rb_atm = float(data_table[10][1])#Давление_насыщение_атм
        print("Данные прочитанны из файла "+Data)
    

    def calc_pvt(self, P, T):
        'Конвертируем входные параметры'        
        T += self.T_C_K

        P_MPa = P * self.P_atm_MPa
        
        'Пересчёт свойств воды к условиям опорных давления и температуры'
        self.water_formation_volume_factor_B_w_d(P_MPa, T)
        self.water_viscosity(P_MPa, T)
        self.WaterDensitySC()        
        
        'Псевдокритические свойства'
        if self.pseudo_PT == "Standing":
            self.pseudo_P_Standing()
            self.pseudo_T_Standing()            
        elif self.pseudo_PT == "Unknown":        
            self.pseudo_P()
            self.pseudo_T()                

        'Свойства газа'
        self.P_pr_d = P_MPa / self.P_pc_MPa #приведенное давление
        self.T_pr_d = T / self.T_pc_K #приведенная температура
        
        if self.correl_Z == "Dranchuk":
            self.z_Dranchuk()
        elif self.correl_Z == "Beggs_Brill_Standing":        
            self.z_Brill_Beggs_Standing()
            
        self.gas_FVF(T, P_MPa) # Объемный коэффициент газа

        self.visc_g(T, P_MPa) # Вязкость газа

        'Вязкость дегазированной нефти'
        if self.correl_mu_do == "Beggs_Robinson":
            self.visc_do_BR(T)
        elif self.correl_mu_do == "Standing":        
            self.visc_do_S(T)            
            
        'Вязкость насыщенной нефти'
        self.SaturatedOilViscosity_Beggs_Robinson(self.r_sb_m3m3)            
                       
#==============================================================================
#             20151014 - Корректировка (удаление p_offs)
#            'Давление насыщения'   
#         if self.correl_p_b == "Standing":
#             self.p_b_Standing(T)
#         elif self.correl_p_b == "Valko_McCainSI":        
#             self.p_b_Valko_McCainSI(T)        
#    
#         'При заданном калибровочном давлении насыщения'
#         if (self.P_rb_MPa > 0): 
#             p_fact = self.p_b_MPa / self.P_rb_MPa
#             p_offs = self.p_b_MPa - self.P_rb_MPa
#         else: 
#             p_fact = 1
#             p_offs = 0
# 
#         'Недонасыщенная нефть'
#         if P_MPa > (self.p_b_MPa / p_fact): 
#             P_MPa += p_offs
#         else:
#             P_MPa *= p_fact        
#==============================================================================
            
        'Давление насыщения, при пластовой температуре'   
        if self.correl_p_b == "Standing":
            self.p_b_Standing(self.T_res_K)
        elif self.correl_p_b == "Valko_McCainSI":        
            self.p_b_Valko_McCainSI(self.T_res_K)
   
        'При заданном калибровочном давлении насыщения'
        if (self.P_rb_MPa > 0): 
            p_fact = self.p_b_MPa / self.P_rb_MPa
        else: 
            p_fact = 1
          
        P_MPa *= p_fact

        'Расчёт сжимаемости, при давлении насыщения'
        if self.C_o_1MPa == None:        
            self.Compressibility_Oil_VB(self.r_sb_m3m3, self.T_res_K, self.p_b_MPa)

        'При заданном калибровочном объемном коэффициенте\
         нефти при давлении насыщения'
        if (self.b_ro_m3m3 > 0):            
            if self.correl_b_o_sat == "Standing":    
                self.FVF_Saturated_Oil_Standing(self.r_sb_m3m3, self.T_res_K)
            elif self.correl_b_o_sat == "McCainSI":
                self.Density_McCainSI(self.p_b_MPa, self.T_res_K, self.r_sb_m3m3)
                self.FVF_McCainSI(self.r_sb_m3m3)
            b_fact = (self.b_ro_m3m3 - 1) / (self.b_o_sat_m3m3 - 1)
        else:
            b_fact = 1


        'Давление насыщения, при текущей температуре'   
        if self.correl_p_b == "Standing":
            self.p_b_Standing(T)
        elif self.correl_p_b == "Valko_McCainSI":        
            self.p_b_Valko_McCainSI(T)

        if P_MPa > (self.p_b_MPa): #недонасыщенная нефть
            self.r_s_m3m3 = self.r_sb_m3m3
            
            #Предполагаем, P = 1 [атм] -> b_o_m3m3 = 1
            if self.correl_b_o_sat == "Standing":    
                self.FVF_Saturated_Oil_Standing(self.r_s_m3m3, T)
                self.b_o_sat_m3m3 = b_fact * (self.b_o_sat_m3m3 - 1) + 1
            elif self.correl_b_o_sat == "McCainSI":
                self.Density_McCainSI(self.p_b_MPa, T, self.r_s_m3m3)
                self.FVF_McCainSI(self.r_s_m3m3)
                self.b_o_sat_m3m3 = b_fact * (self.b_o_sat_m3m3 - 1) + 1    
            self.b_o_m3m3 = self.b_o_sat_m3m3 * math.exp(self.C_o_1MPa\
            * (self.p_b_MPa - P_MPa))
            
            if self.r_sb_m3m3 < 350:
                self.Oil_Viscosity_Standing(P_MPa)
            else:    
                self.OilViscosity_Vasquez_Beggs(P_MPa)
        
        else: 
            if self.correl_r_s == "Standing":    
                self.GOR_Standing(P_MPa, T)
            elif self.correl_r_s == "VelardeSI":
                self.GOR_VelardeSI(P_MPa, T)
            
            if self.correl_b_o_sat == "Standing":    
                self.FVF_Saturated_Oil_Standing(self.r_s_m3m3, T)
            elif self.correl_b_o_sat == "McCainSI":
                self.Density_McCainSI(P_MPa, T, self.r_s_m3m3)
                self.FVF_McCainSI(self.r_s_m3m3)
            self.b_o_m3m3 = b_fact * (self.b_o_sat_m3m3 - 1) + 1    
        
            if self.r_sb_m3m3 < 350:
                self.Oil_Viscosity_Standing(P_MPa)
            else:    
                self.SaturatedOilViscosity_Beggs_Robinson(self.r_s_m3m3)
        self.p_b_MPa /= p_fact

    
    def WaterDensitySC(self):
        wpTDS = self.Salinity_mg_liter / (10000 * self.gamma_w_d)
        self.rho_w_sc_kgm3 = 0.0160185 * (62.368 + 0.438603 * wpTDS\
        + 0.00160074 * wpTDS ** 2) * self.rho_ref
                      
    def Oil_Viscosity_Standing(self, Pressure_MPa):
        A = 5.6148 * self.r_s_m3m3 * (0.1235 * 10 ** (-5) * self.r_s_m3m3\
        - 0.00074)
        B = 0.68 / 10 ** (0.000484 * self.r_s_m3m3) + 0.25 / 10 ** (0.006176\
        * self.r_s_m3m3)+ 0.062 / 10 ** (0.021 * self.r_s_m3m3)
        self.mu_o_cP = 10 ** A * self.mu_do_cP ** B
        if self.p_b_MPa < Pressure_MPa:
            self.mu_o_cP = self.mu_o_cP + 0.14504 * (Pressure_MPa\
            - self.p_b_MPa) * (0.024 * self.mu_o_cP ** 1.6 + 0.038\
            * self.mu_o_cP ** 0.56)
    
    def GOR_VelardeSI(self, Pressure_MPa, Temperature_K):
        r_sb_old = self.r_sb_m3m3
        p_b_old = self.p_b_MPa
        self.r_sb_m3m3 = 800
        self.p_b_Valko_McCainSI(Temperature_K)
        if p_b_old > self.p_b_MPa:
            self.r_sb_m3m3 = r_sb_old
            if Pressure_MPa < p_b_old:
                self.r_s_m3m3 = (self.r_sb_m3m3) * (Pressure_MPa / p_b_old)
            else:
                self.r_s_m3m3 = self.r_sb_m3m3
            self.p_b_MPa = p_b_old
            return self.r_s_m3m3     
        self.r_sb_m3m3 = r_sb_old
        self.p_b_MPa = p_b_old    
        if self.p_b_MPa > 0:
            Pr = (Pressure_MPa - 0.101) / (self.p_b_MPa)
        else:
            Pr = 0
        if Pr <= 0:
            self.r_s_m3m3 = 0
            return self.r_s_m3m3
        if Pr >= 1: 
            self.r_s_m3m3 = self.r_sb_m3m3
            return self.r_s_m3m3
        if Pr < 1:
            API = 141.5 / self.gamma_o_d - 131.5
            A_0 = 1.8653 * 10 ** (-4)
            A_1 = 1.672608
            A_2 = 0.92987
            A_3 = 0.247235
            A_4 = 1.056052
            A1 = A_0 * self.gamma_g_d ** A_1 * API ** A_2\
            * (1.8 * Temperature_K - 460) ** A_3 * self.p_b_MPa ** A_4
            B_0 = 0.1004
            B_1 = -1.00475
            B_2 = 0.337711
            B_3 = 0.132795
            B_4 = 0.302065
            A2 = B_0 * self.gamma_g_d ** B_1 * API ** B_2\
            * (1.8 * Temperature_K - 460) ** B_3 * self.p_b_MPa ** B_4
            C_0 = 0.9167
            C_1 = -1.48548
            C_2 = -0.164741
            C_3 = -0.09133
            C_4 = 0.047094
            A3 = C_0 * self.gamma_g_d ** C_1 * API ** C_2\
            * (1.8 * Temperature_K - 460) ** C_3 * self.p_b_MPa ** C_4
            Rsr = A1 * Pr ** A2 + (1 - A1) * Pr ** A3
            self.r_s_m3m3 = Rsr * self.r_sb_m3m3
   
    def GOR_Standing(self, Pressure_MPa, Temperature_K):
        yg = 1.225 + 0.001648 * Temperature_K - 1.769 / self.gamma_o_d
        self.r_s_m3m3 = self.gamma_g_d * (1.92 * Pressure_MPa / 10 ** yg)\
        ** 1.204
   
    def OilViscosity_Vasquez_Beggs(self, Pressure_MPa):
        C1 = 957
        C2 = 1.187
        C3 = -11.513
        C4 = -0.01302
        m = C1 * Pressure_MPa ** C2 * math.exp(C3 + C4 * Pressure_MPa)
        self.mu_o_cP = self.mu_o_sat_cP * (Pressure_MPa / self.p_b_MPa) ** m

    def SaturatedOilViscosity_Beggs_Robinson(self, Rs_m3_m3):
        A = 10.715 * (5.615 * Rs_m3_m3 + 100) ** (-0.515)
        B = 5.44 * (5.615 * Rs_m3_m3 + 150) ** (-0.338)
        mu_o = A * (self.mu_do_cP) ** B
        if Rs_m3_m3 == self.r_sb_m3m3:
            self.mu_o_sat_cP = mu_o
        elif Rs_m3_m3 == self.r_s_m3m3:
            self.mu_o_cP = mu_o

    def FVF_McCainSI(self, Rs_m3m3):
        self.b_o_sat_m3m3 = (self.gamma_o_d * self.rho_ref + 1.22117 * Rs_m3m3\
        * self.gamma_g_d) / self.rho_o_sat_kgm3

    def Density_McCainSI(self, Pressure_MPa, Temperature_K , Rs_m3_m3):
        p_b_old = self.p_b_MPa
        if Rs_m3_m3 > 800:
            Rs_m3_m3 = 800
            self.p_b_Valko_McCainSI(Temperature_K)
        ropo = 845.8 - 0.9 * Rs_m3_m3
        pm = ropo
        pmmo = 0
        counter = 0
        while (math.fabs(pmmo - pm) > 0.000001 and counter < 1000):
            pmmo = pm
            A0 = -799.21
            A1 = 1361.8
            A2 = -3.70373
            A3 = 0.003
            A4 = 2.98914
            A5 = -0.00223
            roa = A0 + A1 * self.gamma_g_d + A2 * self.gamma_g_d * ropo + A3\
            * self.gamma_g_d * ropo ** 2 + A4 * ropo + A5 * ropo ** 2
            ropo = (Rs_m3_m3 * self.gamma_g_d + 818.81 * self.gamma_o_d)\
            / (0.81881 + Rs_m3_m3 * self.gamma_g_d / roa)
            pm = ropo
            counter += 1
    #поправка для правильной работы при низких температурах
        if Temperature_K < 520 / 1.8:
            Temperature_K = 520 / 1.8
        if Pressure_MPa <= self.p_b_MPa:
            dpp = (0.167 + 16.181 * (10 ** (-0.00265 * pm)))\
            * (2.32328 * Pressure_MPa) - 0.16 * (0.299 + 263\
            * (10 ** (-0.00376 * pm))) * (0.14503774 * Pressure_MPa) ** 2
            pbs = pm + dpp
            dpt = (0.04837 + 337.094 * pbs ** (-0.951))\
            * (1.8 * Temperature_K - 520) ** 0.938\
            - (0.346 - 0.3732 * (10 ** (-0.001 * pbs)))\
            * (1.8 * Temperature_K - 520) ** 0.475
            pm = pbs - dpt
            self.rho_o_sat_kgm3 = pm
        else:
            dpp = (0.167 + 16.181 * (10 ** (-0.00265 * pm)))\
            * (2.32328 * self.p_b_MPa) - 0.16\
            * (0.299 + 263 * (10 ** (-0.00376 * pm)))\
            * (0.14503774 * self.p_b_MPa) ** 2
            pbs = pm + dpp
            dpt = (0.04837 + 337.094 * pbs ** (-0.951))\
            * (1.8 * Temperature_K - 520) ** 0.938\
            - (0.346 - 0.3732 * (10 ** (-0.001 * pbs)))\
            * (1.8 * Temperature_K - 520) ** 0.475
            pm = pbs - dpt
            self.rho_o_sat_kgm3 = pm * math.exp(self.C_o_1MPa * (Pressure_MPa\
            - self.p_b_MPa))
        self.p_b_MPa = p_b_old
    
    def FVF_Saturated_Oil_Standing(self, Rs_m3m3, Temperature_K):
        f = 5.615 * Rs_m3m3 * (self.gamma_g_d / self.gamma_o_d) ** 0.5 + 2.25\
        * Temperature_K - 575
        self.b_o_sat_m3m3 = 0.972 + 0.000147 * f ** 1.175

    def Compressibility_Oil_VB(self, Rs_m3m3, Temperature_K, Pressure_MPa):
        self.C_o_1MPa = (28.1 * Rs_m3m3 + 30.6 * Temperature_K - 1180\
        * self.gamma_g_d + 1784 / self.gamma_o_d - 10910)\
        / (100000 * Pressure_MPa)

    def water_formation_volume_factor_B_w_d(self, Pressure_MPa, Temperature_K):
        f = 1.8 * Temperature_K - 460
        psi = Pressure_MPa * 145.04
        dvwp = -1.95301 * 10 ** (-9) * psi * f - 1.72834 * 10 ** (-13)\
        * psi ** 2 * f - 3.58922 * 10 ** (-7) * psi - 2.25341\
        * 10 ** (-10) * psi ** 2
        dvwt = -1.0001 * 10 ** (-2) + 1.33391 * 10 ** (-4) * f + 5.50654\
        * 10 ** (-7) * f ** 2
        self.b_w_m3m3 = (1 + dvwp) * (1 + dvwt)
        
    def water_viscosity(self, Pressure_MPa, Temperature_K):
        wpTDS = self.Salinity_mg_liter / (10000 * self.gamma_w_d)
        a = 109.574 - 8.40564 * wpTDS + 0.313314 * wpTDS ** 2 + 0.00872213\
        * wpTDS ** 3
        B = -1.12166 + 0.0263951 * wpTDS - 0.000679461 * wpTDS ** 2 - 5.47119\
        * 10 ** (-5) * wpTDS ** 3 + 1.55586 * 10 ** (-6) * wpTDS ** 4
        visc = a * (1.8 * Temperature_K - 460) ** B
        psi = Pressure_MPa * 145.04
        self.mu_w_cP = visc * (0.9994 + 4.0295 * 10 ** (-5) * psi + 3.1062\
        * 10 ** (-9) * psi ** 2)
      
    def pseudo_P_Standing(self):
        self.P_pc_MPa = 4.6 + 0.1 * self.gamma_g_d - 0.258 * self.gamma_g_d\
        ** 2
            
    def pseudo_T_Standing(self):
        self.T_pc_K = 93.3 + 180 * self.gamma_g_d - 6.94 * self.gamma_g_d ** 2
           
    def pseudo_P(self):
        self.P_pc_MPa = 4.9 - 0.4 * self.gamma_g_d
           
    def pseudo_T(self):
        self.T_pc_K = 95 + 171 * self.gamma_g_d

    def z_Dranchuk(self):
         Z_low = 0.1
         Z_hi = 5
         i = 0
         while (i < 25 or math.fabs(Z_low - Z_hi) > 0.001):
             Z_mid = (Z_hi + Z_low) / 2
             y_low = self.z_estimate_Dranchuk(Z_low)
             y_hi = self.z_estimate_Dranchuk(Z_mid)
             if y_low * y_hi < 0:
                 Z_hi = Z_mid
             else:
                 Z_low = Z_mid
             i += 1

    

#==============================================================================
#          Метод Ньютона - Рафсона 
#          A_1 = 0.3265; A_2 = -1.07; A_3 = -0.5339; A_4 = 0.01569;
#          A_5 = -0.05165; A_6 = 0.5475; A_7 = -0.7361; A_8 = 0.1844;
#          A_9 = 0.1056; A_10 = 0.6134; A_11 = 0.721;
#          T_z = self.T_pr_d
#          P_z = self.P_pr_d
#  #        rho_r = lambda z_d: 0.27 * P_z / (z_d * T_z)
#          myfunc = lambda z_d: -z_d + (A_1 + A_2 / T_z + A_3 / T_z ** 3\
#          + A_4 / T_z ** 4 + A_5 / T_z ** 5) * (0.27 * P_z / (z_d * T_z))\
#          + (A_6 + A_7 / T_z + A_8 / T_z ** 2) * (0.27 * P_z / (z_d * T_z)) ** 2\
#          - A_9 * (A_7 / T_z + A_8 / T_z ** 2) * (0.27 * P_z / (z_d * T_z)) ** 5\
#          + A_10 * (1 + A_11 * (0.27 * P_z / (z_d * T_z)) ** 2) * (0.27 * P_z / (z_d * T_z)) ** 2 / T_z ** 3\
#          *math.exp(-A_11 * (0.27 * P_z / (z_d * T_z)) ** 2) + 1      
#  
#          dermyfunc = lambda z_d: 0.01062882*A_10*A_11*P_z**4*(0.0729*A_11*P_z**2/(T_z**2*z_d**2) + 1)*math.exp(-0.0729*A_11*P_z**2/(T_z**2*z_d**2))/(T_z**7*z_d**5) - 0.01062882*A_10*A_11*P_z**4*math.exp(-0.0729*A_11*P_z**2/(T_z**2*z_d**2))/(T_z**7*z_d**5) - 0.1458*A_10*P_z**2*(0.0729*A_11*P_z**2/(T_z**2*z_d**2) + 1)*math.exp(-0.0729*A_11*P_z**2/(T_z**2*z_d**2))/(T_z**5*z_d**3) + 0.0071744535*A_9*P_z**5*(A_7/T_z + A_8/T_z**2)/(T_z**5*z_d**6) - 0.1458*P_z**2*(A_6 + A_7/T_z + A_8/T_z**2)/(T_z**2*z_d**3) - 0.27*P_z*(A_1 + A_2/T_z + A_3/T_z**3 + A_4/T_z**4 + A_5/T_z**5)/(T_z*z_d**2) - 1
#          
#          z_d = scipy.optimize.newton(myfunc, x0 = 1, fprime = dermyfunc, 
#                                    tol=1e-4, maxiter=200)    
#==============================================================================

    
         self.z_d = Z_mid  
             
            
#==============================================================================
# z_d, A_1, A_2, self.T_pr_d, A_3, self.T_pr_d, A_4, A_5, rho_r, A_6, A_7, A_8, A_9, A_10, A_11 = sympy.Symbols[z_d, A_1, A_2, self.T_pr_d, A_3, self.T_pr_d, A_4, A_5, rho_r, A_6, A_7, A_8, A_9, A_10, A_11]
#              
#             y = -z_d + (A_1 + A_2 / T_pr_d + A_3 / T_pr_d ** 3 + A_4 / T_pr_d ** 4 + A_5 / T_pr_d ** 5) * (0.27 * P_pr_d / (z_d * T_pr_d)) + (A_6 + A_7 / T_pr_d + A_8 / T_pr_d ** 2) * (0.27 * P_pr_d / (z_d * T_pr_d)) ** 2 - A_9 * (A_7 / T_pr_d + A_8 / T_pr_d ** 2) * (0.27 * P_pr_d / (z_d * T_pr_d)) ** 5 + A_10 * (1 + A_11 * (0.27 * P_pr_d / (z_d * T_pr_d)) ** 2) * (0.27 * P_pr_d / (z_d * T_pr_d)) ** 2 / T_pr_d ** 3 * sympy.exp(-A_11 * (0.27 * P_pr_d / (z_d * T_pr_d)) ** 2) + 1  
#              
# (0.27 * P_pr_d / (z_d * T_pr_d))             
#==============================================================================
             
    def z_estimate_Dranchuk(self, z_d): 
        A_1 = 0.3265; A_2 = -1.07; A_3 = -0.5339; A_4 = 0.01569;
        A_5 = -0.05165; A_6 = 0.5475; A_7 = -0.7361; A_8 = 0.1844;
        A_9 = 0.1056; A_10 = 0.6134; A_11 = 0.721;
        rho_r = 0.27 * self.P_pr_d / (z_d * self.T_pr_d)
        X = -z_d + (A_1 + A_2 / self.T_pr_d + A_3 / self.T_pr_d ** 3\
        + A_4 / self.T_pr_d ** 4 + A_5 / self.T_pr_d ** 5) * rho_r\
        + (A_6 + A_7 / self.T_pr_d + A_8 / self.T_pr_d ** 2) * rho_r ** 2\
        - A_9 * (A_7 / self.T_pr_d + A_8 / self.T_pr_d ** 2) * rho_r ** 5\
        + A_10 * (1 + A_11 * rho_r ** 2) * rho_r ** 2 / self.T_pr_d ** 3\
        * math.exp(-A_11 * rho_r ** 2) + 1 
        return X 

    def z_Brill_Beggs_Standing(self):
        'Brill and Beggs, Standing'    
        a = 1.39 * (self.T_pr_d - 0.92) ** 0.5 - 0.36 * self.T_pr_d - 0.101
        b = self.P_pr_d * (0.62 - 0.23 * self.T_pr_d)\
        + self.P_pr_d ** 2 * (0.006 / (self.T_pr_d - 0.86) - 0.037)\
        + 0.32 * self.P_pr_d ** 6 / math.exp(20.723 * (self.T_pr_d - 1))
        c = 0.132 - 0.32 * math.log(self.T_pr_d) / math.log(10)
        d = math.exp(0.715 - 1.128 * self.T_pr_d + 0.42 * self.T_pr_d ** 2)
        self.z_d = a + (1 - a) * math.exp(-b) + c * self.P_pr_d ** d
            
    def gas_FVF(self, T_K, P_MPa):
        self.b_g_m3m3 = 0.00034722 * T_K * self.z_d / P_MPa
         
    def visc_g(self, T_K, P_MPa):
        R = 1.8 * T_K
        mwg = 28.966 * self.gamma_g_d
        gd = P_MPa * mwg / (self.z_d * T_K * 8.31)
        A = (9.379 + 0.01607 * mwg) * R ** 1.5 / (209.2 + 19.26 * mwg + R)
        B = 3.448 + 986.4 / R + 0.01009 * mwg
        C = 2.447 - 0.2224 * B
        self.mu_g_cP = 0.0001 * A * math.exp(B * gd ** C)
            
    def visc_do_BR(self, T_K):
        x = (1.8 * T_K - 460) ** (-1.163)\
        * math.exp(13.108 - 6.591 / self.gamma_o_d)
        self.mu_do_cP = 10 ** (x) - 1
        
    def visc_do_S(self, T_K):
        self.mu_do_cP = (0.32 + 1.8 * 10 ** 7 / (141.5 / self.gamma_o_d\
        - 131.5) ** 4.53) * (360 / (1.8 * T_K - 260))\
        ** (10 ** (0.43 + 8.33 / (141.5 / self.gamma_o_d - 131.5)))
    
    def p_b_Standing(self, T_K):
        Min_rsb = 1.8
        Rsb_old = self.r_sb_m3m3
        if self.r_sb_m3m3 < Min_rsb:
            self.r_sb_m3m3 = Min_rsb
        yg = 1.225 + 0.001638 * T_K - 1.76875 / self.gamma_o_d
        self.p_b_MPa = 0.5197 * (self.r_sb_m3m3 / self.gamma_g_d) ** 0.83 * 10\
        ** yg
        if Rsb_old < Min_rsb:
            self.p_b_MPa = (self.p_b_MPa - 0.1013) * Rsb_old / Min_rsb + 0.1013
     
    def p_b_Valko_McCainSI(self, Temperature_K):
        Min_rsb = 1.8
        Max_rsb = 800
        Rsb_old = self.r_sb_m3m3
        if self.r_sb_m3m3 < Min_rsb:
            self.r_sb_m3m3 = Min_rsb
        if self.r_sb_m3m3 > Max_rsb:
            self.r_sb_m3m3 = Max_rsb
        API = 141.5 / self.gamma_o_d - 131.5
        z1 = -4.814074834 + 0.7480913 * math.log(self.r_sb_m3m3) + 0.1743556\
        * math.log(self.r_sb_m3m3) ** 2 - 0.0206 * math.log(self.r_sb_m3m3)\
        ** 3
        z2 = 1.27 - 0.0449 * API + 4.36 * 10 ** (-4) * API ** 2 - 4.76\
        * 10 ** (-6) * API ** 3
        z3 = 4.51 - 10.84 * self.gamma_g_d + 8.39 * self.gamma_g_d ** 2\
        - 2.34 * self.gamma_g_d ** 3
        z4 = -7.2254661 + 0.043155 * Temperature_K - 8.5548 * 10 ** (-5)\
        * Temperature_K ** 2 + 6.00696 * 10 ** (-8) * Temperature_K ** 3
        z_d = z1 + z2 + z3 + z4
        lnpb = 2.498006 + 0.713 * z_d + 0.0075 * z_d ** 2
        self.p_b_MPa = 2.718282 ** lnpb
        if Rsb_old < Min_rsb:
            self.p_b_MPa = (self.p_b_MPa - 0.1013) * Rsb_old / Min_rsb + 0.1013
        if Rsb_old > Max_rsb:
            self.p_b_MPa = (self.p_b_MPa - 0.1013) * Rsb_old / Max_rsb + 0.1013
        self.r_sb_m3m3 = Rsb_old    