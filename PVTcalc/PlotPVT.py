# -*- coding: utf-8 -*-
"""
Created on Mon Mar  7 16:49:17 2016

@author: Rish
"""
import numpy as np
import matplotlib.pyplot as plt 

    
def GetData(attr, Pval, Tval, N, *obj):    
    
    plt.ylabel(attr)
    
    if attr=="Rs":
        attr="r_s_m3m3"
    elif attr=="Bo":
        attr="b_o_m3m3"
    elif attr=="mu_o":
        attr="mu_o_cP"
    elif attr=="C_o":
        attr="C_o_1MPa"
    
    P = np.arange(1,Pval,Pval/N)
    T = np.arange(0,Tval,Tval/N)
    col=len(obj)
    Var_arr=np.empty((N,col))
    for j in range(col):
        for i in range(len(P)):
            obj[j].calc_pvt(P[i],T[i])
            Var_arr[i,j] = getattr(obj[j],attr)
    return PlotData(P,Var_arr,col)
    plt.show()

        
def PlotData(X,Y,col):       
    if min(X)==0:
        axmin = 0
    else: 
        axmin = min(X)       
    if min2X(Y)==0:
        aymin = 0
    else:
        aymin = min2X(Y)-min2X(Y)*0.1
    plt.axis([axmin,max(X),aymin, max2X(Y)+max2X(Y)*0.1])
    plt.xlabel('P')
    for i in range(col):           
        plt.plot(X,Y[:,i],'o-', label="Obj"+str(i))
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        
def min2X(arr):
    minim = 1
    for row in arr:
        for elem in row:
            if elem<minim:
                minim=elem
    return minim

def max2X(arr):
    maxim = 0
    for row in arr:
        for elem in row:
            if elem>maxim:
                maxim=elem
    return maxim    