# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 15:56:57 2015

@author: KhalikovRuslan

Режим работы скважины
"""

class Mode_of_working():
    def __init__(self, Data):
        self.q_osc_m3day = Data.q_osc_m3day
        self.WCT_d = Data.WCT_d            
        self.R_p_m3m3 = Data.R_p_m3m3
        self.t_en_C = Data.t_en_C 
        self.t_sn_C = Data.t_sn_C 
        self.p_sn_atm = Data.p_sn_atm
        self.P_test_atm = Data.P_test_atm
        self.Es_d = None
    
    def Es(self, Data, Beta_g_d):
        'Рассчёт коэффициента естественной сепарации по Ляпкову'        
        if self.WCT_d <= 0.5: # Ляпков 0.02
            wd_g = 0.02
        elif self.WCT_d > 0.5:
            wd_g = 0.17 
        q_l_sc_m3day = self.q_osc_m3day / (1 - self.WCT_d)
        w_l = 4 * q_l_sc_m3day / 86400 / 3.14 / (Data.D_c_m ** 2\
        - Data.D_p_m ** 2)
        self.Es_d = 1 / (1 + (0.52 * w_l) / (wd_g *(1 - 0.06 * Beta_g_d)))
        
    def Es_Marqeez(self, out, Calc):
        g = 9.81
        
        if out.flow_pattern == "Расслоенный" or out.flow_pattern == "Прерывистый":
            v_inf = 1.53 * (g * Calc.sigma_l_Newtonm * (Calc.rho_l_kgm3\
            - Calc.rho_g_kgm3) / Calc.rho_l_kgm3 ** 2) ** 0.25         
        else:
            v_inf = 1.41 * (g * Calc.sigma_l_Newtonm * (Calc.rho_l_kgm3\
            - Calc.rho_g_kgm3) / Calc.rho_l_kgm3 ** 2) ** 0.25

        # calculate separation efficienty
        a = -0.0093
        b = 57.758
        c = 34.4
        d = 1.308
        st = 272
        backst = 1 / 272
        m = Calc.v_sl_msec / v_inf
        
        self.Es_d = ((1 + (a * b + c * m ** d) / (b + m ** d)) ** st\
        + m ** st) ** backst - m
   


     
if __name__ == "__main__":
    print("Вы запустили модуль напрямую, а не импортировали его.") 
    input ("\n\nНажмите Enter, чтобы выйти.")          