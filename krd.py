# -*- coding: utf-8 -*-
"""
Created on Sat Oct 31 20:58:34 2015

@author: HP
"""
import math
import matplotlib.pyplot as plt

#class krd ():
#    def krilov(self,Data):
#        self.Pi=[]
#        self.Pn=11
#        self.Py=2
#        self.pn=1
#        self.N = (self.Pn-self.Py)/self.pn
#        self.n = self.N+1
#        self.Pi=[ i+1 for i in range(self.n)]
#        print(self.Pi)
    
    
    
    
    
    
    
    
    
    
    
q = 96/86400
Ro_nd = 849
p_y = 2*10**6
Ro_g0 = 1.26 # плотность смеси газов
p_z = 12.5*10**6
mu_nd = 10.2
T_pl = 355
mu_n = 1.3
omega = 0.041
b_n = 1.27
L_c = 2000
G = 76.7
d_t = 0.0635
p_n = 11*10**6
R_g = 78
beta = 0

#
#q = 96/86400
#Ro_nd = 868
#p_y = 2*10**6
#Ro_g0 = 1.26 # плотность смеси газов
#p_z = 17.5*10**6
#mu_nd = 14
#T_pl = 313
#mu_n = 1.3
#omega = 0.041
#b_n = 1.27
#L_c = 2000
#G = 55.6
#d_t = 0.0635
#p_n = 9.2*10**6
#R_g = 78
#beta = 0



delta_p=0.1*10**6

P=[]
#Pn=11
#Py=2
#pn=1
N = int((p_n-p_y)/(1*10**6))
n = int(N+1)
P=[ i+2 for i in range(n)]

#w=(Tp-Tn)/(Lc-Lnc)
dH_dp_mas=[]
H_mass = []
ni=0
for i in range(len(P)):
    
    Pi = P[i]*10**(6)
    omega_p=(0.0034+0.79*omega)/10**(q/(20*d_t**2.67)) #температурный градиент потока
    
    print(i)
    
    T_y = T_pl-omega_p*L_c #температура на устье
    print(T_y)
    T = T_y+((T_pl-T_y)*(Pi-p_y))/(p_z-p_y)  #температура в сечении 
   
   
    #Pi=5.5*10**6
    #Ti=300.5
    
    Ro_gy = Ro_g0/1.293 #относ плот по возд
    Pr=Pi/((46.9-2.06*Ro_gy**2)*10**5) #приведенные P и T
    Tr=T/(97+172*Ro_gy**2)
#    print(Ro_gy)

    
#    z фактор
    if 0<=Pr<=3.8 and 1.17<=Tr<2.0:
        z = 1-Pr*(0.18/(Tr-0.73)-0.135)+0.016*(Pr**3.45)/Tr**6.1
    elif 0<=Pr<=1.45 and 1.05<=Tr<1.17:
        z = 1-0.23*Pr-(1.88-1.6*Tr)*Pr**2
    elif 1.45<=Pr<=4.0 and 1.05<=Tr<=1.17:
        z = 0.13*Pr+(6.05*Tr-6.26)*Tr/Pr**2
    
#    print('z= ',z) 
    
     
    Ro=Ro_g0*(Pi+delta_p)*273/(z*delta_p*T) #плотность газа при зад усл
#    print('Ro= ',Ro)
    sigma = 1/(10**(1.58+0.05*Pi*10**(-6))-(T-305)*72*10**(-6)) #повер нат на границе н-г
#    print('sigma= ',sigma)
    
#    p_t = p_n-(T_pl-T)/(9.157+701.8/(G*(y_c1-0.8*y_a)))*10**6
#    

#      Находим приведенный к норм условиям V выделившегося газа
    Rp = (1 + math.log10(Pi*10**(-6)))/(1+math.log10(p_n*10**(-6)))-1 
    
    mT = 1+0.029*(T-293)*(Ro_nd*Ro_gy*10**(-3)-0.7966)
    Dt = (10**(-3))*Ro_nd*Ro_gy*(4.5-0.00305*(T-293))-4.785
    V_gv = G*Rp*mT*(Dt*(1+Rp)-1) #искомый объем
  
    
#    print('V_gv= ',V_gv)
#    
    
    #это считаем, чтобы найти объемный коэффициент
    V_gr = (G*mT-V_gv) #удельный объем растворенного газа
    alfa = 1+0.0054*(T-293)
    u = 10**(-3)*Ro_nd*G-186
    Ro_gv = alfa*(Ro_gy-0.0036*(1+Rp)*(105.7+u*Rp)) #относ плотность выделившегося газа
    Ro_gr = G*(alfa*mT*Ro_gy-Ro_gv*V_gv/G)/V_gr #относ плотность растворенного газа
    lamT = 10**(-3)*(4.3-3.54*10**(-3)*Ro_nd+1.0377*Ro_gr/alfa+5.581*10**(-6)*Ro_nd*(1-1.61*Ro_nd*V_gr*10**(-6))*V_gr)
    
    if 780<=Ro_nd<=860:
        alfa_n = (3.083-2.638*Ro_nd*10**(-3))*10**(-3)
    elif 860<Ro_nd<=960:
        alfa_n = (2.513-1.975*Ro_nd*10**(-3))*10**(-3)
    

#    print(((8*10**(-5))*Ro_nd-0.047))
#    print(10.2**(0.13+0.002*(T-293)))
    
    b = ((8*10**(-5))*Ro_nd-0.047)*mu_nd**(0.13+0.002*(T-293))
    a = 10**(-0.0175*(293-T)-2.58)
    mu_ndT = mu_nd*(T-293)**a*math.exp(b*(293-T))#вязкость дегаз нефти от T
    
    b = 1+1.0733*10**(-3)*Ro_nd*V_gr*lamT/mT+alfa_n*(T-293)-6.5*10**(-4)*Pi*10**(-6)#объемный коэффициент
    
#    print('b= ',b)
    
    Ro_n = Ro_nd*(1+1.293*10**(-3)*Ro_gr*V_gr/alfa*mT)/b#плотность нефти
    
    
    #расход жидкой и газовой фаз
    Q_v = q*(1-beta)*b+q*beta 
    V_g = ((V_gv*(1-beta))+R_g)*q*z*T*delta_p/(273*Pi)
#    print('Q_v= ',Q_v)
#    print('V_g= ',V_g)
    Vg_kr = 1.75*d_t**2.5+1.25*q#крит расх газа
    
    if V_g<Vg_kr:
        fi = V_g/(V_g+q+0.233*d_t**2*pow(sigma/(72*10**(-3)),1/3))
    else:
        fi = d_t*V_g**(1/2)/(d_t*(V_g**(1/2))+q*0.6023+0.0942*(d_t**1.5)*pow(sigma/(72*10**(-3)),1/3))
    
    Ro_cm = Ro_n*(1-fi)+Ro*fi#плотность газожидкостной смеси

    Gz = 1.055*10**(-3)*(1+5*alfa_n)*G*Ro_nd
    B = 1+0.0017*Gz-0.0228*Gz**0.667
    A = 1+0.0129*Gz-0.0364*Gz**0.85
    mu_nas = A*mu_ndT**B#вязкость газонас нефти
#    print('111111111111111111111= ',mu_ndT)
    if mu_nas<5:
        delta = 0.0114*mu_nas
    elif 5<mu_nas<10:
        delta = 0.057+0.023*(mu_nas-5)
    elif 10<mu_nas<25:
        delta = 0.0171+0.031*(mu_nas-10)
    elif 25<mu_nas<45:
        delta = 0.643+0.045*(mu_nas-25)
    elif 45<mu_nas<75:
        delta = 1.539+0.058*(mu_nas-45)
    elif 75<mu_nas<85:
        delta = 3.286+0.1*(mu_nas-75)
    
    mu_np = mu_nas + delta*(p_z-p_n)*10**(-6)#вязкость пласт нефти

    k=0.73
#    mu_np = [9.7,8.7,7.7,6.8,5.8,4.8,3.8,2.9,1.9,1.3]
    g=9.81
    alfa=0
    dp_dHt=9.07*10**(-9)*(V_g**2)/d_t**5.33+7.95*10**(-6)*(q**1.75)*(mu_np**(1/4))/d_t**4.75+1.08*10**(-7)*q**(1/3)*(mu_np**0.025)*((V_g*q*10**6)**k)/d_t**3
    
    dp_dH = Ro_cm*g*10**(-6)*math.cos(alfa)+dp_dHt
#    print(dp_dHt)
    dH_dp = 1/dp_dH
    
    
    
    if ni == 0:
         dH_dp1 = 1/dp_dH
        
    

    
#Это интеграл 
    if ni == 0:
        yx = 0
    else:
        yx = ((Pi-p_y)*10**(-6)/ni)
        
#    yx = 1
    xx = (dH_dp1+dH_dp)/2
#    print('xx= ',dH_dp)
    if dH_dp_mas != 0:
        for x in range(len(dH_dp_mas)):
            if x < len(dH_dp_mas)-1:
                xx = xx + dH_dp_mas[x]
#    print('y11111= ',dH_dp_mas)
#    print('45555= ',xx)
    H = yx*xx
    xo = Pi*10**(-6)
    print(Pi)
    
#    print('yx= ',yx)
    print('H= ',H)
    dH_dp_mas.append(dH_dp)
    H_mass.append(H)
    ni+=1
    
plt.plot(P,H_mass)  
    
    
plt.xlabel(r'$Давление \ МПа.$') 
plt.ylabel(r'$Высота$') 
plt.title(r'$График$') 
plt.grid(True) 
plt.show() 
print(H)
#print(dH_dp)














